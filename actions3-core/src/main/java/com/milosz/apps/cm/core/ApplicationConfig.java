/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.core;

import static com.milosz.apps.cm.common.Common.getConfigFile;

import com.milosz.apps.cm.common.store.Bytes;
import com.milosz.apps.cm.common.store.ExtA3Db;
import com.milosz.apps.cm.common.store.Key;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public class ApplicationConfig {

  private ExtA3Db db;

  public ApplicationConfig(String appVersion) {
    db = new ExtA3Db(getConfigFile().toFile());
  }

  public void shutdown() {
    db.rollback();
    db.close();
  }

  public void setCurrentProject(Path path) {
    db.begin();
    db.put(Key.of("settings", "current_project"), Bytes.array(path.toAbsolutePath().toString()));
    db.commit();
  }

  public Optional<Path> getCurrentProject() {
    return db.get(Key.of("settings", "current_project")).map(Bytes::str).map(Paths::get);
  }
}

/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.core;

import com.milosz.apps.cm.spi.AppView;
import com.milosz.apps.cm.spi.AppViewConfig;
import com.milosz.apps.cm.spi.AppViewContext;
import com.milosz.apps.cm.spi.AppViewSupport;
import com.milosz.apps.cm.spi.Application;
import java.util.Objects;

public class ShowViewActionNode implements AppActionNode, AppViewContext {

  private final String name;
  private final AppViewConfig configuration;
  private final AppViewSupport viewFactory;
  private final String group;
  private final Application main;

  public ShowViewActionNode(
      String group,
      String name,
      AppViewConfig configuration,
      AppViewSupport viewFactory,
      Application main) {
    super();
    this.name = name;
    this.configuration = configuration;
    this.viewFactory = viewFactory;
    this.group = group;
    this.main = main;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getGroup() {
    return group;
  }

  @Override
  public AppViewConfig getConfig() {
    return configuration;
  }

  @Override
  public AppView createView() {
    final var view = viewFactory.create(this);
    view.changeSupport().addPropertyChangeListener(main);
    view.start();
    return view;
  }

  @Override
  public String toString() {
    return getName();
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final var other = (AppActionNode) obj;
    return Objects.equals(name, other.getName());
  }

  @Override
  public String getCategory() {
    return viewFactory.getName();
  }

  @Override
  public Application getApp() {
    return main;
  }
}

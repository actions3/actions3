/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.core;

import com.milosz.apps.cm.core.DockStateEvent.EventType;
import com.milosz.apps.cm.spi.AppView;
import java.awt.Component;
import java.awt.DefaultKeyboardFocusManager;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.event.WindowListener;
import javax.swing.JFrame;

public class DockDialog extends JFrame implements WindowFocusListener, WindowListener {

  private final AppView view;
  private final DockStateListener listener;
  private final AppKeyEventDispatcher aked;

  public DockDialog(String title, AppView view, DockStateListener listener) {
    super(title);
    this.view = view;
    add(view.component());
    setVisible(true);
    setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
    this.listener = listener;
    addWindowListener(this);
    addWindowFocusListener(this);

    aked = new AppKeyEventDispatcher();
    view.actions().install(null, aked);
    DefaultKeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(aked);
    pack();
  }

  public Component getView() {
    return view.component();
  }

  public String getViewName() {
    return view.component().getName();
  }

  @Override
  public void windowGainedFocus(WindowEvent e) {
    listener.stateChanged(new DockStateEvent(EventType.FOCUS_GAINED, this));
  }

  @Override
  public void windowLostFocus(WindowEvent e) {
    listener.stateChanged(new DockStateEvent(EventType.FOCUS_LOST, this));
  }

  @Override
  public void windowOpened(WindowEvent e) {}

  @Override
  public void windowClosing(WindowEvent e) {
    listener.stateChanged(new DockStateEvent(EventType.CLOSED, this));
  }

  @Override
  public void windowClosed(WindowEvent e) {}

  @Override
  public void windowIconified(WindowEvent e) {}

  @Override
  public void windowDeiconified(WindowEvent e) {}

  @Override
  public void windowActivated(WindowEvent e) {}

  @Override
  public void windowDeactivated(WindowEvent e) {}

  @Override
  public int hashCode() {
    return getViewName().hashCode();
  }

  @Override
  public boolean equals(Object other) {
    if (other instanceof final DockDialog dd) {
      return dd.getViewName().equals(getViewName());
    }
    return false;
  }
}

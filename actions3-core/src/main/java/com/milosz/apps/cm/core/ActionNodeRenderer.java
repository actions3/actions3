/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.core;

import com.milosz.apps.cm.spi.AppViewSupport;
import java.awt.Component;
import java.util.Map;
import java.util.stream.Collectors;
import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

public class ActionNodeRenderer extends DefaultTreeCellRenderer {

  private final Map<String, Icon> icons;

  public ActionNodeRenderer() {
    icons =
        Services.INSTANCE.all().stream()
            .map(n -> Services.INSTANCE.viewSupport(n))
            .collect(Collectors.toMap(AppViewSupport::getName, AppViewSupport::icon));
  }

  @Override
  public Component getTreeCellRendererComponent(
      JTree tree,
      Object value,
      boolean sel,
      boolean expanded,
      boolean leaf,
      int row,
      boolean hasFocus) {
    super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
    if (leaf && value instanceof final AppActionNode an) {
      final var ic = icons.get(an.getCategory());
      if (ic != null) {
        setIcon(icons.get(an.getCategory()));
      }
    }
    return this;
  }
}

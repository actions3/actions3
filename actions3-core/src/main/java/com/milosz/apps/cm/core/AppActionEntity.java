/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.core;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public record AppActionEntity(String id, String name, String type, String group, String rawConfig)
    implements Comparable<AppActionEntity> {
  public List<String> path() {
    return Arrays.asList(group.split("/"));
  }

  public AppActionEntity newWithName(String newName) {
    return new AppActionEntity(UUID.randomUUID().toString(), newName, type(), group(), rawConfig());
  }

  public String key() {
    return "%s:%s".formatted(group, name);
  }

  public String[] toArray() throws Exception {
    return new String[] {id, name, type, group, rawConfig};
  }

  public static AppActionEntity fromArray(String[] data) throws Exception {
    return new AppActionEntity(data[0], data[1], data[2], data[3], data[4]);
  }

  public static AppActionEntity newWithConfig(String type, String raw) {
    return new AppActionEntity(UUID.randomUUID().toString(), "", type, "", raw);
  }

  @Override
  public int compareTo(AppActionEntity o) {
    return key().compareTo(o.key());
  }
}

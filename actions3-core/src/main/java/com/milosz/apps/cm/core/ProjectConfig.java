/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.core;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface ProjectConfig {

  void reload();

  void addAction(AppActionEntity action);

  void deleteAction(String group, String name);

  Optional<AppActionEntity> getAction(String group, String name);

  Collection<AppActionEntity> findSimilarActions(String group, String name);

  void save();

  void shutdown();

  static ProjectConfig stub() {
    return new ProjectConfig() {

      @Override
      public void shutdown() {}

      @Override
      public void save() {}

      @Override
      public void reload() {}

      @Override
      public Collection<AppActionEntity> findSimilarActions(String group, String name) {

        return List.of();
      }

      @Override
      public void deleteAction(String group, String name) {}

      @Override
      public void addAction(AppActionEntity action) {}

      @Override
      public Optional<AppActionEntity> getAction(String group, String name) {
        // TODO Auto-generated method stub
        return Optional.empty();
      }
    };
  }
}

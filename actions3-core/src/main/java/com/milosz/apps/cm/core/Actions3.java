/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.core;

import static com.milosz.apps.cm.common.SwingSupport.showErrorDialog;
import static java.awt.event.InputEvent.CTRL_DOWN_MASK;
import static java.awt.event.KeyEvent.VK_B;
import static java.awt.event.KeyEvent.VK_D;
import static java.awt.event.KeyEvent.VK_DOWN;
import static java.awt.event.KeyEvent.VK_E;
import static java.awt.event.KeyEvent.VK_F;
import static java.awt.event.KeyEvent.VK_G;
import static java.awt.event.KeyEvent.VK_J;
import static java.awt.event.KeyEvent.VK_LEFT;
import static java.awt.event.KeyEvent.VK_M;
import static java.awt.event.KeyEvent.VK_R;
import static java.awt.event.KeyEvent.VK_RIGHT;
import static java.awt.event.KeyEvent.VK_T;
import static java.awt.event.KeyEvent.VK_UP;
import static java.awt.event.KeyEvent.VK_W;
import static java.time.ZonedDateTime.now;
import static java.util.logging.Level.FINE;
import static java.util.logging.Level.INFO;
import static java.util.logging.Level.WARNING;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import static javax.swing.JOptionPane.OK_OPTION;
import static javax.swing.JOptionPane.showConfirmDialog;
import static javax.swing.JOptionPane.showInputDialog;
import static javax.swing.JOptionPane.showMessageDialog;
import static javax.swing.SwingUtilities.invokeLater;

import com.milosz.apps.cm.common.Common;
import com.milosz.apps.cm.common.JsonSupport;
import com.milosz.apps.cm.common.LogUtils;
import com.milosz.apps.cm.common.MouseEventHandler;
import com.milosz.apps.cm.core.DockStateEvent.EventType;
import com.milosz.apps.cm.spi.AppAction;
import com.milosz.apps.cm.spi.AppException;
import com.milosz.apps.cm.spi.AppView;
import com.milosz.apps.cm.spi.AppViewConfig;
import com.milosz.apps.cm.spi.AppViewContext;
import com.milosz.apps.cm.spi.Application;
import java.awt.BorderLayout;
import java.awt.DefaultKeyboardFocusManager;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.lang.Thread.UncaughtExceptionHandler;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Actions3 extends JFrame
    implements DockStateListener,
        ActionListener,
        ChangeListener,
        PropertyChangeListener,
        AppAction,
        Application,
        UncaughtExceptionHandler {

  private static final String APP_NAME = "Actions3";

  private static final Set<Integer> ALL_KEYS =
      Set.of(
          VK_LEFT, VK_RIGHT, VK_W, VK_G, VK_UP, VK_DOWN, VK_E, VK_R, VK_F, VK_D, VK_B, VK_J, VK_M);

  private static final Logger LOG = LogUtils.getLogger(FINE, Actions3.class.getSimpleName());
  private final JTabbedPane tabs;
  private final Map<String, ViewRef> views;
  private AppView currentView;
  private final JTree actionsTree;
  private final AppActionModel treeModel;
  private final AppKeyEventDispatcher aked;
  private final JMenuBar menuBar;
  private final JSplitPane split;
  private final JLabel statusBar = new JLabel(" ");
  private ProjectConfig projectConfig;
  private ApplicationConfig configMgr;
  private Properties appProps;
  private final JsonSupport jsonMapper = JsonSupport.INSTANCE;

  private List<AppActionEntity> allActions;

  public Actions3() {
    super(APP_NAME);
    Thread.setDefaultUncaughtExceptionHandler(this);
    this.views = new HashMap<>();
    treeModel = new AppActionModel();
    actionsTree = new JTree(treeModel);
    actionsTree.addMouseListener(
        MouseEventHandler.of(
            (e) -> {
              if (e.getID() == MouseEvent.MOUSE_PRESSED && e.getClickCount() == 2) {
                final var selPath = actionsTree.getPathForLocation(e.getX(), e.getY());
                if (selPath == null) {
                  // User might double click outside tree items
                  return;
                }
                if (selPath.getLastPathComponent() instanceof final AppActionNode an) {
                  openView(an);
                }
              }
            }));
    actionsTree.setCellRenderer(new ActionNodeRenderer());
    setLayout(new BorderLayout());

    this.tabs = new JTabbedPane();
    tabs.addChangeListener(this);
    this.menuBar = appMenu();
    setJMenuBar(menuBar);

    final var scroll = new JScrollPane(actionsTree);
    final var spf = scroll.getPreferredSize();
    scroll.setPreferredSize(new Dimension(250, spf.height));
    this.split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, scroll, tabs);
    split.setOneTouchExpandable(true);
    add(split, BorderLayout.CENTER);
    add(statusBar, BorderLayout.PAGE_END);

    aked = new AppKeyEventDispatcher();
    aked.addAction(this);
    DefaultKeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(aked);

    addWindowListener(
        new WindowAdapter() {
          @Override
          public void windowClosing(WindowEvent e) {
            try {
              views.forEach((k, v) -> v.view.shutdown());
              if (configMgr != null) {
                configMgr.shutdown();
              }
              if (projectConfig != null) {
                projectConfig.shutdown();
              }
            } catch (final Exception e1) {
              LOG.severe("Closing window error " + e1.getMessage());
            }
            System.exit(0);
          }

          @Override
          public void windowOpened(WindowEvent e) {
            invokeLater(
                () -> {
                  Actions3.this.appProps = loadAppProps();
                  Actions3.this.configMgr =
                      new ApplicationConfig(appProps.getOrDefault("version", "0.0").toString());
                  loadProject();
                  projectConfig.reload();
                });
          }
        });
    LOG.info("App loaded");
    pack();
    this.setExtendedState(this.getExtendedState() | JFrame.MAXIMIZED_BOTH);
  }

  private static Properties loadAppProps() {
    try {
      final var props = new Properties();
      props.load(Actions3.class.getResourceAsStream("application.properties"));
      return props;

    } catch (final Exception e1) {
      LOG.warning("Can't load application properties");
    }
    return new Properties();
  }

  private static record MenuAction(String label, int mnemonic, String command, String submenu) {}

  private static final List<MenuAction> APP_ACTIONS =
      List.of(
          new MenuAction("New project", -1, "$CMD_NEW_PR", ""),
          new MenuAction("Open project", -1, "$CMD_OPEN_PR", ""),
          new MenuAction("Open view", VK_G, "$CMD_OPEN", ""),
          new MenuAction("Close view", VK_W, "$CMD_CLOSE", ""),
          new MenuAction("Previous view", VK_LEFT, "$CMD_LEFT", ""),
          new MenuAction("Next view", VK_RIGHT, "$CMD_RIGHT", ""),
          new MenuAction("Full Screen", VK_F, "$CMD_FULL_SCREEN", ""),
          new MenuAction("Detach view", VK_D, "$CMD_UNDOCK", ""),
          new MenuAction("Change view title", VK_T, "$CMD_NEW_TITLE", ""),
          new MenuAction("New action", -1, "$CMD_NEW_ACTION", "Actions"),
          new MenuAction("Edit action", -1, "$CMD_EDIT_ACTION", "Actions"),
          new MenuAction("Delete action", -1, "$CMD_DELETE_ACTION", "Actions"),
          new MenuAction("Clone action", -1, "$CMD_CLONE_ACTION", "Actions"),
          new MenuAction("Action from view", -1, "$CMD_ACTION_FROM_VIEW", "Actions"),
          new MenuAction("Import actions", -1, "$CMD_IMPORT_ACTIONS", "Actions"),
          new MenuAction("Export actions", -1, "$CMD_EXPORT_ACTIONS", "Actions"));

  private void loadProject() {
    if (projectConfig != null) {
      projectConfig.shutdown();
    }
    final var cp = configMgr.getCurrentProject();
    if (cp.isEmpty()) {
      LOG.log(Level.WARNING, "Current project is undefined");
      projectConfig = ProjectConfig.stub();
    } else {
      setTitle("%s: %s".formatted(APP_NAME, cp.get().getFileName()));

      projectConfig = new LocalProjectConfig(this::configLoaded, cp.get().toString());
    }
    projectConfig.reload();
  }

  private JMenuBar appMenu() {
    final var bar = new JMenuBar();

    final var application = new JMenu("Application");
    final var submenus = new HashMap<String, JMenu>();

    for (final var a : APP_ACTIONS) {
      final var mi = new JMenuItem(a.label);
      if (a.mnemonic() >= 0) {
        mi.setAccelerator(KeyStroke.getKeyStroke(a.mnemonic, CTRL_DOWN_MASK));
      }
      mi.setActionCommand(a.command);
      mi.addActionListener(this);
      if (a.submenu.isBlank()) {
        application.add(mi);
      } else if (submenus.containsKey(a.submenu)) {
        submenus.get(a.submenu).add(mi);
      } else {
        final var sub = new JMenu(a.submenu);
        submenus.put(a.submenu, sub);
        application.add(sub);
        sub.add(mi);
      }
    }

    bar.add(application);

    final var view = new JMenu("View");
    view.setEnabled(false);
    bar.add(view);

    final var help = new JMenu("Help");
    final var about = new JMenuItem("About");
    about.setActionCommand("$CMD_HELP_ABOUT");
    about.addActionListener(this);
    help.add(about);

    final var logs = new JMenuItem("Gather logs");
    logs.setActionCommand("$CMD_HELP_LOGS");
    logs.addActionListener(this);
    help.add(logs);

    bar.add(help);

    return bar;
  }

  private void configLoaded(List<AppActionEntity> actions) {
    this.allActions = Collections.unmodifiableList(actions);
    treeModel.clear();
    for (final var da : actions) {
      if (Services.INSTANCE.isSupported(da.type())) {
        final var support = Services.INSTANCE.viewSupport(da.type());
        treeModel.addGroup(da.group());
        final var viewCfg = support.readConfig(da.rawConfig());
        final var node = new ShowViewActionNode(da.group(), da.name(), viewCfg, support, this);
        treeModel.addAction(da.group(), node);
      } else {
        LOG.info("Unsupported type " + da.type());
      }
    }
  }

  @Override
  public List<AppViewContext> treeActions(Class<? extends AppViewConfig> type) {
    final var ta = new ArrayList<AppViewContext>();
    final var root = treeModel.getRoot();
    for (var g = 0; g < root.getCount(); g++) {
      final var group = root.getGroup(g);
      for (var n = 0; n < group.getCount(); n++) {
        final var an = group.getAction(n);
        if (type.isInstance(an.getConfig())) {
          ta.add(group.getAction(n));
        }
      }
    }
    return ta;
  }

  private void undock() {
    final var activeTab = this.tabs.getSelectedIndex();
    if (activeTab < 0 && this.tabs.getTabCount() == 0) {
      return;
    }
    final var cn = tabs.getSelectedComponent();
    this.tabs.removeTabAt(activeTab);
    final var ref = views.get(cn.getName());
    new DockDialog(ref.title, ref.view, this);
  }

  private void closeView() {
    final var activeTab = this.tabs.getSelectedIndex();
    if (activeTab < 0 && this.tabs.getTabCount() == 0) {
      return;
    }
    final var cn = tabs.getSelectedComponent().getName();
    views.remove(cn).view.shutdown();
    this.tabs.removeTabAt(activeTab);
    if (activeTab > 0) {
      this.tabs.setSelectedIndex(activeTab - 1);
    } else if (activeTab == 0 && tabs.getTabCount() > 0) {
      this.tabs.setSelectedIndex(0);
    }
  }

  private void openView() {
    final var last = actionsTree.getLastSelectedPathComponent();
    if (last instanceof final AppActionNode an) {
      openView(an);
    }
  }

  private void changeTitle() {
    final var activeTab = this.tabs.getSelectedIndex();
    if (activeTab < 0 && this.tabs.getTabCount() == 0) {
      return;
    }
    final var cn = tabs.getSelectedComponent().getName();
    final var ref = views.get(cn);
    final var newTitle = showInputDialog("Insert new title", ref.title);
    if (newTitle == null || newTitle.isBlank()) {
      return;
    }
    views.put(cn, ref.withTitle(newTitle));
    tabs.setTitleAt(activeTab, newTitle);
    final var tab = (ViewTabPanel) tabs.getTabComponentAt(activeTab);
    tab.setTitle(newTitle);
  }

  private void openView(AppActionNode an) {
    var errors = an.getConfig().validateRuntime();
    if (!errors.isEmpty()) {
      showErrorDialog(this, "Can't open the view", errors);
      return;
    }
    final var view = an.createView();
    final var name = an.getName() + "_" + System.currentTimeMillis();
    view.component().setName(name);
    final var title = "%s - %s (%s)".formatted(an.getCategory(), an.getName(), an.getGroup());
    this.views.put(name, new ViewRef(view, title));
    this.tabs.addTab(title, view.component());
    tabs.setSelectedIndex(tabs.getTabCount() - 1);
    tabs.setTabComponentAt(
        tabs.getTabCount() - 1, new ViewTabPanel(name, title, (e) -> closeView(), tabs));
  }

  @Override
  public void openView(String group, String name, String type, AppViewConfig config) {
    final var support = Services.INSTANCE.viewSupport(type);
    openView(new ShowViewActionNode(group, name, config, support, this));
  }

  private void exportActions() {
    final var chooser = new JFileChooser();
    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    final var filter = new FileNameExtensionFilter("Actions3", "a3");
    chooser.setFileFilter(filter);
    final var returnVal = chooser.showSaveDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      var sf = chooser.getSelectedFile();
      if (!sf.getName().endsWith(".a3")) {
        sf = new File(sf.getParent(), sf.getName() + ".a3");
      }
      try (var fout = new FileOutputStream(sf)) {
        final var content =
            jsonMapper.bytes(
                new ProjectExport(allActions.stream().map(ActionExport::fromEntity).toList()));
        fout.write(content);
      } catch (final Exception e) {
        e.printStackTrace();
        throw new RuntimeException(e);
      }
    }
  }

  private void importActions() {
    final var chooser = new JFileChooser();
    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    final var filter = new FileNameExtensionFilter("Actions3", "a3");
    chooser.setFileFilter(filter);
    final var returnVal = chooser.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      final var sfs = chooser.getSelectedFile();
      invokeLater(
          () -> {
            try (var fin = new FileInputStream(sfs)) {
              final var project = jsonMapper.read(fin.readAllBytes(), ProjectExport.class);
              project.actions().stream()
                  .map(
                      e -> {
                        final var name = resolveActionName(e.group(), e.name());
                        return new AppActionEntity(
                            UUID.randomUUID().toString(), name, e.type(), e.group(), e.config());
                      })
                  .forEach(projectConfig::addAction);
              projectConfig.reload();
            } catch (final Exception e) {
              e.printStackTrace();
              throw new RuntimeException(e);
            }
          });
    }
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    LOG.fine("Execute command " + e.getActionCommand());
    switch (e.getActionCommand()) {
      case "$CMD_CLOSE" -> closeView();
      case "$CMD_OPEN" -> openView();
      case "$CMD_LEFT" -> nextTab(-1);
      case "$CMD_RIGHT" -> nextTab(1);
      case "$CMD_FULL_SCREEN" -> fullScreen();
      case "$CMD_UNDOCK" -> undock();
      case "$CMD_NEW_ACTION" -> newAction();
      case "$CMD_DELETE_ACTION" -> deleteAction();
      case "$CMD_EDIT_ACTION" -> editAction();
      case "$CMD_CLONE_ACTION" -> cloneAction();
      case "$CMD_ACTION_FROM_VIEW" -> actionFromView();
      case "$CMD_EXPORT_ACTIONS" -> exportActions();
      case "$CMD_IMPORT_ACTIONS" -> importActions();
      case "$CMD_OPEN_PR" -> openProject(false);
      case "$CMD_NEW_PR" -> openProject(true);
      case "$CMD_HELP_ABOUT" -> helpAbout();
      case "$CMD_HELP_LOGS" -> gatherLogs();
      case "$CMD_NEW_TITLE" -> changeTitle();
    }
  }

  private void gatherLogs() {
    final var cfgDir = Common.getConfigDir();
    final var fid = DateTimeFormatter.ofPattern("yyMMddHHmm").format(now());
    final var fn = "actions3-logs-%s.zip".formatted(fid);
    final var fzip = cfgDir.resolve(fn);
    try (final var zout = new ZipOutputStream(new FileOutputStream(fzip.toFile()))) {
      final var logsDir = cfgDir.resolve("logs");
      Files.walkFileTree(
          logsDir,
          new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                throws IOException {
              final var f = file.toFile().getName();
              if (f.endsWith(".log")) {
                final var all = Files.readAllBytes(file);
                zout.putNextEntry(new ZipEntry(f));
                zout.write(all);
                zout.closeEntry();
              }
              return FileVisitResult.CONTINUE;
            }
          });
      zout.putNextEntry(new ZipEntry("settings.txt"));
      System.getProperties().list(new PrintStream(zout));
      zout.closeEntry();
      zout.putNextEntry(new ZipEntry("application.properties"));
      appProps.list(new PrintStream(zout));
      zout.closeEntry();
      showMessageDialog(this, "Logs saved in file %s".formatted(fzip.toString()));

    } catch (final Exception e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  private void helpAbout() {
    try (final var br =
        new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("about.txt")))) {
      final var msg = new StringBuilder("<html><body>");
      var emptyLine = true;
      String ln = null;
      while ((ln = br.readLine()) != null) {
        if (emptyLine) {
          msg.append("<p>");
        }
        if (ln.isBlank()) {
          emptyLine = true;
          msg.append("</p>");
          continue;
        }
        ln += "<br>";
        msg.append(ln);
      }
      msg.append("</body></html>");
      showMessageDialog(this, msg.toString());
    } catch (final IOException e) {
      LOG.log(WARNING, "Exception", e);
      throw new RuntimeException(e);
    }
  }

  private void openProject(boolean newProject) {
    final var chooser = new JFileChooser();
    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    final var filter = new FileNameExtensionFilter("Actions3 project", "pa3");
    chooser.setFileFilter(filter);
    final var returnVal = newProject ? chooser.showSaveDialog(this) : chooser.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      var sf = chooser.getSelectedFile();
      if (newProject && !sf.getName().endsWith("pa3")) {
        sf = new File(sf.getParent(), sf.getName() + ".pa3");
      }
      configMgr.setCurrentProject(sf.toPath());
      loadProject();
    }
  }

  private void editAction() {
    final var last = actionsTree.getLastSelectedPathComponent();
    if (last instanceof final AppActionNode an) {
      final var dba = projectConfig.getAction(an.getGroup(), an.getName());
      if (dba.isEmpty()) {
        statusBar.setText("Action not found");
        return;
      }
      final var udba = AppActionConfigDialog.showDialog(this, dba.get());
      if (udba.isPresent()) {
        invokeLater(
            () -> {
              projectConfig.addAction(udba.get());
              projectConfig.save();
              projectConfig.reload();
            });
      }
    }
  }

  private void deleteAction() {
    final var last = actionsTree.getLastSelectedPathComponent();
    if (last instanceof final AppActionNode an) {
      final var answ =
          showConfirmDialog(
              this, "Do you want to remove action %s/%s".formatted(an.getGroup(), an.getName()));
      if (answ != OK_OPTION) {
        statusBar.setText("Action canceled");
        return;
      }
      invokeLater(
          () -> {
            projectConfig.deleteAction(an.getGroup(), an.getName());
            projectConfig.save();
            projectConfig.reload();
          });
    }
  }

  private String resolveActionName(String group, String name) {
    final Set<String> similar =
        projectConfig.findSimilarActions(group, name).stream()
            .map(AppActionEntity::name)
            .collect(Collectors.toSet());
    var index = 1;
    var cloneName = "%s-%s".formatted(name, index);
    while (similar.contains(cloneName)) {
      index++;
      cloneName = "%s-%s".formatted(name, index);
    }
    return cloneName;
  }

  private void cloneAction() {
    final var last = actionsTree.getLastSelectedPathComponent();
    if (last instanceof final AppActionNode an) {
      invokeLater(
          () -> {
            final var cloneName = resolveActionName(an.getGroup(), an.getName());
            final var toClone = projectConfig.getAction(an.getGroup(), an.getName());
            projectConfig.addAction(toClone.get().newWithName(cloneName));
            projectConfig.save();
            projectConfig.reload();
          });
    }
  }

  private int notMinDiv = -1;

  private void newAction() {
    if (configMgr.getCurrentProject().isEmpty()) {
      showMessageDialog(
          this,
          "Before you start working with Actions3 you need to open project or create new one",
          "Open or create project",
          INFORMATION_MESSAGE);
      return;
    }
    final var a = AppActionConfigDialog.showDialog(this);
    LOG.log(INFO, a.toString());
    if (a.isPresent()) {
      invokeLater(
          () -> {
            projectConfig.addAction(a.get());
            projectConfig.save();
            projectConfig.reload();
          });
    }
  }

  private void actionFromView() {
    final var config = currentView.viewConfig();
    final var support = Services.INSTANCE.viewSupport(currentView.type());
    final var raw = support.writeConfig(config);
    final var newEntity = AppActionEntity.newWithConfig(currentView.type(), raw);
    final var a = AppActionConfigDialog.showDialog(this, newEntity);
    LOG.log(INFO, a.toString());
    if (a.isPresent()) {
      invokeLater(
          () -> {
            projectConfig.addAction(a.get());
            projectConfig.save();
            projectConfig.reload();
          });
    }
  }

  private void fullScreen() {
    if (split.getDividerLocation() > 1) {
      notMinDiv = split.getDividerLocation();
      split.setDividerLocation(1);
    } else {
      split.setDividerLocation(notMinDiv);
    }
  }

  public static void main(String[] args) {
    if (args.length > 0) {
      if (Arrays.binarySearch(args, "-offline") >= 0) {
        System.setProperty("random.data", "true");
      }
    }
    final var properties = loadAppProps();
    var lafClass = properties.getProperty("laf");
    if (lafClass == null || lafClass.isBlank()) {
      LOG.info("Use cross-platform L&F");
      lafClass = UIManager.getCrossPlatformLookAndFeelClassName();
    }
    try {
      UIManager.setLookAndFeel(lafClass);
    } catch (final Exception e) {
      LOG.log(Level.WARNING, "Can't set look and feel", e);
    }
    SwingUtilities.invokeLater(
        () -> {
          final var app = new Actions3();
          app.setVisible(true);
        });
  }

  private void viewChanged(AppView newView) {
    final var viewMenu = menuBar.getMenu(1);
    if (currentView != null) {
      currentView.changeSupport().removePropertyChangeListener(AppView.STATUS_PROP, this);
      final var previous = currentView;
      previous.actions().uninstall(viewMenu, aked);

      viewMenu.setEnabled(false);
    }
    currentView = newView;
    if (newView != null) {
      currentView.changeSupport().addPropertyChangeListener(AppView.STATUS_PROP, this);
      newView.actions().install(viewMenu, aked);
      viewMenu.setEnabled(true);
    }
  }

  @Override
  public void stateChanged(ChangeEvent e) {
    final var tc = tabs.getTabCount();
    if (tc > 0) {
      final var selected = tabs.getSelectedComponent();
      final var selectedView = views.get(selected.getName());
      viewChanged(selectedView.view);
    } else {
      viewChanged(null);
    }
  }

  private void nextTab(int step) {
    System.out.println("Next tab " + step);
    final var ct = tabs.getSelectedIndex();
    final var total = tabs.getTabCount();
    if (ct + step >= 0 && ct + step < total) {
      tabs.setSelectedIndex(ct + step);
    }
  }

  private void nextNode(int step) {
    final var sm = actionsTree.getSelectionModel();
    final var row = sm.getMaxSelectionRow();
    if (row + step >= 0 && row + step < actionsTree.getRowCount()) {
      actionsTree.setSelectionInterval(row + step, row + step);
    }
  }

  @Override
  public void invoke(KeyEvent e) {
    if (e.getModifiersEx() == CTRL_DOWN_MASK) {
      switch (e.getKeyCode()) {
        case VK_LEFT -> nextTab(-1);
        case VK_RIGHT -> nextTab(1);
        case VK_W -> closeView();
        case VK_G -> openView();
        case VK_UP -> nextNode(-1);
        case VK_DOWN -> nextNode(1);
        case VK_F -> fullScreen();
        case VK_D -> undock();
        case VK_B -> newAction();
      }
    }
  }

  @Override
  public boolean match(KeyEvent e) {
    return e.getModifiersEx() == CTRL_DOWN_MASK && ALL_KEYS.contains(e.getKeyCode());
  }

  @Override
  public void stateChanged(DockStateEvent event) {
    if (event.getType() == EventType.CLOSED) {
      final var vn = event.getSource().getView().getName();
      final var ref = views.get(vn);
      tabs.addTab(ref.title, event.getSource().getView());
    } else if (event.getType() == EventType.FOCUS_GAINED) {
      viewChanged(null);
    } else if (event.getType() == EventType.FOCUS_LOST) {
      final var c = tabs.getSelectedComponent();
      if (c != null) {
        viewChanged(views.get(c.getName()).view);
      }
    }
  }

  private static record ViewRef(AppView view, String title) {

    public ViewRef withTitle(String value) {
      return new ViewRef(view, value);
    }
  }

  private record ProjectExport(List<ActionExport> actions) {}

  private record ActionExport(String group, String name, String type, String config) {
    static ActionExport fromEntity(AppActionEntity e) {
      return new ActionExport(e.group(), e.name(), e.type(), e.rawConfig());
    }
  }

  @Override
  public void propertyChange(PropertyChangeEvent e) {
    switch (e.getPropertyName()) {
      case AppView.STATUS_PROP -> {
        final var nv = e.getNewValue() != null ? e.getNewValue().toString() : "";
        statusBar.setText(nv);
      }
      case "data" -> {
        viewChanged((AppView) e.getNewValue());
      }
    }
  }

  @Override
  public int compareTo(AppAction o) {
    return 0;
  }

  @Override
  public String name() {
    return "Actions3";
  }

  @Override
  public void uncaughtException(Thread t, Throwable e) {
    if (e instanceof final AppException a) {
      LOG.log(Level.WARNING, "Application error in thread %s".formatted(t.getName()), e);
      var message = "Application error %s: %s".formatted(a.type(), a.getMessage());
      if (a.context().isPresent()) {
        message += " (code: %s)".formatted(a.context().get());
      }
      showErrorDialog(this, message, e);
    } else {
      LOG.log(Level.WARNING, "Error in thread %s".formatted(t.getName()), e);
      showErrorDialog(
          this,
          "Unexpected error %s: %s".formatted(e.getMessage(), e.getClass().getSimpleName()),
          e);
    }
  }
}

/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.core;

import com.milosz.apps.cm.spi.AppAction;
import com.milosz.apps.cm.spi.AppActionDispatcher;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public class AppKeyEventDispatcher implements AppActionDispatcher {

  private final List<AppAction> listeners = new ArrayList<>();

  @Override
  public boolean dispatchKeyEvent(KeyEvent e) {
    if (e.getID() != KeyEvent.KEY_PRESSED) {
      return false;
    }
    for (final var l : listeners) {
      if (l.match(e)) {
        l.invoke(e);
        return true;
      }
    }
    return false;
  }

  @Override
  public void addAction(AppAction action) {
    listeners.add(action);
  }

  @Override
  public void removeAction(AppAction action) {
    listeners.remove(action);
  }
}

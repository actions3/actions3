/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.core;

import com.milosz.apps.cm.spi.AppException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

public class AppActionModel implements TreeModel {

  private final RootNode root = new RootNode();
  private final List<TreeModelListener> listeners = new ArrayList<>();

  @Override
  public RootNode getRoot() {
    return root;
  }

  @Override
  public Object getChild(Object parent, int index) {
    if (parent instanceof final RootNode rn) {
      return rn.getGroup(index);
    } else if (parent instanceof final GroupNode gn) {
      return gn.getAction(index);
    }
    throw new AppException("Unsupported type %s".formatted(parent.getClass().getSimpleName()));
  }

  @Override
  public int getChildCount(Object parent) {
    if (parent instanceof final RootNode rn) {
      return rn.getCount();
    } else if (parent instanceof final GroupNode gn) {
      return gn.getCount();
    }
    return 0;
  }

  @Override
  public boolean isLeaf(Object node) {
    return node instanceof AppActionNode;
  }

  public void addGroup(String name) {
    if (root.findGroup(name).isEmpty()) {
      final var idx = root.getCount();
      final var ng = root.addGroup(name);
      final var event =
          new TreeModelEvent(this, new Object[] {root}, new int[] {idx}, new Object[] {ng});
      listeners.forEach(l -> l.treeNodesInserted(event));
    }
  }

  public void addAction(String group, AppActionNode action) {
    final var gr = root.findGroup(group);
    if (gr.isEmpty()) {
      throw new AppException("Group %s not found".formatted(group));
    }
    final var g = gr.get();
    final var idx = g.getCount();
    g.addAction(action);
    final var event =
        new TreeModelEvent(this, new Object[] {root, g}, new int[] {idx}, new Object[] {action});
    listeners.forEach(l -> l.treeNodesInserted(event));
  }

  public void clear() {
    root.clear();
    listeners.forEach(l -> l.treeStructureChanged(new TreeModelEvent(this, new Object[] {root})));
  }

  @Override
  public void valueForPathChanged(TreePath path, Object newValue) {}

  @Override
  public int getIndexOfChild(Object parent, Object child) {
    if (parent instanceof final RootNode rn) {
      return rn.getCount();
    } else if (parent instanceof final GroupNode gn) {
      return gn.getCount();
    }
    throw new AppException("Unsupported type %s".formatted(parent.getClass().getSimpleName()));
  }

  @Override
  public void addTreeModelListener(TreeModelListener l) {
    listeners.add(l);
  }

  @Override
  public void removeTreeModelListener(TreeModelListener l) {
    listeners.remove(l);
  }

  static class RootNode {
    private final List<GroupNode> groups = new ArrayList<>();

    public GroupNode addGroup(String name) {
      final var ng = new GroupNode(name);
      groups.add(ng);
      return ng;
    }

    public void clear() {
      groups.clear();
    }

    public GroupNode getGroup(int index) {
      return groups.get(index);
    }

    public int getCount() {
      return groups.size();
    }

    public Optional<GroupNode> findGroup(String name) {
      return groups.stream().filter(g -> name.equals(g.getName())).findFirst();
    }

    @Override
    public String toString() {
      return "Actions";
    }
  }

  static class GroupNode {
    private final String name;
    private final List<AppActionNode> actions = new ArrayList<>();

    public GroupNode(String name) {
      super();
      this.name = name;
    }

    public void addAction(AppActionNode action) {
      actions.add(action);
    }

    public AppActionNode getAction(int index) {
      return actions.get(index);
    }

    public int getCount() {
      return actions.size();
    }

    @Override
    public String toString() {
      return name;
    }

    public String getName() {
      return name;
    }

    @Override
    public int hashCode() {
      return Objects.hash(name);
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final var other = (GroupNode) obj;
      return Objects.equals(name, other.name);
    }
  }
}

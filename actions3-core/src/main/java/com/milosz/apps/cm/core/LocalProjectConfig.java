/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.core;

import static com.milosz.apps.cm.common.store.Bytes.sha512;

import com.milosz.apps.cm.common.JsonSupport;
import com.milosz.apps.cm.common.store.ExtA3Db;
import com.milosz.apps.cm.common.store.Key;
import com.milosz.apps.cm.common.store.Node;
import com.milosz.apps.cm.spi.AppException;
import java.io.File;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

public class LocalProjectConfig implements ProjectConfig {
  private final Consumer<List<AppActionEntity>> listener;
  private final JsonSupport mapper = JsonSupport.INSTANCE;
  private final ExtA3Db store;

  public LocalProjectConfig(Consumer<List<AppActionEntity>> listener, String file) {
    super();
    this.store = new ExtA3Db(new File(file));
    this.listener = listener;
  }

  @Override
  public void reload() {
    final var all =
        store
            .list(
                k -> k.pkStr().equals("action"),
                (k, b) -> {
                  final var v = fromBytes(b);
                  return new ActionNode(v, k);
                })
            .stream()
            .map(n -> n.value())
            .sorted(Comparator.comparing(AppActionEntity::key))
            .toList();
    listener.accept(all);
  }

  private Key actionKey(String group, String name) {
    final var fullName = "%s:%s".formatted(group, name);
    return Key.of("action", sha512(fullName));
  }

  @Override
  public void addAction(AppActionEntity action) {
    try {
      final var actionKey = actionKey(action.group(), action.name());
      final var idxKey = Key.of("index", action.id());
      store.begin();
      final var existing = store.get(idxKey);
      if (existing.isPresent()) {
        final var ek = new Key(existing.get());
        if (!ek.equals(actionKey)) {
          final var removed = store.remove(ek);
          assert removed : "Expected action not found";
        }
      }
      store.put(actionKey, mapper.bytes(action));
      store.put(idxKey, actionKey.bytes());
      store.commit();
    } catch (final Exception e) {
      e.printStackTrace();
      store.rollback();
      throw new AppException("Can't add action to database", e);
    }
  }

  @Override
  public void deleteAction(String group, String name) {
    final var actionKey = actionKey(group, name);
    final var existing = store.get(actionKey);
    existing.ifPresent(
        a -> {
          final var action = fromBytes(a);
          store.begin();
          store.remove(actionKey);
          store.remove(Key.of("index", action.id()));
          store.commit();
        });
  }

  @Override
  public Optional<AppActionEntity> getAction(String group, String name) {
    return store.get(actionKey(group, name)).map(this::fromBytes);
  }

  @Override
  public Collection<AppActionEntity> findSimilarActions(String group, String name) {
    final var prefix = group + ":" + name;
    return store
        .list(
            k -> k.pkStr().equals("action"),
            (k, b) -> {
              final var v = fromBytes(b);
              return new ActionNode(v, k);
            })
        .stream()
        .map(n -> n.value())
        .filter(a -> a.key().startsWith(prefix))
        .sorted(Comparator.comparing(AppActionEntity::key))
        .toList();
  }

  @Override
  public void save() {}

  @Override
  public void shutdown() {
    store.close();
  }

  private AppActionEntity fromBytes(byte[] a) {
    return mapper.read(a, AppActionEntity.class);
  }

  record ActionNode(AppActionEntity value, Key key) implements Node<AppActionEntity> {}
}

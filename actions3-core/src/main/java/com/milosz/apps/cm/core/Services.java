/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.core;

import com.milosz.apps.cm.spi.AppException;
import com.milosz.apps.cm.spi.AppViewSupport;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public enum Services {
  INSTANCE;

  private Map<String, AppViewSupport> viewSupports;

  Services() {
    this.viewSupports = viewCreators();
  }

  private Map<String, AppViewSupport> viewCreators() {
    return StreamSupport.stream(ServiceLoader.load(AppViewSupport.class).spliterator(), false)
        .collect(Collectors.toMap(AppViewSupport::getName, Function.identity()));
  }

  public AppViewSupport viewSupport(String name) {
    if (viewSupports.containsKey(name)) {
      return viewSupports.get(name);
    }
    throw new AppException("Type %s is not supported".formatted(name), null);
  }

  public boolean isSupported(String name) {
    return viewSupports.containsKey(name);
  }

  public List<String> all() {
    return viewSupports.keySet().stream().sorted().toList();
  }
}

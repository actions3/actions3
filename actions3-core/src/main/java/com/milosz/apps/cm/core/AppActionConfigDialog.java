/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.core;

import com.milosz.apps.cm.common.AppPanelBuilder;
import com.milosz.apps.cm.common.FnAction;
import com.milosz.apps.cm.common.LogUtils;
import com.milosz.apps.cm.spi.AppException;
import com.milosz.apps.cm.spi.ValidationResult;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class AppActionConfigDialog extends JDialog implements ActionListener {

  private AppActionEntity action = null;
  private TextField nameField;
  private TextField groupField;
  private ComboTextField actionTypeField;
  private TextField configField;
  private final Map<String, String> values = new HashMap<>();
  private JLabel status;

  private static final Logger LOG =
      LogUtils.getLogger(Level.FINE, AppActionConfigDialog.class.getSimpleName());

  public AppActionConfigDialog(Frame owner, AppActionEntity dba) {
    super(owner);
    layoutUI();

    final var adapter =
        new WindowAdapter() {
          @Override
          public void windowClosing(WindowEvent e) {
            action = null;
            setVisible(false);
          }
        };
    addWindowListener(adapter);
    if (dba != null) {
      setDbAction(dba);
    }
    if (dba == null) {
      setTitle("New action");
    } else {
      setTitle("Edit action");
    }
    pack();
  }

  private void layoutUI() {
    setLayout(new BorderLayout());
    final var form = new AppPanelBuilder();
    nameField = new StringTextField(form.line("Name"));
    actionTypeField = new ComboTextField(form.list("Type", Services.INSTANCE.all()));
    groupField = new StringTextField(form.line("Group"));
    configField = new MultilineTextField(form.text("Configuration"));
    FnAction.esc(nameField.component(), this::actionPerformed);
    FnAction.esc(actionTypeField.component(), this::actionPerformed);
    FnAction.esc(groupField.component(), this::actionPerformed);
    FnAction.esc(configField.component(), this::actionPerformed);

    nextAction(nameField, actionTypeField);
    nextAction(actionTypeField, groupField);
    nextAction(groupField, configField);

    // configField is last input component. Therefore when user press
    // Ctrl+ENTER it saves configuration.
    FnAction.enter(configField.component(), this::actionPerformed);

    initConfigField();
    actionTypeField.addSelectionListener(
        new ItemListener() {

          @Override
          public void itemStateChanged(ItemEvent e) {
            initConfigField();
          }
        });

    final var box = new Box(BoxLayout.LINE_AXIS);
    final var ok = new JButton("OK");
    ok.setActionCommand(FnAction.ENTER_CMD);
    ok.addActionListener(this);
    box.add(ok);
    box.add(Box.createHorizontalStrut(10));
    final var cancel = new JButton("Cancel");
    cancel.setActionCommand(FnAction.ESC_CMD);
    cancel.addActionListener(this);
    box.add(cancel);

    box.add(Box.createHorizontalStrut(10));
    final var format = new JButton("Validate");
    format.setActionCommand("$Format");
    format.addActionListener(this);
    box.add(format);

    status = new JLabel(" ");
    form.component(status);
    add(form.build(), BorderLayout.CENTER);
    add(box, BorderLayout.PAGE_END);
  }

  private void initConfigField() {
    final var actionType = actionTypeField.getText();
    final var support = Services.INSTANCE.viewSupport(actionType);
    configField.setText(support.emptyConfig());
  }

  private void setDbAction(AppActionEntity action) {
    try {
      final var formattedCfg =
          Services.INSTANCE.viewSupport(action.type()).format(action.rawConfig());
      this.action = action;
      nameField.setText(action.name());
      groupField.setText(action.group());
      configField.setText(formattedCfg);
      actionTypeField.setText(action.type());
      actionTypeField.setEditable(false);
    } catch (final AppException e) {
      throw e;
    } catch (final Exception e) {
      LOG.log(Level.WARNING, "Exception", e);
      throw new AppException("Can't open action in editor", e);
    }
  }

  private void nextAction(TextField t, TextField next) {
    FnAction.enterNext(
        t.component(),
        (e) -> {
          status.setText(" ");
          final var vt = t.getText();
          if (!vt.isBlank()) {
            values.put(t.component().getName(), vt);
            next.component().requestFocusInWindow();
          } else {
            status.setText("Field %s must be not empty".formatted(t.component().getName()));
          }
        });
  }

  public static Optional<AppActionEntity> showDialog(JFrame app) {
    final var dialog = new AppActionConfigDialog(app, null);
    dialog.setModal(true);
    dialog.setVisible(true);
    return Optional.ofNullable(dialog.action);
  }

  private ValidationResult validate(String type, String config) {

    final var support = Services.INSTANCE.viewSupport(type);
    return support.validate(config);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    final var cmd = e.getActionCommand();
    switch (cmd) {
      case "$Format":
        final var cfg = validate(actionTypeField.getText(), configField.getText());
        if (cfg.config() != null) {
          configField.setText(cfg.config());
        }
        if (cfg.config() != null && !cfg.success()) {
          final var errorsMsg =
              cfg.errors().stream()
                  .map(es -> "<li>%s</li>".formatted(es))
                  .collect(Collectors.joining());
          JOptionPane.showMessageDialog(
              this,
              "<html><body><p><b>Action configuration is invalid</b> <br>Incorrect properties:<ul>%s</ul></p></body></html>"
                  .formatted(errorsMsg),
              "Invalid configuration",
              JOptionPane.ERROR_MESSAGE);
        } else if (cfg.config() == null) {
          JOptionPane.showMessageDialog(
              this,
              "<html><body><p><b>Action configuration is invalid:</b> %s</p></body></html>"
                  .formatted("Invalid JSON string. Check details in logs"),
              "Invalid configuration",
              JOptionPane.ERROR_MESSAGE);
        }
        break;
      case FnAction.ESC_CMD:
        action = null;
        setVisible(false);
        break;

      case FnAction.ENTER_CMD:
        final var name = values.getOrDefault("Name", nameField.getText());
        if (name == null || name.isBlank()) {
          status.setText("Field %s must be not empty".formatted("Name"));
          break;
        }

        final var type = values.getOrDefault("Type", actionTypeField.getText());
        if (type == null || type.isBlank()) {
          status.setText("Field %s must be not empty".formatted("Type"));
          break;
        }

        final var group = values.getOrDefault("Group", groupField.getText());
        if (group == null || group.isBlank()) {
          status.setText("Field %s must be not empty".formatted("Group"));
          break;
        }

        final var config = validate(actionTypeField.getText(), configField.getText());
        if (config.success()) {
          final var id = action == null ? UUID.randomUUID().toString() : action.id();
          action = new AppActionEntity(id, name, type, group, configField.getText());
          setVisible(false);
        } else {
          status.setText("Invalid configuration");
        }
        break;
    }
  }

  public static Optional<AppActionEntity> showDialog(JFrame app, AppActionEntity dbAction) {
    final var dialog = new AppActionConfigDialog(app, dbAction);
    dialog.setDbAction(dbAction);
    dialog.setModal(true);
    dialog.setVisible(true);
    return Optional.ofNullable(dialog.action);
  }

  private static interface TextField {
    String getText();

    JComponent component();

    void setText(String t);

    void setEditable(boolean b);
  }

  private record ComboTextField(JComboBox<String> combo) implements TextField {

    @Override
    public String getText() {
      return combo.getSelectedItem().toString();
    }

    @Override
    public JComponent component() {
      return combo;
    }

    @Override
    public void setText(String t) {
      combo.setSelectedItem(t);
    }

    @Override
    public void setEditable(boolean b) {
      combo.setEditable(b);
    }

    public void addSelectionListener(ItemListener listener) {
      combo.addItemListener(listener);
    }
  }

  private record StringTextField(JTextField txt) implements TextField {

    @Override
    public String getText() {
      return txt.getText();
    }

    @Override
    public JComponent component() {
      return txt;
    }

    @Override
    public void setText(String t) {
      txt.setText(t);
    }

    @Override
    public void setEditable(boolean b) {
      txt.setEditable(b);
    }
  }

  private record MultilineTextField(JTextArea txt) implements TextField {

    @Override
    public String getText() {
      return txt.getText();
    }

    @Override
    public JComponent component() {
      return txt;
    }

    @Override
    public void setText(String t) {
      txt.setText(t);
    }

    @Override
    public void setEditable(boolean b) {
      txt.setEditable(b);
    }
  }
}

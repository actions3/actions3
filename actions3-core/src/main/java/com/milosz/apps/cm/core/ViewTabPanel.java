/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.core;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.function.Consumer;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;

public class ViewTabPanel extends JPanel {
  private final Consumer<TabEvent> listener;
  private final JTabbedPane tabs;
  private final JLabel txt;
  private final JLabel close;
  private final Color defaultBg;
  private final Color selectedBg;

  public ViewTabPanel(
      String name, String title, Consumer<TabEvent> tabEventsListener, JTabbedPane tabbed) {
    this.listener = tabEventsListener;
    this.tabs = tabbed;
    setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
    defaultBg = getBackground();
    selectedBg = UIManager.getColor("TabbedPane.selected");
    setBackground(selectedBg);
    txt = new JLabel();
    txt.setFont(txt.getFont().deriveFont(4).deriveFont(Font.BOLD));
    txt.setText(title);
    add(txt);
    add(Box.createHorizontalStrut(8));
    close = new JLabel("x");
    close.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
    close.addMouseListener(
        new MouseAdapter() {

          @Override
          public void mouseClicked(MouseEvent e) {
            if (tabs.getSelectedComponent().getName().equals(name)) {
              listener.accept(new TabEvent(name));
            }
          }

          @Override
          public void mouseExited(MouseEvent e) {
            close.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
          }

          @Override
          public void mouseEntered(MouseEvent e) {
            close.setBorder(BorderFactory.createLineBorder(Color.BLACK));
          }
        });
    close.setToolTipText("Close view");
    tabbed
        .getModel()
        .addChangeListener(
            (e) -> {
              final var selected = tabbed.getSelectedComponent();
              if (selected == null) {
                return;
              }
              final var sn = selected.getName();
              if (sn.equals(name)) {
                setBackground(selectedBg);
              } else {
                setBackground(defaultBg);
              }
            });
    add(close);
  }

  public void setTitle(String value) {
    txt.setText(value);
  }

  public static record TabEvent(String name) {}
  ;
}

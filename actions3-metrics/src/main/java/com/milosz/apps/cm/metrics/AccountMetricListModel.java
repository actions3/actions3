/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import static java.util.Comparator.comparing;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Predicate;
import javax.swing.table.AbstractTableModel;

/** */
public class AccountMetricListModel extends AbstractTableModel implements MetricTable {

  private final List<MetricSpec> metrics;
  private final List<Predicate<MetricSpec>> filters;
  private List<MetricSpec> rows;

  AccountMetricListModel() {
    metrics = new CopyOnWriteArrayList<>();
    filters = new ArrayList<>();
    rows = new CopyOnWriteArrayList<>();
  }

  @Override
  public int getRowCount() {
    synchronized (rows) {
      return rows.size();
    }
  }

  @Override
  public int getColumnCount() {
    return 3;
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    synchronized (rows) {
      final var row = rows.get(rowIndex);
      switch (columnIndex) {
        case 1:
          return row.namespace();
        case 0:
          return row.name();
        case 2:
          return row.dimensionsStr();
        default:
          return null;
      }
    }
  }

  @Override
  public String getColumnName(int column) {
    switch (column) {
      case 0:
        return "Name";
      case 1:
        return "Namespace";
      case 2:
        return "Dimensions";
      default:
        return "";
    }
  }

  public void addFilter(Predicate<MetricSpec> test) {
    filters.add(test);
    rebuildModel();
  }

  public void clearFilters() {
    filters.clear();
    rebuildModel();
  }

  public MetricSpec getRow(int index) {
    synchronized (rows) {
      return rows.get(index);
    }
  }

  @Override
  public void addRow(List<MetricSpec> m) {
    synchronized (rows) {
      metrics.addAll(m);
      rebuildModel();
    }
  }

  @Override
  public void clear() {
    synchronized (rows) {
      metrics.clear();
      rebuildModel();
    }
  }

  private void rebuildModel() {
    var ms = metrics.stream();
    for (final var f : filters) {
      ms = ms.filter(f);
    }
    rows =
        ms.sorted(
                comparing(MetricSpec::namespace)
                    .thenComparing(MetricSpec::name)
                    .thenComparing(MetricSpec::dimensionsStr))
            .toList();
    fireTableDataChanged();
  }
}

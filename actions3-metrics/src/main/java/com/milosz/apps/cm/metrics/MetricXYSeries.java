/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYSeries;

public class MetricXYSeries extends XYSeries {

  private final String label;
  private final double conversion;
  private List<MetricDataset.Value> values = new ArrayList<>();

  public MetricXYSeries(String key, String label, double conversion) {
    super(key, true, false);
    this.label = label;
    this.conversion = conversion;
  }

  public String getLabel() {
    return label;
  }

  public void add(ZonedDateTime dt, double v) {
    values.add(new MetricDataset.Value(dt, v * conversion));
    addOrUpdate(dt.toInstant().toEpochMilli(), v);
  }

  public List<MetricDataset.Value> getValues() {
    return values;
  }

  public void setValues(List<MetricDataset.Value> values) {
    this.values = values;
  }

  @Override
  public XYDataItem addOrUpdate(double x, double y) {
    return super.addOrUpdate(new XYDataItem(x, y * conversion));
  }

  public double getConversion() {
    return conversion;
  }
}

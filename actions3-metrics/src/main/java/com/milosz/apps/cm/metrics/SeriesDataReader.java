/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.cloudwatch.CloudWatchClient;
import software.amazon.awssdk.services.cloudwatch.model.GetMetricDataRequest;
import software.amazon.awssdk.services.cloudwatch.model.GetMetricDataResponse;

/** */
public interface SeriesDataReader {

  Iterable<GetMetricDataResponse> getMetricDataPaginator(GetMetricDataRequest request);

  public static SeriesDataReader of(CloudWatchClient cwClient) {
    return new SeriesDataReader() {

      @Override
      public Iterable<GetMetricDataResponse> getMetricDataPaginator(GetMetricDataRequest request) {
        return cwClient.getMetricDataPaginator(request);
      }
    };
  }

  public static SeriesDataReader aws(String awsProfile, String awsRegion) {
    final var client =
        CloudWatchClient.builder()
            .credentialsProvider(ProfileCredentialsProvider.create(awsProfile))
            .region(Region.of(awsRegion))
            .build();
    return of(client);
  }

  public static SeriesDataReader random(TimePeriod period) {
    return new RandomSeriesDataReader(period);
  }
}

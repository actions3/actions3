/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import static com.milosz.apps.cm.metrics.MetricViewCommand.CLEAR_MARKERS;
import static com.milosz.apps.cm.metrics.MetricViewCommand.CONTROL_PANEL;
import static com.milosz.apps.cm.metrics.MetricViewCommand.CURRENT_VALUES;
import static com.milosz.apps.cm.metrics.MetricViewCommand.MOVE_LEFT;
import static com.milosz.apps.cm.metrics.MetricViewCommand.MOVE_RIGHT;
import static com.milosz.apps.cm.metrics.MetricViewCommand.RANGE_COLLAPSE;
import static com.milosz.apps.cm.metrics.MetricViewCommand.RANGE_EXPAND;
import static com.milosz.apps.cm.metrics.MetricViewCommand.REFRESH_CURRENT;
import static com.milosz.apps.cm.metrics.MetricViewCommand.RELATIVE;
import static com.milosz.apps.cm.metrics.MetricViewCommand.RESET;
import static com.milosz.apps.cm.metrics.MetricViewCommand.SELECT_LEFT;
import static com.milosz.apps.cm.metrics.MetricViewCommand.SELECT_RIGHT;
import static com.milosz.apps.cm.metrics.MetricViewCommand.SERIES_ALL;
import static com.milosz.apps.cm.metrics.MetricViewCommand.SERIES_HIDE;
import static com.milosz.apps.cm.metrics.MetricViewCommand.SERIES_SHOW;
import static com.milosz.apps.cm.metrics.MetricViewCommand.SET_MARKER;
import static com.milosz.apps.cm.metrics.MetricViewCommand.TIME_RANGE;
import static java.awt.event.KeyEvent.VK_DOWN;
import static java.awt.event.KeyEvent.VK_E;
import static java.awt.event.KeyEvent.VK_G;
import static java.awt.event.KeyEvent.VK_I;
import static java.awt.event.KeyEvent.VK_J;
import static java.awt.event.KeyEvent.VK_K;
import static java.awt.event.KeyEvent.VK_LEFT;
import static java.awt.event.KeyEvent.VK_M;
import static java.awt.event.KeyEvent.VK_N;
import static java.awt.event.KeyEvent.VK_O;
import static java.awt.event.KeyEvent.VK_P;
import static java.awt.event.KeyEvent.VK_R;
import static java.awt.event.KeyEvent.VK_RIGHT;
import static java.awt.event.KeyEvent.VK_SPACE;
import static java.awt.event.KeyEvent.VK_T;
import static java.awt.event.KeyEvent.VK_U;
import static java.awt.event.KeyEvent.VK_UP;

import com.milosz.apps.cm.common.ActionCommand;
import com.milosz.apps.cm.common.ActionHandler;
import com.milosz.apps.cm.common.ViewAppAction;
import com.milosz.apps.cm.spi.AppAction;
import com.milosz.apps.cm.spi.AppActionContainer;
import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.List;

public class MetricsActionContainer implements AppActionContainer, ActionHandler {

  private final List<AppAction> viewActions;
  private final SeriesTraceOverlay trace;
  private final MetricChartView view;

  public MetricsActionContainer(MetricChartView view, SeriesTraceOverlay trace) {
    this.trace = trace;
    this.view = view;
    viewActions =
        List.of(
            new ViewAppAction("Shift Left", this, VK_J, MOVE_LEFT),
            new ViewAppAction("Shift Right", this, VK_K, MOVE_RIGHT),
            new ViewAppAction("Previous data point", this, VK_LEFT, SELECT_LEFT),
            new ViewAppAction("Next data point", this, VK_RIGHT, SELECT_RIGHT),
            new ViewAppAction("Reset zoom", this, VK_U, RESET),
            new ViewAppAction("Refresh", this, VK_R, REFRESH_CURRENT),
            new ViewAppAction("Absoulte time range", this, VK_G, TIME_RANGE),
            new ViewAppAction("Show / Hide current values", this, VK_M, CURRENT_VALUES),
            new ViewAppAction("Show / Hide control panel", this, VK_O, CONTROL_PANEL),
            new ViewAppAction("Relative time range", this, VK_T, RELATIVE),
            new ViewAppAction("Expand Range", this, VK_UP, RANGE_EXPAND),
            new ViewAppAction("Collapse Range", this, VK_DOWN, RANGE_COLLAPSE),
            new ViewAppAction("Set marker", this, VK_SPACE, SET_MARKER),
            new ViewAppAction("Clear markers", this, VK_N, CLEAR_MARKERS),
            new ViewAppAction("Show single series", this, VK_I, SERIES_SHOW),
            new ViewAppAction("Hide single series", this, VK_P, SERIES_HIDE),
            new ViewAppAction("Show all series", this, VK_E, SERIES_ALL));
  }

  @Override
  public void accept(ActionCommand command) {
    if (command instanceof final MetricViewCommand cmd) {
      switch (cmd) {
        case REFRESH:
          view.refresh();
          break;
        case REFRESH_CURRENT:
          view.refreshCurrent();
          break;
        case MOVE_LEFT:
          view.move(-1);
          break;
        case MOVE_RIGHT:
          view.move(1);
          break;
        case SELECT_LEFT:
          trace.selectValue(-1);
          break;
        case SELECT_RIGHT:
          trace.selectValue(1);
          break;
        case RESET:
          view.zoomReset();
          break;
        case CURRENT_VALUES:
          trace.switchCurrentValues();
          break;
        case RANGE_EXPAND:
          view.range(-0.25);
          break;
        case RANGE_COLLAPSE:
          view.range(0.25);
          break;
        case SERIES_SHOW:
          view.showOnlySeries();
          break;
        case SERIES_HIDE:
          view.hideSingleSeries();
          break;
        case SET_MARKER:
          trace.setMarker();
          break;
        case CLEAR_MARKERS:
          trace.clearMarkers();
          break;
        case RELATIVE:
          view.changeRelativeRange();
          break;
        case CONTROL_PANEL:
          view.controlPanel();
          break;
        case TIME_RANGE:
          view.changeTimeRange();
          break;
        case SERIES_ALL:
          view.showAllSeries();
          break;
      }
    }
  }

  @Override
  public Collection<AppAction> getActions() {
    return viewActions.stream().sorted().map(AppAction.class::cast).toList();
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    final var cmd = MetricViewCommand.of(e.getActionCommand());
    accept(cmd);
  }
}

/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import java.util.List;
import java.util.Map;
import javax.swing.table.AbstractTableModel;

public class MapDetailsModel extends AbstractTableModel {

  private List<Map.Entry<? extends Object, ? extends Object>> items = null;

  public void setDetails(List<Map.Entry<? extends Object, ? extends Object>> items) {
    this.items = items;
    fireTableDataChanged();
  }

  @Override
  public int getRowCount() {
    if (items == null) {
      return 10;
    }
    return items.size() + 2;
  }

  @Override
  public int getColumnCount() {
    return 2;
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    if (items == null || rowIndex >= items.size()) {
      return null;
    }
    if (columnIndex == 0) {
      return items.get(rowIndex).getKey();
    }
    return items.get(rowIndex).getValue();
  }

  @Override
  public String getColumnName(int column) {
    return switch (column) {
      case 0 -> "Property";
      case 1 -> "Value";
      default -> "";
    };
  }
}

/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/** */
public class ChartMetrics {

  private final List<DataSeriesConfig> metrics;
  private final PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

  public ChartMetrics(List<DataSeriesConfig> metrics) {
    this.metrics =
        metrics.stream()
            .map(DataSeriesConfig::new)
            .collect(Collectors.toCollection(ArrayList::new));
  }

  public void add(DataSeriesConfig m) {
    metrics.add(m);
    changeSupport.firePropertyChange("metrics", metrics.size() - 1, metrics.size());
  }

  public void removeVisible(int index) {
    final var m =
        metrics.stream().filter(DataSeriesConfig::isVisible).skip(index).findFirst().get();
    metrics.remove(m);
    changeSupport.firePropertyChange("metrics", metrics.size() + 1, metrics.size());
  }

  public String resolveLabel(String v) {
    final var similar =
        metrics.stream()
            .filter(DataSeriesConfig::isVisible)
            .map(DataSeriesConfig::getLabel)
            .filter(s -> s.startsWith(v))
            .sorted()
            .toList();
    if (similar.isEmpty() || !similar.get(0).equals(v)) {
      return v;
    }
    var index = 1;
    var alt = "%s-%s".formatted(v, index);
    while (similar.contains(alt)) {
      index++;
      alt = "%s-%s".formatted(v, index);
    }
    return alt;
  }

  public List<DataSeriesConfig> getMetrics() {
    return metrics.stream().filter(DataSeriesConfig::isVisible).toList();
  }

  public int count() {
    return metrics.size();
  }

  public DataSeriesConfig get(int index) {
    return metrics.get(index);
  }

  public PropertyChangeSupport getChangeSupport() {
    return changeSupport;
  }
}

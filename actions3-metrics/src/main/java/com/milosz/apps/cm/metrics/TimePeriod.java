/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import com.milosz.apps.cm.common.DateUtils;
import java.time.Duration;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public enum TimePeriod {
  MINUTES_5("PT5M", "5 minutes"),
  MINUTES_15("PT15M", "15 minutes"),
  HOUR_1("PT1H", "One hour"),
  HOUR_6("PT6H", "6 hours"),
  HOUR_12("PT12H", "12 hours"),
  DAY_1("P1D", "1 day"),
  DAY_7("P7D", "7 days");

  private final String text;
  private final long seconds;
  private final String description;

  private TimePeriod(String text, String description) {
    this.seconds = Duration.parse(text).getSeconds();
    this.text = text;
    this.description = description;
  }

  public long getSeconds() {
    return seconds;
  }

  public ZonedDateTime floor(ZonedDateTime ts) {
    final var epochSec = ts.toInstant().getEpochSecond();
    return DateUtils.of(Instant.ofEpochSecond(epochSec / seconds * seconds));
  }

  public ZonedDateTime ceil(ZonedDateTime ts) {
    final var epochSec = ts.toInstant().getEpochSecond();
    if (epochSec % seconds == 0) {
      return ts;
    }
    final var instant = Instant.ofEpochSecond(epochSec / seconds * seconds + seconds);
    return DateUtils.of(instant);
  }

  public static TimePeriod of(Duration d) {
    final var seconds = d.getSeconds();
    for (final var tp : values()) {
      if (tp.seconds == seconds) {
        return tp;
      }
    }
    return HOUR_1;
  }

  @Override
  public String toString() {
    return description;
  }

  public static final Set<String> DURATIONS =
      Arrays.stream(values()).map(t -> t.text).collect(Collectors.toSet());
}

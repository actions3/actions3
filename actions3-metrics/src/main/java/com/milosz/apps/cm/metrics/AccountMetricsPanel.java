/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import static com.milosz.apps.cm.common.SwingSupport.newButton;

import com.milosz.apps.cm.common.PropertyTable;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.stream.Collectors;
import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.SwingWorker.StateValue;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.cloudwatch.CloudWatchClient;

/** */
public class AccountMetricsPanel extends JPanel implements ListSelectionListener, ActionListener {

  private final JTable listTable;
  private final PropertyTable detailsTable;
  private final AccountMetricListModel listModel;
  private final MapDetailsModel detailsModel;
  private final String awsProfile;
  private final String awsRegion;
  private final JLabel status = new JLabel("0 metrics visible");
  private final ChartMetrics metrics;
  private final JList<String> filters = new JList<>(new DefaultListModel<>());

  public AccountMetricsPanel(String awsProfile, String awsRegion, ChartMetrics metrics) {
    this.metrics = metrics;
    listModel = new AccountMetricListModel();
    listTable = new JTable(listModel);
    listTable.getSelectionModel().addListSelectionListener(this);

    detailsModel = new MapDetailsModel();
    detailsTable = new PropertyTable(detailsModel);
    detailsTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

    final var scroll = new JScrollPane(listTable);
    final var s2 = new JScrollPane(detailsTable);
    s2.setPreferredSize(detailsTable.getPreferredSize());
    final var split = new JSplitPane(JSplitPane.VERTICAL_SPLIT, scroll, s2);
    split.setResizeWeight(1);
    split.setDividerLocation(0.8);

    setLayout(new BorderLayout());
    add(new JScrollPane(filters), BorderLayout.PAGE_START);
    add(split, BorderLayout.CENTER);

    final var bottom = Box.createVerticalBox();
    final var buttonBox = Box.createHorizontalBox();

    final var addBtn = newButton("Add to chart", "$CMD_ADD", this);
    final var filterBtn = newButton("Filter", this);
    final var clearFiltersBtn = newButton("Clear filters", "$CMD_CLEAR", this);
    final var refreshBtn = newButton("Refresh", this);

    buttonBox.add(addBtn);
    buttonBox.add(filterBtn);
    buttonBox.add(clearFiltersBtn);
    buttonBox.add(Box.createHorizontalGlue());
    buttonBox.add(refreshBtn);
    bottom.add(buttonBox);
    final var statusBox = Box.createHorizontalBox();
    statusBox.add(status);
    statusBox.add(Box.createHorizontalGlue());
    bottom.add(statusBox);
    add(bottom, BorderLayout.PAGE_END);
    this.awsProfile = awsProfile;
    this.awsRegion = awsRegion;
  }

  @Override
  public Dimension getMinimumSize() {
    return new Dimension();
  }

  public void populate() {
    final var client =
        CloudWatchClient.builder()
            .credentialsProvider(ProfileCredentialsProvider.create(awsProfile))
            .region(Region.of(awsRegion))
            .build();
    final var mr = new MetricListReader(listModel, client, awsRegion, awsProfile);
    mr.addPropertyChangeListener(
        pc -> {
          if (pc.getPropertyName().equals("state") && pc.getNewValue() == StateValue.DONE) {
            status.setText("%s metrics visible".formatted(listModel.getRowCount()));
          }
        });

    mr.execute();
  }

  @Override
  public void valueChanged(ListSelectionEvent e) {
    if (!e.getValueIsAdjusting()) {
      final var selected = listTable.getSelectedRow();
      if (selected >= 0) {
        final var m = listModel.getRow(selected);
        detailsModel.setDetails(m.details());
      }
    }
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    final var cmd = e.getActionCommand();
    if (cmd.equals("$CMD_ADD")) {
      final var params = SeriesSettingsDialog.showDialog();
      if (params.isEmpty()) {
        return;
      }
      final var selected = listTable.getSelectedRow();
      final var m = listModel.getRow(selected);
      final var cwm = new DataSeriesConfig();
      var stat = params.getOrDefault("Statistic", "Average").toString();
      if (stat.isEmpty()) {
        stat = "Average";
      }
      final var conv = (Number) params.getOrDefault("Conversion", 1L);
      var label = params.getOrDefault("Label", stat + " " + m.name()).toString();
      label = metrics.resolveLabel(label);
      cwm.setLabel(label);
      cwm.setDimensions(
          m.dimensions().stream().collect(Collectors.toMap(d -> d.name(), d -> d.value())));
      cwm.setStatistic(stat);
      cwm.setConversion(conv.longValue());
      cwm.setName(m.name());
      cwm.setNamespace(m.namespace());
      metrics.add(cwm);
    } else if (cmd.equals("$CMD_FILTER")) {
      final var f = JOptionPane.showInputDialog("Input filter");
      final var model = (DefaultListModel<String>) filters.getModel();
      if (f != null) {
        if (f.startsWith("ns like ")) {
          listModel.addFilter(c -> c.namespace().contains(f.replace("ns like ", "")));
        } else if (f.startsWith("ns not like ")) {
          listModel.addFilter(c -> !c.namespace().contains(f.replace("ns not like ", "")));
        } else if (f.startsWith("name like ")) {
          listModel.addFilter(c -> c.name().contains(f.replace("name like ", "")));
        } else if (f.startsWith("name not like ")) {
          listModel.addFilter(c -> !c.name().contains(f.replace("name not like ", "")));
        } else if (f.startsWith("dim like ")) {
          listModel.addFilter(c -> c.dimensionsStr().contains(f.replace("dim like ", "")));
        } else if (f.startsWith("dim not like ")) {
          listModel.addFilter(c -> !c.dimensionsStr().contains(f.replace("dim not like ", "")));
        } else if (f.startsWith("any like ")) {
          listModel.addFilter(c -> c.fullStr().contains(f.replace("any like ", "")));
        } else if (f.startsWith("any not like ")) {
          listModel.addFilter(c -> !c.fullStr().contains(f.replace("any not like ", "")));
        }
        model.addElement(f);
      }
    } else if (cmd.equals("$CMD_CLEAR")) {
      listModel.clearFilters();
      final var model = (DefaultListModel<String>) filters.getModel();
      model.clear();
    } else if (cmd.equals("$CMD_REFRESH")) {
      populate();
    }
  }
}

/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/** */
public class ChartSeriesListModel extends AbstractTableModel implements PropertyChangeListener {

  private final ChartMetrics metrics;
  private final List<DataSeriesConfig> visibleMetrics;

  public ChartSeriesListModel(ChartMetrics metrics) {
    super();
    this.metrics = metrics;
    this.visibleMetrics = new ArrayList<>();
    resetVisible();
  }

  private void resetVisible() {
    visibleMetrics.clear();
    for (var i = 0; i < metrics.count(); i++) {
      final var m = metrics.get(i);
      if (m.isVisible()) {
        visibleMetrics.add(metrics.get(i));
      }
    }
    fireTableDataChanged();
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (evt.getPropertyName().equals("metrics")) {
      resetVisible();
    }
  }

  @Override
  public int getRowCount() {
    return visibleMetrics.size();
  }

  @Override
  public int getColumnCount() {
    return 1;
  }

  public DataSeriesConfig getVisible(int index) {
    return visibleMetrics.get(index);
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    return visibleMetrics.get(rowIndex).getLabel();
  }
}

/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import software.amazon.awssdk.services.cloudwatch.model.Dimension;
import software.amazon.awssdk.services.cloudwatch.model.Metric;

public class MetricSpec {

  private final String namespace;
  private final String name;
  private final List<Dimension> dimensions;
  private final String dimensionsStr;
  private final String fullStr;

  public MetricSpec(String namespace, String name, List<Dimension> dimensions) {
    this.namespace = namespace;
    this.name = name;
    this.dimensions = dimensions;
    dimensionsStr =
        dimensions.stream()
            .map(d -> "%s=%s".formatted(d.name(), d.value()))
            .collect(Collectors.joining(";"));
    fullStr = "%s;%s;%s".formatted(namespace, name, dimensionsStr);
  }

  public static MetricSpec of(Metric awsMetric) {
    return new MetricSpec(awsMetric.namespace(), awsMetric.metricName(), awsMetric.dimensions());
  }

  public String namespace() {
    return namespace;
  }

  public String name() {
    return name;
  }

  public List<Dimension> dimensions() {
    return dimensions;
  }

  public String dimensionsStr() {
    return dimensionsStr;
  }

  public String fullStr() {
    return fullStr;
  }

  public List<Map.Entry<? extends Object, ? extends Object>> details() {
    final var details = new TreeMap<String, Object>();
    details.put("Name", name);
    details.put("Namespace", namespace);
    if (dimensions != null) {
      dimensions.stream()
          .sorted(Comparator.comparing(Dimension::name))
          .forEach(e -> details.put(e.name(), e.value()));
    }

    return new ArrayList<>(details.entrySet());
  }

  public String key(int index) {
    if (index == 0) {
      return "Namespace";
    } else if (index == 1) {
      return "Name";
    }
    return dimensions.get(index - 2).name();
  }

  public String value(int index) {
    if (index == 0) {
      return namespace;
    } else if (index == 1) {
      return name;
    }
    return dimensions.get(index - 2).value();
  }
}

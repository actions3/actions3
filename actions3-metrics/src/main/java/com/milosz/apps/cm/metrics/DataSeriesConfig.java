/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import static java.lang.Math.abs;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.TreeMap;

@JsonAutoDetect(
    setterVisibility = Visibility.NON_PRIVATE,
    getterVisibility = Visibility.NON_PRIVATE)
public class DataSeriesConfig {

  private String namespace;
  private String name;
  private String seriesKey;
  private String statistic;
  private Map<String, String> dimensions;
  private String label;
  private Long conversion;
  private boolean visible = true;
  private String expression;

  public DataSeriesConfig() {}

  public DataSeriesConfig(DataSeriesConfig orig) {
    this.name = orig.name;
    this.namespace = orig.namespace;
    this.seriesKey = orig.seriesKey;
    this.statistic = orig.statistic;
    this.label = orig.label;
    this.conversion = orig.conversion;
    this.visible = orig.visible;
    this.expression = orig.expression;
    this.dimensions = orig.dimensions;
  }

  public List<String> validate() {
    final var errors = new ArrayList<String>();
    if (expression == null) {
      checkNull(name, "name", errors);
      checkNull(namespace, "namespace", errors);
    }
    checkNull(label, "label", errors);
    if (conversion == null || conversion <= 0) {
      errors.add("Property conversion must be greater than zero");
    }
    return errors;
  }

  private void checkNull(Object v, String name, List<String> errors) {
    if (v == null) {
      errors.add("Property %s must be defined".formatted(name));
    }
  }

  public String getNamespace() {
    return namespace;
  }

  public void setNamespace(String namespace) {
    this.namespace = namespace;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String resolveSeriesKey() {
    return seriesKey != null ? seriesKey : "sk%s".formatted(abs(hashCode()));
  }

  String getSeriesKey() {
    return seriesKey;
  }

  void setSeriesKey(String s) {
    this.seriesKey = s;
  }

  public String getStatistic() {
    return statistic;
  }

  public void setStatistic(String statistic) {
    this.statistic = statistic;
  }

  public Map<String, String> getDimensions() {
    return dimensions;
  }

  public void setDimensions(Map<String, String> dimensions) {
    this.dimensions = dimensions;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public Long getConversion() {
    return conversion == null ? 1 : conversion;
  }

  public void setConversion(Long conversion) {
    this.conversion = conversion;
  }

  public boolean isVisible() {
    return visible;
  }

  public void setVisible(boolean visible) {
    this.visible = visible;
  }

  public String getExpression() {
    return expression;
  }

  public void setExpression(String expression) {
    this.expression = expression;
  }

  public List<Map.Entry<? extends Object, ? extends Object>> details() {
    final var view = new TreeMap<String, Object>();
    view.put("Name", name);
    view.put("Namespace", namespace);
    if (dimensions != null) {
      dimensions.entrySet().stream()
          .sorted(Comparator.comparing(Entry::getKey))
          .forEach(e -> view.put(e.getKey(), e.getValue()));
    }
    view.put("Statistic", statistic);
    view.put("Conversion", conversion);
    if (expression != null) {
      view.put("expression", expression);
    }
    return new ArrayList<>(view.entrySet());
  }

  @Override
  public String toString() {
    return (namespace != null ? "namespace=" + namespace + "\n" : "")
        + (name != null ? "name=" + name + "\n" : "")
        + (statistic != null ? "statistic=" + statistic + "\n" : "")
        + (dimensions != null ? "dimensions=" + dimensions + "\n" : "")
        + (conversion != null ? "conversion=" + conversion + "\n" : "")
        + (expression != null ? "expression=" + expression : "");
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        conversion, dimensions, expression, label, name, namespace, statistic, visible);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    DataSeriesConfig other = (DataSeriesConfig) obj;
    return Objects.equals(conversion, other.conversion)
        && Objects.equals(dimensions, other.dimensions)
        && Objects.equals(expression, other.expression)
        && Objects.equals(label, other.label)
        && Objects.equals(name, other.name)
        && Objects.equals(namespace, other.namespace)
        && Objects.equals(statistic, other.statistic)
        && visible == other.visible;
  }

  static DataSeriesConfig defaultConfig() {
    var cfg = new DataSeriesConfig();
    cfg.setName("Invocations");
    cfg.setConversion(1L);
    cfg.setLabel("Lambda invocations");
    cfg.setNamespace("AWS/Lambda");
    cfg.setDimensions(Map.of("FunctionName", "aws-lambda-function"));
    cfg.setStatistic("Sum");
    return cfg;
  }
}

/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import static com.milosz.apps.cm.common.DateUtils.of;
import static com.milosz.apps.cm.common.DateUtils.toMiliseconds;
import static java.util.logging.Level.FINE;

import com.milosz.apps.cm.common.LogUtils;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingWorker;
import software.amazon.awssdk.services.cloudwatch.model.Dimension;
import software.amazon.awssdk.services.cloudwatch.model.GetMetricDataRequest;
import software.amazon.awssdk.services.cloudwatch.model.Metric;
import software.amazon.awssdk.services.cloudwatch.model.MetricDataQuery;
import software.amazon.awssdk.services.cloudwatch.model.MetricDataResult;
import software.amazon.awssdk.services.cloudwatch.model.MetricStat;

/** */
public class CloudWatchDataWorker extends SwingWorker<List<MetricXYSeries>, MetricDataResult> {

  private static final Logger LOG =
      LogUtils.getLogger(FINE, CloudWatchDataWorker.class.getSimpleName());
  private final ZonedDateTime startRange, endRange;
  private final List<MetricDataQuery> queries;
  private final List<ZonedDateTime> timestamps;
  private final LinkedList<Exception> errors = new LinkedList<>();
  private final SeriesDataReader reader;
  private final Map<String, MetricXYSeries> series;
  private final TimePeriod period;

  public CloudWatchDataWorker(
      ChartMetrics metrics,
      ZonedDateTime startRange,
      ZonedDateTime endRange,
      TimePeriod timePeriod,
      SeriesDataReader reader) {
    super();
    LOG.info("Init: S=%s E=%s".formatted(startRange, endRange));
    this.endRange = timePeriod.ceil(endRange);
    final var start = timePeriod.floor(startRange);
    this.startRange =
        start.equals(this.endRange) ? start.minusSeconds(timePeriod.getSeconds()) : start;
    this.queries = metricQueries(timePeriod, metrics);
    this.timestamps = genTimestamps(this.startRange, this.endRange, timePeriod);
    this.reader = reader;
    this.period = timePeriod;
    this.series = new TreeMap<>();
    for (var x = 0; x < metrics.count(); x++) {
      final var m = metrics.get(x);
      if (m.isVisible()) {
        var sk = m.resolveSeriesKey();
        series.put(sk, new MetricXYSeries(sk, m.getLabel(), 1d / m.getConversion()));
      }
    }
  }

  public TimePeriod getPeriod() {
    return period;
  }

  private List<ZonedDateTime> genTimestamps(
      ZonedDateTime start, ZonedDateTime end, TimePeriod timePeriod) {
    var next = start;
    final var values = new ArrayList<ZonedDateTime>();
    while (next.compareTo(end) <= 0) {
      values.add(next);
      next = next.plusSeconds(timePeriod.getSeconds());
    }
    Collections.reverse(values);
    return values;
  }

  private List<Datapoint> getDatapoints(MetricDataResult mdr) {
    final var vals = mdr.values();
    final var tss = mdr.timestamps();
    var ti = 0;
    final var len = tss.size();
    final var dps = new ArrayList<Datapoint>();
    for (final var gi : timestamps) {
      if (ti == len) {
        dps.add(Datapoint.of(gi, 0));
        continue;
      }
      final var dt = of(tss.get(ti));
      final var cmp = gi.compareTo(dt);
      if (cmp > 0) {
        dps.add(Datapoint.of(gi, 0));
      } else if (cmp == 0) {
        dps.add(Datapoint.of(gi, vals.get(ti)));
        ti++;
      }
    }

    return dps;
  }

  @Override
  protected List<MetricXYSeries> doInBackground() throws Exception {
    if (queries.isEmpty()) {
      return List.of();
    }
    try {
      LOG.fine("S=%s, E=%s".formatted(startRange, endRange));
      final var cwRequest =
          GetMetricDataRequest.builder()
              .metricDataQueries(queries)
              .endTime(endRange.toInstant())
              .startTime(startRange.toInstant())
              .build();
      final var iter = reader.getMetricDataPaginator(cwRequest);
      for (final var data : iter) {
        data.metricDataResults().forEach(this::publish);
      }
      return new ArrayList<>(series.values());
    } catch (final Exception e) {
      errors.add(e);
      LOG.log(Level.WARNING, "Exception", e);
      throw e;
    }
  }

  public Optional<Exception> getLastError() {
    return Optional.ofNullable(errors.peekLast());
  }

  @Override
  protected void process(List<MetricDataResult> chunks) {
    for (final var dataSeries : chunks) {
      final var seriesKey = dataSeries.id();
      final var ds = series.get(seriesKey);
      if (ds != null) {
        getDatapoints(dataSeries)
            .forEach(
                item -> {
                  ds.add(item.dateTime(), item.value());
                });
      }
    }
  }

  private static MetricDataQuery newMetricQuery(
      TimePeriod timePeriod, DataSeriesConfig metricSpec) {
    final var dimensions =
        metricSpec.getDimensions().entrySet().stream()
            .map(e -> Dimension.builder().name(e.getKey()).value(e.getValue()).build())
            .toList();
    final var metric =
        Metric.builder()
            .metricName(metricSpec.getName())
            .namespace(metricSpec.getNamespace())
            .dimensions(dimensions)
            .build();
    final var stat =
        MetricStat.builder()
            .metric(metric)
            .stat(metricSpec.getStatistic())
            .period((int) timePeriod.getSeconds())
            .build();
    return MetricDataQuery.builder()
        .metricStat(stat)
        .id(metricSpec.resolveSeriesKey())
        .returnData(true)
        .build();
  }

  private static List<MetricDataQuery> metricQueries(
      TimePeriod timePeriod, ChartMetrics cwMetrics) {
    final var queries = new ArrayList<MetricDataQuery>();
    for (var m = 0; m < cwMetrics.count(); m++) {
      final var cm = cwMetrics.get(m);
      if (cm.getExpression() == null || cm.getExpression().isBlank()) {
        queries.add(newMetricQuery(timePeriod, cm));
      } else {
        queries.add(
            MetricDataQuery.builder()
                .expression(cm.getExpression())
                .id(cm.resolveSeriesKey())
                .returnData(true)
                .build());
      }
    }
    return queries;
  }

  record Datapoint(ZonedDateTime dateTime, double value) {

    long timestamp() {
      return toMiliseconds(dateTime);
    }

    static Datapoint of(ZonedDateTime instant, double value) {
      return new Datapoint(instant, value);
    }
  }

  public DtRange getDateRange() {
    return new DtRange(startRange, endRange);
  }
}

/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import static java.util.logging.Level.FINE;

import com.milosz.apps.cm.common.LogUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import javax.swing.SwingWorker;
import software.amazon.awssdk.services.cloudwatch.CloudWatchClient;
import software.amazon.awssdk.services.cloudwatch.model.ListMetricsRequest;
import software.amazon.awssdk.services.cloudwatch.model.RecentlyActive;

/** */
public class MetricListReader extends SwingWorker<Void, MetricSpec> {

  private static final Logger LOG =
      LogUtils.getLogger(FINE, MetricListReader.class.getSimpleName());
  private static final Map<String, List<MetricSpec>> METRIC_REG = new HashMap<>();

  private final MetricTable tableModel;
  private final CloudWatchClient client;
  private final List<MetricSpec> metrics = new ArrayList<>();

  private final String regKey;

  public MetricListReader(
      MetricTable tableModel, CloudWatchClient client, String awsRegion, String awsProfile) {
    super();
    this.tableModel = tableModel;
    this.client = client;
    regKey = "%s:%s".formatted(awsProfile, awsRegion);
  }

  @Override
  protected Void doInBackground() throws Exception {
    try {
      tableModel.clear();
      metricStream().peek(metrics::add).forEach(this::publish);
      METRIC_REG.put(regKey, metrics);
    } catch (final Exception e) {
      LOG.warning("Can't read list of metrics " + e.getMessage());
    }
    return null;
  }

  private Stream<MetricSpec> metricStream() {
    if (METRIC_REG.containsKey(regKey)) {
      LOG.info("Using local copy");
      return METRIC_REG.get(regKey).stream();
    }
    return StreamSupport.stream(
            client
                .listMetricsPaginator(
                    ListMetricsRequest.builder().recentlyActive(RecentlyActive.PT3_H).build())
                .metrics()
                .spliterator(),
            false)
        .map(MetricSpec::of);
  }

  @Override
  protected void process(List<MetricSpec> chunks) {
    tableModel.addRow(chunks);
  }
}

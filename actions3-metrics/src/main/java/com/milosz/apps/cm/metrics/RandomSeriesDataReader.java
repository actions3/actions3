/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import com.milosz.apps.cm.common.LogUtils;
import java.security.SecureRandom;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import software.amazon.awssdk.services.cloudwatch.model.GetMetricDataRequest;
import software.amazon.awssdk.services.cloudwatch.model.GetMetricDataResponse;
import software.amazon.awssdk.services.cloudwatch.model.MetricDataQuery;
import software.amazon.awssdk.services.cloudwatch.model.MetricDataResult;

/** */
public class RandomSeriesDataReader implements SeriesDataReader {

  private static final Logger LOG =
      LogUtils.getLogger(Level.FINE, RandomSeriesDataReader.class.getSimpleName());

  private final Random rand = new SecureRandom();

  private final TimePeriod period;

  public RandomSeriesDataReader(TimePeriod period) {
    super();
    this.period = period;
  }

  @Override
  public Iterable<GetMetricDataResponse> getMetricDataPaginator(GetMetricDataRequest request) {
    LOG.warning("Random data generator");
    final var start = request.startTime();
    final var end = request.endTime();
    final var results =
        request.metricDataQueries().stream().map(q -> random(q, start, end)).toList();
    final var response = GetMetricDataResponse.builder().metricDataResults(results).build();

    return List.of(response);
  }

  private MetricDataResult random(MetricDataQuery query, Instant start, Instant end) {
    final var base = rand.nextInt(100, 1000000);
    final List<Instant> timestamps = new ArrayList<>();
    final List<Double> values = new ArrayList<>();
    var s = start;
    while (!s.isAfter(end)) {
      timestamps.add(s);
      final var v = Math.abs(rand.nextGaussian() * base);
      values.add(v);
      s = s.plusSeconds(period.getSeconds());
    }
    Collections.reverse(timestamps);
    return MetricDataResult.builder().id(query.id()).timestamps(timestamps).values(values).build();
  }
}

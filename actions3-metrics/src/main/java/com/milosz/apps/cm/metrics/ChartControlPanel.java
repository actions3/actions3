/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import java.awt.Dimension;
import javax.swing.JTabbedPane;

/** */
public class ChartControlPanel extends JTabbedPane {

  public ChartControlPanel(int tabPlacement) {
    super(tabPlacement);
  }

  @Override
  public Dimension getMinimumSize() {
    return new Dimension();
  }

  @Override
  public Dimension getPreferredSize() {
    return new Dimension();
  }

  @Override
  public Dimension getMaximumSize() {
    return new Dimension();
  }
}

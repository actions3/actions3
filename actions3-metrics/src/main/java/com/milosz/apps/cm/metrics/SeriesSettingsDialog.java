/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import static java.awt.GridBagConstraints.BOTH;
import static java.awt.GridBagConstraints.RELATIVE;
import static java.awt.GridBagConstraints.REMAINDER;
import static javax.swing.JOptionPane.getRootFrame;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;

/** */
public class SeriesSettingsDialog extends JDialog implements ActionListener {

  private final Map<String, Object> values = new HashMap<>();
  private final SeriesSettingsPanel settingsPanel = new SeriesSettingsPanel();

  public SeriesSettingsDialog(Frame owner) {
    super(owner);
    setLayout(new BorderLayout());
    add(new JLabel("Set series settings"), BorderLayout.PAGE_START);
    add(settingsPanel, BorderLayout.CENTER);
    final var box = Box.createHorizontalBox();
    final var btnOk = new JButton("OK");
    btnOk.setActionCommand("$CMD_OK");
    btnOk.addActionListener(this);
    box.add(btnOk);

    final var btnCancel = new JButton("Cancel");
    btnCancel.addActionListener(this);
    btnCancel.setActionCommand("$CMD_CANCEL");
    box.add(btnCancel);
    box.add(Box.createHorizontalGlue());
    add(box, BorderLayout.PAGE_END);

    setMinimumSize(new Dimension(300, 200));
    setLocationRelativeTo(owner);
    setTitle("Series settings");
  }

  private static class SeriesSettingsPanel extends JPanel {
    /** */
    public SeriesSettingsPanel() {
      setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
      setLayout(new GridBagLayout());
      final var gbc = new GridBagConstraints();
      gbc.weightx = 1;
      gbc.weighty = 1;
      gbc.fill = BOTH;
      gbc.insets = new Insets(5, 5, 5, 5);
      addText("Label", "Label", gbc);
      addText("Statistic", "Statistic", gbc);
      addNumber("Conversion", "Conversion", gbc);
    }

    private void addLine(String label, Component c, GridBagConstraints gbc) {
      gbc.gridwidth = RELATIVE;
      gbc.weightx = 0;
      add(new JLabel(label), gbc);
      gbc.weightx = 1;
      gbc.gridwidth = REMAINDER;
      add(c, gbc);
    }

    private Object getValue(String name) {
      final var c =
          Arrays.stream(getComponents()).filter(v -> name.equals(v.getName())).findFirst().get();
      if (c instanceof final JTextField tf) {
        return tf.getText();
      } else if (c instanceof final JSpinner sp) {
        return sp.getValue();
      }
      return null;
    }

    private JTextField addText(String label, String name, GridBagConstraints gbc) {
      final var txt = new JTextField();
      txt.setMinimumSize(txt.getPreferredSize());
      txt.setName(name);
      addLine(label, txt, gbc);
      return txt;
    }

    private JSpinner addNumber(String label, String name, GridBagConstraints gbc) {
      final var c = new JSpinner();
      c.setValue(1);
      c.setMinimumSize(c.getPreferredSize());
      c.setName(name);
      addLine(label, c, gbc);
      return c;
    }
  }

  public static Map<String, Object> showDialog() {
    final var dialog = new SeriesSettingsDialog(getRootFrame());
    dialog.setModal(true);
    dialog.setVisible(true);
    return dialog.values;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    switch (e.getActionCommand()) {
      case "$CMD_OK":
        values.put("Label", settingsPanel.getValue("Label"));
        values.put("Statistic", settingsPanel.getValue("Statistic"));
        values.put("Conversion", settingsPanel.getValue("Conversion"));
        break;
      case "CMD_CANCEL":
        values.clear();
        break;
    }
    setVisible(false);
  }
}

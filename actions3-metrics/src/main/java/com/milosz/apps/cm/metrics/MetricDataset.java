/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.jfree.data.xy.AbstractXYDataset;

/** */
public class MetricDataset extends AbstractXYDataset {

  private final List<Value> values;
  private String seriesKey = "";
  private String seriesLabel = "";

  public MetricDataset() {
    values = new ArrayList<>();
  }

  @Override
  public int getItemCount(int series) {
    return values.size();
  }

  @Override
  public Number getX(int series, int item) {
    return values.get(item).dateTime.toInstant().toEpochMilli();
  }

  @Override
  public Number getY(int series, int item) {
    return values.get(item).value;
  }

  @Override
  public int getSeriesCount() {
    return 1;
  }

  @SuppressWarnings("rawtypes")
  @Override
  public Comparable getSeriesKey(int series) {
    return seriesKey;
  }

  public String getSeriesKey() {
    return seriesKey;
  }

  public String getSeriesLabel() {
    return seriesLabel;
  }

  public void setSeriesLabel(String seriesLabel) {
    this.seriesLabel = seriesLabel;
  }

  public void setSeriesKey(String seriesKey) {
    this.seriesKey = seriesKey;
  }

  public void reset() {
    values.clear();
    seriesKey = "";
    seriesLabel = "";
  }

  public boolean isUsed() {
    return !(values.isEmpty() && seriesKey.isBlank() && seriesLabel.isBlank());
  }

  public Number getMinX() {
    return values.get(0).timestamp;
  }

  public Number getMaxX() {
    return values.get(values.size()).timestamp;
  }

  public double getMinY() {
    return values.stream().mapToDouble(v -> v.value).min().getAsDouble();
  }

  public double getMaxY() {
    return values.stream().mapToDouble(v -> v.value).max().getAsDouble();
  }

  public Optional<Value> getNearestY(double x) {
    return values.stream()
        .min(
            (v1, v2) -> {
              return Double.compare(Math.abs(x - v1.timestamp), Math.abs(x - v2.timestamp));
            });
  }

  public void addAll(List<Value> other) {
    this.values.addAll(other);
  }

  public boolean isEmpty() {
    return this.values.isEmpty();
  }

  public static class Value {
    private final ZonedDateTime dateTime;
    private final double value;
    private final long timestamp;

    public Value(ZonedDateTime dateTime, double value) {
      this.dateTime = dateTime;
      this.value = value;
      this.timestamp = dateTime.toInstant().toEpochMilli();
    }

    public ZonedDateTime getDateTime() {
      return dateTime;
    }

    public double getValue() {
      return value;
    }

    public long getTimestamp() {
      return timestamp;
    }

    @Override
    public int hashCode() {
      return Objects.hash(timestamp);
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final var other = (Value) obj;
      return timestamp == other.timestamp;
    }

    @Override
    public String toString() {
      return "Value ["
          + (dateTime != null ? "dateTime=" + dateTime + ", " : "")
          + "value="
          + value
          + ", timestamp="
          + timestamp
          + "]";
    }
  }
}

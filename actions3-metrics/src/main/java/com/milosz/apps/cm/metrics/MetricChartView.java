/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import static com.milosz.apps.cm.common.DateUtils.of;
import static com.milosz.apps.cm.common.DateUtils.toDate;
import static com.milosz.apps.cm.common.QueryParamsFormDialog.Param.date;
import static com.milosz.apps.cm.common.SwingSupport.showErrorDialog;
import static com.milosz.apps.cm.metrics.SeriesDataReader.aws;
import static com.milosz.apps.cm.metrics.SeriesDataReader.random;
import static com.milosz.apps.cm.metrics.TimePeriod.DAY_1;
import static com.milosz.apps.cm.metrics.TimePeriod.HOUR_1;
import static com.milosz.apps.cm.metrics.TimePeriod.MINUTES_15;
import static com.milosz.apps.cm.metrics.TimePeriod.MINUTES_5;
import static java.time.ZonedDateTime.now;
import static java.util.logging.Level.FINE;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import static org.jfree.chart.ChartPanel.DEFAULT_HEIGHT;
import static org.jfree.chart.ChartPanel.DEFAULT_MAXIMUM_DRAW_HEIGHT;
import static org.jfree.chart.ChartPanel.DEFAULT_MAXIMUM_DRAW_WIDTH;
import static org.jfree.chart.ChartPanel.DEFAULT_MINIMUM_DRAW_HEIGHT;
import static org.jfree.chart.ChartPanel.DEFAULT_MINIMUM_DRAW_WIDTH;
import static org.jfree.chart.ChartPanel.DEFAULT_WIDTH;
import static org.jfree.chart.plot.DefaultDrawingSupplier.DEFAULT_FILL_PAINT_SEQUENCE;
import static org.jfree.chart.plot.DefaultDrawingSupplier.DEFAULT_OUTLINE_PAINT_SEQUENCE;
import static org.jfree.chart.plot.DefaultDrawingSupplier.DEFAULT_OUTLINE_STROKE_SEQUENCE;
import static org.jfree.chart.plot.DefaultDrawingSupplier.DEFAULT_SHAPE_SEQUENCE;
import static org.jfree.chart.plot.DefaultDrawingSupplier.DEFAULT_STROKE_SEQUENCE;

import com.milosz.apps.cm.common.DateUtils;
import com.milosz.apps.cm.common.LogUtils;
import com.milosz.apps.cm.common.QueryParamsFormDialog;
import com.milosz.apps.cm.common.QueryParamsFormDialog.InputValue;
import com.milosz.apps.cm.spi.AppActionContainer;
import com.milosz.apps.cm.spi.AppView;
import com.milosz.apps.cm.spi.AppViewConfig;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.text.DecimalFormat;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingWorker.StateValue;
import javax.swing.Timer;
import org.jfree.chart.ChartColor;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.event.AxisChangeEvent;
import org.jfree.chart.event.AxisChangeListener;
import org.jfree.chart.labels.XYSeriesLabelGenerator;
import org.jfree.chart.plot.DefaultDrawingSupplier;
import org.jfree.chart.plot.DrawingSupplier;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.Range;
import org.jfree.data.time.DateRange;
import org.jfree.data.xy.XYDataset;
import software.amazon.awssdk.core.exception.SdkException;

public class MetricChartView implements AppView, PropertyChangeListener, AxisChangeListener {
  private static final Logger LOG = LogUtils.getLogger(FINE, MetricChartView.class.getSimpleName());
  private final MetricViewConfig context;
  private final PropertyChangeSupport changeSupport;
  private JSplitPane viewComponent;
  private JFreeChart chart;
  private ChartPanel chartPanel;
  private ChartSeriesPanel seriesList;
  private final ChartMetrics metrics;
  private final AccountMetricsPanel availableMetrics;
  private SeriesTraceOverlay trace;
  private Timer refreshTimer;

  private TimePeriod currentPeriod;
  private final TimePeriod defaultPeriod;

  private DtRange currentDateRange;

  private final AtomicBoolean controlPanelVisible = new AtomicBoolean();

  public MetricChartView(MetricViewConfig context) {
    this.context = context;
    this.changeSupport = new PropertyChangeSupport(this);
    this.metrics = new ChartMetrics(context.getMetrics());
    defaultPeriod = context.getTimePeriod();
    metrics.getChangeSupport().addPropertyChangeListener(this);
    availableMetrics =
        new AccountMetricsPanel(context.getAwsProfile(), context.getAwsRegion(), metrics);
  }

  @Override
  public Component component() {
    return viewComponent;
  }

  void zoomReset() {
    final var end = now();
    final var start = end.minus(context.getDuration());
    changeRange(start, end, context.getTimePeriod());
  }

  void refresh() {
    if (currentPeriod == defaultPeriod) {
      final var end = now();
      changeRange(currentDateRange.start(), end, currentPeriod, false);
    }
  }

  void refreshCurrent() {
    changeRange(currentDateRange.start(), currentDateRange.end(), currentPeriod, false);
  }

  @Override
  public void shutdown() {
    if (refreshTimer != null) {
      refreshTimer.stop();
      final var copy = List.of(refreshTimer.getActionListeners());
      for (final var a : copy) {
        refreshTimer.removeActionListener(a);
      }
      refreshTimer = null;
    }
  }

  public void changeTimeRange() {
    final var parameters =
        List.of(
            date("Start date", currentDateRange.start()), date("End date", currentDateRange.end()));

    final var values = QueryParamsFormDialog.showParamsDialog(parameters);
    if (values.isEmpty()) {
      return;
    }
    final var dates =
        values.stream()
            .filter(p -> p.name().contains("DATE"))
            .collect(Collectors.toMap(InputValue::name, iv -> iv.value().toString()));
    final var start = DateUtils.parseLocalInput(dates.get("START_DATE"));
    final var end = DateUtils.parseLocalInput(dates.get("END_DATE"));
    changeRange(start, end, currentPeriod);
  }

  private DrawingSupplier drawingSupplier() {
    return new DefaultDrawingSupplier(
        new Color[] {
          ChartColor.DARK_RED,
          ChartColor.DARK_BLUE,
          ChartColor.DARK_CYAN,
          Color.DARK_GRAY,
          ChartColor.DARK_YELLOW,
          ChartColor.DARK_GREEN,
          ChartColor.DARK_MAGENTA,
          Color.GRAY,
          new Color(0xFF, 0x55, 0x55),
          new Color(0x55, 0x55, 0xFF),
          new Color(0xFF, 0x55, 0xFF),
          new Color(0x55, 0xFF, 0xFF),
          Color.PINK,
        },
        DEFAULT_FILL_PAINT_SEQUENCE,
        DEFAULT_OUTLINE_PAINT_SEQUENCE,
        DEFAULT_STROKE_SEQUENCE,
        DEFAULT_OUTLINE_STROKE_SEQUENCE,
        DEFAULT_SHAPE_SEQUENCE);
  }

  @Override
  public void start() {
    this.chart = setupChart(context.getName());
    resetPlot();
    this.chartPanel =
        new ChartPanel(
            chart,
            DEFAULT_WIDTH,
            DEFAULT_HEIGHT,
            DEFAULT_MINIMUM_DRAW_WIDTH,
            DEFAULT_MINIMUM_DRAW_HEIGHT,
            DEFAULT_MAXIMUM_DRAW_WIDTH,
            DEFAULT_MAXIMUM_DRAW_HEIGHT,
            true,
            false,
            false,
            false,
            false,
            false,
            false);
    this.seriesList = new ChartSeriesPanel(metrics);
    final var metricTabs = new ChartControlPanel(JTabbedPane.TOP);
    metricTabs.setMinimumSize(new Dimension());
    metricTabs.insertTab("Visible", null, seriesList, "", 0);
    metricTabs.insertTab("Available", null, availableMetrics, "", 1);
    viewComponent = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, chartPanel, metricTabs);
    viewComponent.setResizeWeight(1);
    viewComponent.setOneTouchExpandable(true);
    final var end = ZonedDateTime.now();
    final var start = end.minusSeconds(context.getDuration().getSeconds());
    loadData(context.getTimePeriod(), start, end);
    this.trace = new SeriesTraceOverlay(chartPanel, this);
    this.chartPanel.addMouseMotionListener(trace);
    this.chartPanel.addMouseListener(trace);
    this.chartPanel.addOverlay(trace);
    if (refreshTimer != null) {
      refreshTimer.start();
    }
  }

  private JFreeChart setupChart(String title) {
    final var plot = initPlot();
    return new JFreeChart(title, JFreeChart.DEFAULT_TITLE_FONT, plot, true);
  }

  private XYPlot initPlot() {
    final var xAxis = new DateAxis();
    final var plot = new XYPlot(null, xAxis, null, null);
    plot.setDomainGridlinesVisible(false);
    plot.setDomainMinorGridlinesVisible(false);
    plot.setRangeGridlinesVisible(false);
    plot.setRangeGridlinesVisible(false);
    final var supplier = drawingSupplier();
    plot.setDrawingSupplier(supplier);
    xAxis.addChangeListener(this);

    for (var i = 0; i < 10; i++) {
      plot.setDataset(i, new MetricDataset());
      final var yAxis = new TraceNumberAxis();
      yAxis.setAutoRange(true);
      yAxis.setNumberFormatOverride(new DecimalFormat("0.00"));
      plot.setRangeAxis(i, yAxis, false);

      final var renderer = new XYLineAndShapeRenderer(true, false);
      final var paint = supplier.getNextPaint();
      renderer.setSeriesPaint(0, paint);
      renderer.setDefaultStroke(new BasicStroke(1.5f));
      yAxis.setAxisLinePaint(paint);
      yAxis.setAxisLineStroke(new BasicStroke(1.5f));
      renderer.setLegendItemLabelGenerator(
          new XYSeriesLabelGenerator() {

            @Override
            public String generateLabel(XYDataset dataset, int series) {
              if (dataset instanceof final MetricDataset ds) {
                return ds.getSeriesLabel();
              }
              return dataset.getSeriesKey(series).toString();
            }
          });
      plot.mapDatasetToRangeAxis(i, i);
      plot.setRenderer(i, renderer, false);
    }

    return plot;
  }

  private XYPlot resetPlot() {
    final var plot = chart.getXYPlot();

    for (var i = 0; i < 10; i++) {
      plot.getRangeAxis(i).setVisible(false);
      plot.getRenderer(i).setDefaultItemLabelsVisible(false);
      plot.getRenderer(i).setDefaultSeriesVisible(false);
      getDataset(i).reset();
      plot.setRangeAxisLocation(i, AxisLocation.BOTTOM_OR_LEFT);
    }
    return plot;
  }

  private MetricDataset getDataset(int index) {
    return (MetricDataset) chart.getXYPlot().getDataset(index);
  }

  private void loadData(TimePeriod period, ZonedDateTime start, ZonedDateTime end) {
    final var rd = System.getProperty("random.data", "false");
    final var reader =
        "false".equals(rd) ? aws(context.getAwsProfile(), context.getAwsRegion()) : random(period);
    this.currentPeriod = period;
    final var worker = new CloudWatchDataWorker(metrics, start, end, period, reader);
    worker.addPropertyChangeListener(this);
    worker.execute();
  }

  @Override
  public PropertyChangeSupport changeSupport() {
    return changeSupport;
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (evt.getPropertyName().equals("state") && evt.getNewValue().equals(StateValue.DONE)) {
      if (evt.getSource() instanceof final CloudWatchDataWorker worker) {
        final var lastError = worker.getLastError();
        if (lastError.isPresent()) {
          var lastMsg = lastError.get().getMessage();
          if (lastMsg.length() > 100) {
            lastMsg = lastMsg.substring(0, 100) + "...";
          }
          var msg =
              "Couldn't fetch data. Check logs for more details<br>Last error: %s"
                  .formatted(lastMsg);
          if (lastError.get() instanceof SdkException) {
            msg += "<ul>";
            msg += "<li>AWS profile: %s".formatted(context.getAwsProfile());
            msg += "<li><p>AWS region: %s".formatted(context.getAwsRegion());
            msg += "</ul>";
          }
          showErrorDialog(chartPanel, msg, lastError.get());
        } else {
          dataSeriesLoaded(worker);
        }
      }
    } else if (evt.getPropertyName().equals("metrics")) {
      loadData(currentPeriod, currentDateRange.start(), currentDateRange.end());
    }
  }

  private void dataSeriesLoaded(CloudWatchDataWorker worker) {
    try {
      final var plot = resetPlot();
      final var visibleSeries = worker.get();
      final List<MetricDataset> used = new ArrayList<>();
      for (var v = 0; v < visibleSeries.size(); v++) {
        final var vs = visibleSeries.get(v);
        final var ds = getDataset(v);
        ds.addAll(vs.getValues());
        ds.setSeriesKey(vs.getKey().toString());
        used.add(ds);

        plot.getRenderer(v).setDefaultSeriesVisible(true);
        plot.getRenderer(v).setDefaultItemLabelsVisible(true);
        plot.getRenderer(v).setDefaultSeriesVisibleInLegend(true);

        final var ay = plot.getRangeAxis(v);
        ay.setVisible(true);
        ay.setLabel(vs.getLabel());
        ds.setSeriesLabel(vs.getLabel());

        final var axisLocation =
            v % 2 == 0 ? AxisLocation.BOTTOM_OR_LEFT : AxisLocation.BOTTOM_OR_RIGHT;
        plot.setRangeAxisLocation(v, axisLocation);
        adjustAxisRange(ay, ds);
      }
      currentDateRange = worker.getDateRange();
      changeSupport.firePropertyChange("data", null, this);
    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
      LOG.log(Level.WARNING, "Execution failure ", e);
      throw new RuntimeException("Loading data has failed");
    }
  }

  private void adjustAxisRange(ValueAxis ay, MetricDataset ds) {
    final var lower = ds.getMinY();
    final var upper = ds.getMaxY();
    final var margin = (upper - lower) * 0.1;
    final var range = new Range(lower - margin, upper + margin);
    if (range.getLength() > 0) {
      ay.setRange(range);
    }
  }

  @Override
  public void axisChanged(AxisChangeEvent event) {
    final var da = (DateAxis) event.getAxis();
    final var dr = (DateRange) da.getRange();
    final var duration = Duration.ofMillis(dr.getUpperMillis() - dr.getLowerMillis());
    var period = DAY_1;
    if (duration.compareTo(Duration.ofHours(12)) < 0) {
      period = MINUTES_5;
    } else if (duration.compareTo(Duration.ofHours(24)) < 0) {
      period = MINUTES_15;
    } else if (duration.compareTo(Duration.ofDays(7)) <= 0) {
      period = HOUR_1;
    }
    loadData(period, of(dr.getLowerDate()), of(dr.getUpperDate()));
  }

  void controlPanel() {
    final var visible = controlPanelVisible.get();
    if (!visible) {
      availableMetrics.populate();
    }
    final var div = visible ? 1 : 0.9;
    viewComponent.setDividerLocation(div);
    viewComponent.revalidate();
    controlPanelVisible.setRelease(!visible);
  }

  public void move(int step) {
    final var axis = chart.getXYPlot().getDomainAxis();
    final var ms = currentPeriod.getSeconds() * step;
    final var range =
        new DtRange(
            currentDateRange.start().plusSeconds(ms), currentDateRange.end().plusSeconds(ms));
    axis.setRange(range.toChart());
  }

  private void fireStatusUpdate(String status) {
    changeSupport.firePropertyChange(AppView.STATUS_PROP, null, status);
  }

  void changeRange(DateRange range, TimePeriod period) {
    currentPeriod = period;
    chart.getXYPlot().getDomainAxis().setRange(range);
  }

  void changeRange(ZonedDateTime start, ZonedDateTime end, TimePeriod period) {
    changeRange(start, end, period, true);
  }

  void changeRange(ZonedDateTime start, ZonedDateTime end, TimePeriod period, boolean reset) {
    if (reset) {
      resetPlot();
    }
    changeRange(new DateRange(toDate(start), toDate(end)), period);
  }

  void changeRelativeRange() {
    final var opt =
        JOptionPane.showInputDialog(
            chartPanel,
            "Select relative time range",
            "Relative time range",
            INFORMATION_MESSAGE,
            null,
            TimePeriod.values(),
            TimePeriod.HOUR_1);
    if (opt != null && opt instanceof final TimePeriod period) {
      final var end = now();
      changeRange(end.minusSeconds(period.getSeconds()), end, period);
    }
  }

  MetricDataset selectDataSeries(String type, List<MetricDataset> used) {
    final var visible = new TreeMap<String, MetricDataset>();
    final var plot = chart.getXYPlot();
    for (final var u : used) {
      final var dsIndex = plot.indexOf(u);
      final var rend = (XYLineAndShapeRenderer) plot.getRenderer(dsIndex);
      if (rend.isSeriesVisible(0)) {
        visible.put(u.getSeriesLabel(), u);
      }
    }
    final var options = visible.keySet().toArray();
    final var opt =
        JOptionPane.showInputDialog(
            chartPanel,
            "Select data series from list",
            type + " data series",
            INFORMATION_MESSAGE,
            null,
            options,
            options[0]);
    if (opt != null) {
      return visible.get(opt);
    }
    return null;
  }

  void range(double d) {
    final var plot = chart.getXYPlot();
    for (var i = 0; i < 10; i++) {
      final var ds = getDataset(i);
      final var axis = plot.getRangeAxis(i);
      if (axis.isVisible()) {
        final var lower = ds.getMinY();
        final var upper = ds.getMaxY();
        final var range = axis.getRange();
        final var margin = (range.getLength() - (upper - lower)) / 2 * (1 + d);
        axis.setRange(lower - margin, upper + margin);
      }
    }
  }

  private List<MetricDataset> getUsedDatasets() {
    final var used = new ArrayList<MetricDataset>();
    final var plot = chart.getXYPlot();
    for (var i = 0; i < plot.getDatasetCount(); i++) {
      final var ds = getDataset(i);
      if (ds.isUsed()) {
        used.add(ds);
      }
    }
    return used;
  }

  public void showOnlySeries() {
    final var plot = chart.getXYPlot();
    final var used = getUsedDatasets();
    final var selecteDs = selectDataSeries("Show", used);
    if (selecteDs == null) {
      return;
    }
    final var selectedIndex = plot.indexOf(selecteDs);
    for (var c = 0; c < used.size(); c++) {
      final var ds = used.get(c);
      final var plotIndex = plot.indexOf(ds);
      final var renderer = plot.getRenderer(plotIndex);
      final var visible = (plotIndex == selectedIndex);
      renderer.setSeriesVisible(0, visible, true);
    }
    fireStatusUpdate("Show " + selecteDs.getSeriesLabel());
  }

  public void hideSingleSeries() {
    final var plot = chart.getXYPlot();
    final var used = getUsedDatasets();
    final var selecteDs = selectDataSeries("Hide", used);
    if (selecteDs == null) {
      return;
    }
    final var selectedIndex = plot.indexOf(selecteDs);
    for (var c = 0; c < used.size(); c++) {
      final var ds = used.get(c);
      final var plotIndex = plot.indexOf(ds);
      final var renderer = plot.getRenderer(plotIndex);
      final var visible = (selectedIndex != plotIndex);
      renderer.setSeriesVisible(0, visible, true);
    }
    fireStatusUpdate("Hide " + selecteDs.getSeriesLabel());
  }

  public void showAllSeries() {
    final var plot = chart.getXYPlot();
    final var used = getUsedDatasets();
    for (var c = 0; c < used.size(); c++) {
      final var ds = used.get(c);
      final var plotIndex = plot.indexOf(ds);
      final var renderer = plot.getRenderer(plotIndex);
      renderer.setSeriesVisible(0, true, true);
    }
  }

  public TimePeriod getCurrentPeriod() {
    return currentPeriod;
  }

  @Override
  public AppActionContainer actions() {
    return new MetricsActionContainer(this, trace);
  }

  @Override
  public AppViewConfig viewConfig() {
    return context.withMetrics(metrics.getMetrics());
  }

  @Override
  public String type() {
    return "Metrics";
  }
}

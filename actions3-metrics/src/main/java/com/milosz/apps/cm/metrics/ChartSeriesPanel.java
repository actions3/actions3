/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import static javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS;

import com.milosz.apps.cm.common.PropertyTable;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/** */
public class ChartSeriesPanel extends JPanel implements ListSelectionListener, ActionListener {

  private final ChartMetrics metrics;
  private final JTable seriesList;
  private final JTable seriesDetails;
  private final JSplitPane split;
  private final ChartSeriesListModel seriesModel;
  private final MapDetailsModel detailsModel;

  public ChartSeriesPanel(ChartMetrics metrics) {
    super(new BorderLayout());
    this.metrics = metrics;
    this.seriesModel = new ChartSeriesListModel(metrics);
    metrics.getChangeSupport().addPropertyChangeListener(seriesModel);
    seriesList = new JTable(seriesModel);
    seriesList.getSelectionModel().addListSelectionListener(this);
    detailsModel = new MapDetailsModel();
    seriesDetails = new PropertyTable(detailsModel);
    seriesDetails.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    final var listScroll = new JScrollPane(seriesList);
    listScroll.setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_ALWAYS);
    final var detailScroll = new JScrollPane(seriesDetails);
    detailScroll.setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_ALWAYS);
    split = new JSplitPane(JSplitPane.VERTICAL_SPLIT, listScroll, detailScroll);
    split.setResizeWeight(1);
    split.setDividerLocation(0.8);
    initPanel();
  }

  private void initPanel() {
    add(split, BorderLayout.CENTER);
    final var box = Box.createHorizontalBox();
    add(box, BorderLayout.PAGE_END);

    final var remove = new JButton("Remove");
    remove.addActionListener(this);
    box.add(remove);
  }

  @Override
  public void valueChanged(ListSelectionEvent e) {
    if (!e.getValueIsAdjusting()) {
      final var selected = seriesList.getSelectedRow();
      if (selected >= 0) {
        final var series = seriesModel.getVisible(selected);
        detailsModel.setDetails(series.details());
      }
    }
  }

  @Override
  public Dimension getMinimumSize() {
    return new Dimension();
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    final var selected = seriesList.getSelectedRow();
    metrics.removeVisible(selected);
  }
}

/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.ui.RectangleEdge;

public class TraceNumberAxis extends NumberAxis {

  private Double currentValue = null;

  @Override
  protected void drawAxisLine(
      Graphics2D g2, double cursor, Rectangle2D dataArea, RectangleEdge edge) {
    super.drawAxisLine(g2, cursor, dataArea, edge);
    if (currentValue != null) {
      final var currentPoint = new Ellipse2D.Double(cursor - 4, currentValue - 4, 8, 8);

      g2.setPaint(getAxisLinePaint());
      g2.setStroke(getAxisLineStroke());
      g2.draw(currentPoint);
    }
  }

  public void setCurrentValue(Double cv) {
    if (cv != null && !cv.equals(currentValue)) {
      // fire changes only if cv is different from previous value
      // when axis change JFreeChart repaints whole plot, including
      // overlays. This might cause infinite loop when axis is again
      // updated during overlay repaint
      currentValue = cv;
      fireChangeEvent();
    }
  }
}

/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import static java.util.logging.Level.FINE;

import com.milosz.apps.cm.common.JsonException;
import com.milosz.apps.cm.common.JsonSupport;
import com.milosz.apps.cm.common.LogUtils;
import com.milosz.apps.cm.spi.AppView;
import com.milosz.apps.cm.spi.AppViewConfig;
import com.milosz.apps.cm.spi.AppViewSupport;
import com.milosz.apps.cm.spi.Application;
import com.milosz.apps.cm.spi.ValidationResult;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class MetricViewSupport implements AppViewSupport {
  private static final Logger LOG =
      LogUtils.getLogger(FINE, MetricViewSupport.class.getSimpleName());

  private final JsonSupport mapper = JsonSupport.INSTANCE;

  @Override
  public String getName() {
    return "Metrics";
  }

  @Override
  public AppView create(AppViewConfig config, Application main) {
    return new MetricChartView((MetricViewConfig) config);
  }

  @Override
  public ValidationResult validate(String config) {
    try {
      final var v = mapper.read(config, MetricViewConfig.class);
      final var errors = v.validate();
      if (errors.isEmpty()) {
        return new ValidationResult(mapper.write(v, true), null);
      } else {
        return new ValidationResult(mapper.write(v, true), errors);
      }
    } catch (final JsonException e) {
      LOG.log(Level.WARNING, "Invalid JSON", e);
      return new ValidationResult(null, List.of("Invalid JSON " + e.getOriginalMessage()));
    } catch (final Exception e) {
      LOG.log(Level.WARNING, "Can't read configuration", e);
      return new ValidationResult(null, List.of(e.getMessage()));
    }
  }

  @Override
  public Icon icon() {
    try (var is = MetricViewSupport.class.getResourceAsStream("icon.gif")) {
      return new ImageIcon(is.readAllBytes());
    } catch (final IOException e) {
      return null;
    }
  }

  @Override
  public AppViewConfig readConfig(String raw) {
    return mapper.read(raw, MetricViewConfig.class);
  }

  @Override
  public String format(String config) {
    return writeConfig(readConfig(config));
  }

  @Override
  public String writeConfig(AppViewConfig cfg) {
    return mapper.write(cfg, true);
  }

  @Override
  public String emptyConfig() {
    return writeConfig(MetricViewConfig.EMPTY);
  }
}

/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import com.milosz.apps.cm.common.ActionCommand;

public enum MetricViewCommand implements ActionCommand {
  REFRESH,
  REFRESH_CURRENT,
  MOVE_LEFT,
  MOVE_RIGHT,
  SELECT_LEFT,
  SELECT_RIGHT,
  RESET,
  CURRENT_VALUES,
  RANGE_EXPAND,
  RANGE_COLLAPSE,
  SERIES_SHOW,
  SERIES_HIDE,
  SET_MARKER,
  CLEAR_MARKERS,
  RELATIVE,
  CONTROL_PANEL,
  TIME_RANGE,
  SERIES_ALL;

  public static MetricViewCommand of(String raw) {
    return valueOf(raw.replace("$CMD_", ""));
  }
}

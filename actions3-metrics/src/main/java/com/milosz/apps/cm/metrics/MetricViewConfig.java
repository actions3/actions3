/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import static com.milosz.apps.cm.metrics.DataSeriesConfig.defaultConfig;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.milosz.apps.cm.spi.AppViewConfig;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import software.amazon.awssdk.auth.credentials.internal.ProfileCredentialsUtils;
import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.profiles.ProfileFile;

public class MetricViewConfig implements AppViewConfig {

  private static final Duration DEFAULT_PERIOD = Duration.ofHours(1);
  private static final Duration DEFAULT_DURATION = Duration.ofDays(7);

  public static final MetricViewConfig EMPTY =
      new MetricViewConfig(
          DEFAULT_DURATION,
          List.of(defaultConfig()),
          "default",
          "us-east-1",
          "AWS metrics",
          false,
          DEFAULT_PERIOD);

  private Duration duration;
  private List<DataSeriesConfig> metrics;
  private String awsProfile;
  private String awsRegion;
  private String name;
  private boolean autoRefresh;
  private Duration period;

  public MetricViewConfig() {}

  public MetricViewConfig(
      Duration duration,
      List<DataSeriesConfig> metrics,
      String awsProfile,
      String awsRegion,
      String name,
      boolean autoRefresh,
      Duration period) {
    super();
    this.duration = duration;
    this.metrics = metrics;
    this.awsProfile = awsProfile;
    this.awsRegion = awsRegion;
    this.name = name;
    this.autoRefresh = autoRefresh;
    this.period = period;
  }

  public List<String> validate() {
    final var errors = new ArrayList<String>();
    checkNull(awsProfile, "awsProfile", errors);
    checkNull(awsRegion, "awsRegion", errors);
    if (metrics.isEmpty()) {
      errors.add("Property metrics must be not empty");
    } else {
      for (var i = 0; i < metrics.size(); i++) {
        final var me = metrics.get(i).validate();
        for (final var e : me) {
          errors.add("Metric at %s: %s".formatted(i, e));
        }
      }
    }
    return errors;
  }

  private void checkNull(Object v, String name, List<String> errors) {
    if (v == null) {
      errors.add("Property %s must be defined".formatted(name));
    }
  }

  @JsonFormat(shape = Shape.STRING)
  public Duration getDuration() {
    return duration == null ? DEFAULT_DURATION : duration;
  }

  public List<DataSeriesConfig> getMetrics() {
    return metrics;
  }

  public String getAwsProfile() {
    return awsProfile;
  }

  public String getAwsRegion() {
    return awsRegion;
  }

  public boolean isAutoRefresh() {
    return autoRefresh;
  }

  @JsonFormat(shape = Shape.STRING)
  public Duration getPeriod() {
    return period == null ? DEFAULT_PERIOD : period;
  }

  public String getName() {
    return name;
  }

  @JsonProperty("timeRange")
  public void setDuration(Duration duration) {
    this.duration = duration;
  }

  public void setMetrics(List<DataSeriesConfig> metrics) {
    this.metrics = metrics;
  }

  public void setAwsProfile(String awsProfile) {
    this.awsProfile = awsProfile;
  }

  public void setAwsRegion(String awsRegion) {
    this.awsRegion = awsRegion;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setAutoRefresh(boolean autoRefresh) {
    this.autoRefresh = autoRefresh;
  }

  public void setPeriod(Duration period) {
    this.period = period;
  }

  @JsonIgnore
  public TimePeriod getTimePeriod() {
    return TimePeriod.of(period);
  }

  @Override
  public List<String> validateRuntime() {
    final var file = ProfileFile.defaultProfileFile();
    final var opt = file.profile(awsProfile);
    if (opt.isEmpty()) {
      return List.of("AWS profile '%s' doesn't exist".formatted(awsProfile));
    }
    try {
      final var utils = new ProfileCredentialsUtils(file, opt.get(), file::profile);
      utils.credentialsProvider();
    } catch (final SdkException e) {
      return List.of(e.getMessage());
    }
    return List.of();
  }

  public MetricViewConfig withMetrics(List<DataSeriesConfig> config) {
    return new MetricViewConfig(duration, config, awsProfile, awsRegion, name, autoRefresh, period);
  }
}

/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.metrics;

import static com.milosz.apps.cm.common.DateUtils.of;
import static com.milosz.apps.cm.common.DateUtils.toMiliseconds;

import com.milosz.apps.cm.metrics.MetricDataset.Value;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.text.DecimalFormat;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.jfree.chart.ChartColor;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.panel.AbstractOverlay;
import org.jfree.chart.panel.Overlay;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.ui.RectangleEdge;

/** */
public class SeriesTraceOverlay extends AbstractOverlay
    implements MouseMotionListener, Overlay, MouseListener {
  private static final DateTimeFormatter FMT = DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm");
  private static final DecimalFormat NUM_FMT = new DecimalFormat("0.00");
  private final ChartPanel chart;
  private final DateAxis xAxis;
  private final MetricChartView view;
  private boolean showCurrentValues = true;
  private final Set<Marker> markers = new HashSet<>();
  private Point lastPoint;
  private final XYPlot plot;

  public SeriesTraceOverlay(ChartPanel chart, MetricChartView view) {
    this.chart = chart;
    this.plot = chart.getChart().getXYPlot();
    xAxis = (DateAxis) plot.getDomainAxis();
    this.view = view;
  }

  public void switchCurrentValues() {
    showCurrentValues(!showCurrentValues);
  }

  public void showCurrentValues(boolean b) {
    if (showCurrentValues != b) {
      showCurrentValues = b;
      fireOverlayChanged();
    }
  }

  @Override
  public void mousePressed(MouseEvent e) {}

  @Override
  public void mouseReleased(MouseEvent e) {}

  @Override
  public void mouseEntered(MouseEvent e) {}

  @Override
  public void mouseExited(MouseEvent e) {}

  @Override
  public void mouseDragged(MouseEvent e) {}

  private static record Marker(ZonedDateTime ts) {
    public double getX(DateAxis xAxis, Rectangle2D area) {
      return xAxis.valueToJava2D(toMiliseconds(ts), area, RectangleEdge.BOTTOM);
    }
  }

  private static record MetricValue(
      String value, Paint color, double x, double y, int dsIndex, String label) {
    static MetricValue of(Value di, int dsIndex, Paint color, String label) {
      return new MetricValue(
          NUM_FMT.format(di.getValue()), color, di.getTimestamp(), di.getValue(), dsIndex, label);
    }

    @Override
    public String toString() {
      return "%s - %s".formatted(label, value);
    }
  }

  private static Rectangle2D rect(double x, double y, double width, double height) {
    return new Rectangle2D.Double(x, y, width, height);
  }

  private ZonedDateTime nearestDt(long lineTs) {
    return nearestDt(of(lineTs));
  }

  private ZonedDateTime nearestDt(ZonedDateTime lts) {
    final var cp = view.getCurrentPeriod();
    final var floor = cp.floor(lts);
    final var ceil = cp.ceil(lts);
    final var fd = Duration.between(floor, lts).abs();
    final var cd = Duration.between(ceil, lts).abs();
    return fd.compareTo(cd) < 0 ? floor : ceil;
  }

  private String nearestTs(long lineTs) {
    return FMT.format(nearestDt(lineTs));
  }

  public void setMarker() {
    final var area = chart.getScreenDataArea();
    final var ts = (long) xAxis.java2DToValue(lastPoint.getX(), area, RectangleEdge.BOTTOM);
    final var vx = nearestDt(ts);
    markers.add(new Marker(vx));
    fireOverlayChanged();
  }

  @Override
  public void mouseClicked(MouseEvent e) {
    setMarker();
  }

  public void clearMarkers() {
    markers.clear();
    fireOverlayChanged();
  }

  @Override
  public void mouseMoved(MouseEvent e) {
    final var last = e.getPoint();
    final var area = chart.getScreenDataArea();
    if (area.contains(last)) {
      lastPoint = last;
    }
    fireOverlayChanged();
  }

  private void drawSelectionRect(
      Graphics2D g2d, String timestamp, List<MetricValue> values, Rectangle2D area) {
    if (!showCurrentValues) {
      return;
    }
    g2d.setFont(g2d.getFont().deriveFont(Font.BOLD));
    final var xmax = area.getMaxX();
    final var ymin = area.getMinY();

    var wmax = 0D;
    var hmax = 0D;
    final var fm = g2d.getFontMetrics();
    final var allStr = new ArrayList<String>();
    allStr.add(timestamp);
    allStr.addAll(values.stream().map(String::valueOf).toList());

    for (final var s : allStr) {
      final var bounds = fm.getStringBounds(s, g2d);
      final var rwidth = bounds.getWidth() + 10;
      if (rwidth > wmax) {
        wmax = rwidth;
      }
      hmax += bounds.getHeight() + 3;
    }

    final var rect = rect(xmax - wmax - 20, ymin + 20, wmax + 10, hmax + 12);
    g2d.draw(rect);

    var sy = (float) (rect.getMinY() + 13);
    var si = 0;
    for (final var s : allStr) {
      final var sx = si > 0 ? rect.getMinX() + 13 : rect.getMinX() + 3;
      if (si > 0) {
        drawShape(
            g2d, rect(rect.getMinX() + 3, sy - 8, 8, 8), values.get(si - 1).color, null, true);
      }
      g2d.drawString(s, (float) sx + 3, sy);
      final var bounds = fm.getStringBounds(s, g2d);
      sy += (float) bounds.getHeight() + 3;
      si++;
    }
  }

  private void drawShape(Graphics2D g2d, Shape shape, Paint paint, Stroke stroke, boolean fill) {
    final var gp = g2d.getPaint();
    final var gs = g2d.getStroke();

    if (stroke != null) {
      g2d.setStroke(stroke);
    }

    if (paint != null) {
      g2d.setPaint(paint);
    }

    g2d.draw(shape);
    if (fill) {
      g2d.fill(shape);
    }

    g2d.setStroke(gs);
    g2d.setPaint(gp);
  }

  @Override
  public void paintOverlay(Graphics2D g2, ChartPanel chartPanel) {
    if (lastPoint == null) {
      return;
    }
    final var area = chartPanel.getScreenDataArea();
    final var ymin = area.getMinY();
    if (area.contains(lastPoint)) {
      drawShape(
          g2,
          new Line2D.Double(lastPoint.getX(), ymin, lastPoint.getX(), area.getMaxY()),
          Color.GRAY,
          new BasicStroke(0.5f),
          false);
      final var ts = (long) xAxis.java2DToValue(lastPoint.getX(), area, RectangleEdge.BOTTOM);
      final var lineItems = valuesNearLine(ts);
      drawSelectionRect(g2, nearestTs(ts), lineItems, area);
      drawCurrentValue(g2, lineItems, area);
      drawMarkers(g2, area);
    }
  }

  private void drawMarkers(Graphics2D g2, Rectangle2D area) {
    for (final var m : markers) {
      final var mx = m.getX(xAxis, area);
      if (area.contains(mx, lastPoint.getY())) {
        final var ymin = area.getMinY();
        drawShape(
            g2,
            new Line2D.Double(mx, ymin, mx, area.getMaxY()),
            ChartColor.VERY_DARK_GREEN,
            new BasicStroke(0.5f),
            false);
        final var c = g2.getPaint();
        final var f = g2.getFont();
        g2.setPaint(ChartColor.VERY_DARK_GREEN);
        g2.setFont(f.deriveFont(Font.BOLD, f.getSize() - 1));
        g2.drawString(FMT.format(m.ts), (int) mx + 5, (int) ymin + 50);
        g2.setFont(f);
        g2.setPaint(c);
      }
    }
  }

  private void drawCurrentValue(Graphics2D g2, List<MetricValue> items, Rectangle2D area) {
    for (final var it : items) {
      final var x = xAxis.valueToJava2D(it.x, area, RectangleEdge.BOTTOM);
      final var ay = (TraceNumberAxis) plot.getRangeAxis(it.dsIndex);
      final var y = ay.valueToJava2D(it.y, area, RectangleEdge.LEFT);
      ay.setCurrentValue(y);
      drawShape(g2, new Ellipse2D.Double(x - 4, y - 4, 8, 8), it.color, null, false);
    }
  }

  private List<MetricValue> valuesNearLine(final long lineTs) {
    final var values = new ArrayList<MetricValue>();
    for (var i = 0; i < plot.getDatasetCount(); i++) {
      final var ds = (MetricDataset) plot.getDataset(i);
      if (!ds.isEmpty()) {
        final var rend = (XYLineAndShapeRenderer) plot.getRenderer(i);
        final var di = ds.getNearestY(lineTs);
        if (di.isPresent() && rend.isSeriesVisible(0)) {
          final var mv =
              MetricValue.of(di.get(), i, rend.lookupSeriesPaint(0), ds.getSeriesLabel());
          values.add(mv);
        }
      }
    }
    return values;
  }

  public void selectValue(int step) {
    final var area = chart.getScreenDataArea();
    if (lastPoint == null) {
      lastPoint = new Point((int) area.getCenterX(), (int) area.getCenterY());
      fireOverlayChanged();
      return;
    }
    final var ts = (long) xAxis.java2DToValue(lastPoint.getX(), area, RectangleEdge.BOTTOM);
    final var cp = view.getCurrentPeriod();
    var select = of(ts).plusSeconds(step * cp.getSeconds());
    select = nearestDt(select);
    final var nx = xAxis.valueToJava2D(toMiliseconds(select), area, RectangleEdge.BOTTOM);
    final var newPoint = new Point(lastPoint);
    newPoint.setLocation(nx, lastPoint.getY());
    if (area.contains(newPoint)) {
      lastPoint = newPoint;
    }
    fireOverlayChanged();
  }
}

# Actions3 + Wayland

Wayland is a display server protocol that provides a more modern and secure 
alternative to the X11 protocol, which has been in use since the 1980s. Wayland 
is designed to be more efficient and performant than X11, and it also supports 
more modern features such as multi-touch and high-DPI displays. However Java and 
Java GUI is lacking native Wayland support. Instead Java uses XWayland which 
is compatibility layer for native X11 applications. Tests on Fedora 39 that uses
uses Wayland by default shows that certain features `Actions3`, such as creating
markers and showing showing current values might not work correctly in that 
configuration. These features are implemented with [JFreeChart Overlays](https://www.jfree.org/jfreechart/javadoc/org/jfree/chart/panel/Overlay.html).

![JetBrains Runtime](actions3_wayland_jbr.png)

First screenshot shows correct behavior of application - all markes (green 
vertical lines with date) and current values line are visible, current values are 
selected with small circle.

![OpenJDK](actions3_wayland_jdk.png)

Second screenshot shows behavior of `Actions3` on Fedora 39 with Wayland: markers 
and current line are partialy visible, current values are seleceted only on one 
data series and box with current values don't have border.

## Possible solutions

User running `Actions3` on linux distribution that uses Wayland and see effects 
described above has two options to try:

- login with X session if distribution allows that
- use [JetBrains Runtime](https://github.com/JetBrains/JetBrainsRuntime/releases) 
  to run `actions3.jar`. When application is run with that runtime on Fedora 39 
  then undesired effect are not visible.
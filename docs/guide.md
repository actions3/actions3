# User guide

# I Installation

Actions3 requires installed Java Runtime Environment version 21 or higher. Free version is distributed as an executable JAR file that contains all necessary libraries. Users can download the latest version from the project’s website.

# II First run

The application can be run from the command line as `java -jar actions3.jar` or with mouse double click if the operating system recognizes executable JAR. After the application starts it tries to read general settings. Depending on the operating system it loads settings from file: 

- Windows: `$USER_DIR/AppData/Local/actions3/settings.db`  
- Linux: `$USER_DIR/.config/actions3/settings.db`  
- other: `$USER_DIR/.actions3/settings.db`

If the directory or `settings.db` file is not found, the application creates it automatically.

Before adding the first action to the application users need to initialize a new project, which in practice means creating a file with `pa3` extension in preferred localization. Users can create multiple projects but only one can be open at the same time. Application will remember last opened project and will load during next start.

# II.1 Add first action

Concepts of `action` and `view` are essential for Actions3. Each action that users can invoke from the application menu defines a configuration that is used to fetch an initial data and present it the new view. Depending on the type of action some of the initial settings might be modified after the view is displayed in the application.

Actions3 supports following action types:

- Logs - displays results of CloudWatch Insights query in tabular form,  
- Metrics - displays CloudWatch Metrics on line chart.

## II.1.1 Logs configuration

To add a new action displaying results of CloudWatch Insights query users select option `Application > Actions > New action` in main menu. In the new dialog window, they need to put the name that will be used to display the action in the actions menu, select `Logs` type and put the name of the group. If the group with the given name doesn’t exist, the application will create it automatically.   
In last step users need to define initial view’s configuration:

- `awsProfile` - AWS cli profile that contains credentials that allow executing CW Insights query. Application requires following permissions: `DescribeLogGroups`, `StartQuery`, `StopQuery`, `GetQueryResults`,  
- `awsRegion` - AWS region where logs are collected,  
- `name`- name of view  
- `groups` - list of CloudWatch groups. User that defines an action can select one or more groups using prefix that ends with `*`. For example prefix `/aws/lambda/function-*` will select all groups with names starting from `/aws/lambda/function-`. If the prefix matches more than 10 groups, action won’t be executed in order to protect a user from high query cost. In such case application will display error message ![image](img/fetch_error.png){width=50% height=50%}  
- `searchQuery` - CloudWatch Insights query as defined in [service documentation](https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/CWL_QuerySyntax.html). Actions3 **doesn’t support** the following keywords: `pattern`, `diff`, `unmask`, `filterIndex`, `unnest` and `SOURCE`. Property can be either a single string value or list of strings that are concatenated using a single space. Parameter from `parameters` list can be referenced in any place of query by its name prefixed with `&`. For example `&Date and Time` references the item from the `parameters` list with `name` equal `Date and Time`. If the query is not defined, application sets default query and show in input dialog before execution,  
- `parameters` - list of input parameters that user must insert before query execution. Application uses input values to substitute placeholders in `searchQuery`. List contains items with `name` defined. That value is used as placeholder identifier and as label in input dialog,  
- `dateRange` - initial date and time range for search query. Object has following properties:  
  - `type` - type of date range:  
    - `absolute` - application calculates initial date/time range but user can change it before query execution  
    - relative - application calculates initial date/time range and uses it directly, without asking user  
  - `relativeRange` - period of time that is used to calculate initial date/time range. Application subtracts `relativeRange` from current time to calculate start of date range  
- `askQuery` - if true user can modify predefined `searchQuery` before execution,  
- bytesLimit - maximum number of bytes that application is allowed to read during query execution. When the limit of read bytes is exceeded, the query is canceled to avoid higher costs. Users can define it as a number or string that represents the number of megabytes. For example `100MB` means that the query will be canceled after reading 100 megabytes. Limit enforcement is disabled when `bytesLimit` is negative number

![image](img/new_logs_action.png){width=50% height=50%}

## II.1.2 Metrics configuration

To add a new action displaying results of CloudWatch Insights query users select option `Application > Actions > New action` in main menu. In the new dialog window, they need to put the name that will be used to display the action in the actions menu, select `Metrics` type and put the name of the group. If the group with the given name doesn’t exist, the application will create it automatically. 

- awsProfile - AWS cli profile that contains credentials that allows reading CloudWatch metrics. Role assumed in profile must allow following actions:  `GetMetricData`, `ListMetrics`,   
- awsRegion - AWS region where metrics are stored,  
- name - title displayed on top of chart,  
- period - The granularity, in seconds, of the returned data points  
- timeRange - time duration that is  used to calculate the initial time stamp indicating the earliest data to be returned. To calculate start time stamp application subtract `timeRange` duration from the current timestamp,  
- autoRefresh - controls if current state should be refreshed. If auto refresh is enabled application will read latest metrics in time range that is defined with `timeRange` and `period`,  
- metrics  
  - namespace - the namespace of metric  
  - name - metric name  
  - dimensions - dimensions of metric. A dimension is a name/value pair that is part of the identity of a metric,  
  - conversion - coefficient used to display values in desired unit. For example: if metric collects milliseconds but chart should display values in seconds then `conversion` should be equal to `1000`,  
  - statistic - the statistic function used for values aggregation,  
  - label - name of metric visible in chart,  
  - visible - controls if metric should be visible on chart. Metrics that are referenced in `expression` might be hidden,  
  - expression - metric math expression to be performed on the returned data.

![image](img/new_metrics_action.png){width=50% height=50%}

# III Managing actions

Users can manage all defined actions by calling operations from the application menu. In addition to creating, editing and deleting actions, the application supports operations that help to create new actions based on current view or existing action. Additionally it is possible to export and import actions from a transport file.

## III.1 Edit action

Users can edit existing actions. All parameters of action can be modified except the type of action. 

1. From the actions menu the user selects one that should be modified.  
2. From the menu `Applications > Actions` choose `Edit action`.  
3. User modifies the action setting.  
4. User clicks the `Validate` to check the action’s correctness and format JSON configuration.  If the application detects problems, an appropriate message is displayed. This step can be omitted, because the application always formats and verifies settings before save.  
5. After successful validation, the user clicks the `OK` button to save changes.   
6. Application reloads the actions menu and collapses all nodes.

## III.2 Delete action

In order to delete action from the menu, users should select one and invoke `Application > Actions > Delete action`. Application will display a confirmation dialog. User can confirm the operation or cancel it.

## III.3 Clone action

Users can clone existing action if they need another action with similar settings, for example to clone metrics view from pre-prod environment to production environment. In order to clone action from the menu, users should select one and invoke `Application > Actions > Clone action`. Application adds new action under the same group where the original one was defined. Cloned action has the original name with a numeric suffix. 

## III.4 Action from view

Users that modify the original view, for example add more metrics to the chart or change log search query, might create a new action from the active view. If users decide to create a new action from a Metrics view it will contain all visible and hidden time series loaded in the active chart. If they create an action from Logs view, a new one will have the last executed query. When the user invokes operation `Application > Actions > Action from view` application gets settings from active view and displays it in the new action dialog. Users can set the name of the new action, menu group, adjust view settings and save changes.

## III.5 Export / Import actions

Users can export actions to the transport text file and use it to import actions in another project. Project files (with `pa3` extension) are platform independent and can be moved from one computer to another without any issue. However actions exported to the text file can be used as a backup option or modified in any editor.   
When users invoke `Application > Actions > Export actions` they must choose the  name of the output file and its localization.  Application will dump the content of the project database to the text file with `a3` extension.  
When `Application > Actions > Import actions` is invoked users must select `*.a3` file. Application will try to import actions and a configuration from the input file to the current project’s database. If an action with the same name already exists, the application will create a new action with the original name and numeric suffix.

# IV Logs View

In the Logs View users can display results of CloudWatch Insights Query. Depending on the action configuration, the application will either use initial settings and open the view immediately or ask the user for additional input, such as search query, query parameters or time range.

## IV.1 Open the view

![image](img/logs_input_dialog_ask_false.png){width=50% height=50%}

Images above show example Logs View configuration. In settings, the `parameters` list contains one parameter - `Log level` that is later referenced in the query under `searchQuery` property as `&Log level`. Time range in property `timeRange` is defined as absolute with initial time range `PT2H`.   
When users invoke action with described example configuration, application displays input dialog that contains three editable input fields:

- `Start date` - The beginning of the time range to query. Initial value of this field is calculated by subtracting 2 hours (defined in actions settings as `PT2H`) from current date and time,  
- `End date` - The end of the time range to query. Initial value of this field is equal to current date and time,  
- `Log level` - input field for the parameter defined in settings.

Users can edit values in each field and move to the next field using a mouse or pressing `Enter` key on the keyboard. When the last field has focus and users press `Enter` the application opens the Logs view in the new tab and starts a search query. Application will be reporting search progress - number of scanned bytes, scanned and matching rows.

If the `parameters` list is empty and `searchQuery` is empty or `askQuery` is true the Application will show the initial search query in the input dialog. If `searchQuery` is not defined the application will set the default query. In the input dialog users can adjust the query visible in the text area and execute it by pressing `Ctrl+Enter` on the keyboard or by clicking the `OK` button.

**Screenshot with load bar + screenshot with table**

### IV.1.1 Cancel query

When data loading is in progress, the application shows a blue progress bar in the bottom of the table. Users can cancel this process by pressing the button `Cancel` next to the progress bar or with keyboard shortcut `Ctrl + Shift + D`. All running queries are canceled automatically when users shutdown the application or the view.

## IV.2 View menu

After loading data to the view’s table menu `View` becomes active. Users can use operations from that menu to work with Insight Query results. Each operation can be executed from the application menu or with the keyboard shortcut which starts with `Ctrl + Shift` sequence.

Available operations are described in following subchapters. 

### IV.2.1 `Expand / Collapse` 

Shortcut: `Ctrl + Shift + =`  
To improve browsing data, the application cuts text in the cell if its length exceeds some value. In such a case visible text will end with three dots `...`. Users can use the operation to expand text in the cell to full length or collapse it again.

### IV.2.2 `Run history` 

Shortcut: `Ctrl + Shift + B`  
Application opens a new dialog that contains a list for the latest search query sorted in descending order. History remembers an executed query and query’s time range. After users select one item in the list the application opens the input dialog with the original query and its time range where users can adjust values before execution.  
![image](img/run_history.png) 

### IV.2.3 `Copy cell(s)` 

Shortcut: `Ctrl + Shift + C`  
Copy all selected cells in the table to the system clipboard.

### IV.2.4 `Cancel query` 

Shortcut: `Ctrl + Shift + D`  
Stop running the query. Read more in chapter `Cancel query`.

### IV.2.5 `Load more forward` 

Shortcut: `Ctrl + Shift + F`  
CloudWatch Insight returns only rows up to the limit defined in the query. For example if 1000 rows are matching the query filter and the limit is set to 400 then the application will receive only the first 500 matching rows. Users can use  the `Load more forward` operation to load the next 400 matching rows.

### IV.2.6 `Next time window` 

Shortcut:  `Ctrl + Shift + N`  
If CloudWatch Insights return all rows within the time range that is defined in the configuration or resolved from input parameters, Users can use `Next time window` to load rows from the next time window. Depending on the query sort order the application calculates a next time window by adding or subtracting original duration from last time range. For example if the time range of the query is [2024-02-12, 2024-02-13] and sort order is ascending then the next time window is [2024-02-13, 2024-02-14].  If the time range of the query is [2024-02-12 12:00, 2024-02-12 13:00] and sort order is descending then the next time window is [2024-02-12 11:00, 2024-02-12 12:00].

### IV.2.7 `Run again` 

Shortcut: `Ctrl + Shift + G`  
Shows input dialog with start and end time range and the last executed query. Users can modify the time range and execute the query again.  
![image](img/run_again.png)

### IV.2.8 `Format cell` 

Shortcut:  `Ctrl + Shift + J`  
Formats cell if contains text in JSON format. Next invocation restores original formatting.  
![image](img/format_cells.png)

### IV.2.9`Show query` 

Shortcut: `Ctrl + Shift + Q`  
Shows the last executed query and copies it to the system clipboard.  
![image](img/show_query.png){width=50% height=50%}

### IV.2.10 `Reload query` 

Shortcut: `Ctrl + Shift + R`  
Executes query again without any modifications. For actions with relative time range it calculates a new time range that ends now.

### IV.2.11 `Search`

Shortcut:  `Ctrl + Shift + S`  
Search for strings in table cells. Matching string is highlighted,

### IV.2.12 `Clear search` 

Shortcut: `Ctrl + Shift + U`  
Clear string highlighting

### IV.2.13 `Dynamic column`

Shortcut:  `Ctrl + Shift + Y`  
If cells in  the column contain text in JSON format, users can use this operation to extract values with [the JSON pointer](https://www.rfc-editor.org/rfc/rfc6901) and display it in a new column. Pointer should start with a column number. For example if second column contains body like `{ “body” { "text": "Hello world", "number": 5, "dt": "2024-12-29T15:11:37.712832"},"txId":"0000-1234565"}` then Users can extract `txId` with pointer `2/txId`. To extract `text` they should define column as `2/body/text`  
![image](img/dynamic_column.png)

### IV.2.14 Correlated search

 Users can use any value from the result table to find a correlation with results from another query that is defined in the application. This operation can be executed only from the context menu that is displayed on the mouse right click. Users must double click on a cell with a value that will be used for correlation. When the blinking cursor appears users must highlight the value, display the context menu with the right click and click button `Search more…`. In the new dialog window users should select a query that will be executed with highlighted value. For correlation the application allows selecting only queries without input parameter   
**![image](img/correlated_search.png)**  
 

# V Metrics View

In Metrics View users can visualize collected Cloudwatch Metrics as a line chart. View provides features that help exploring data. In the configuration users need to define the initial relative time range which the application uses to calculate the date of  the first and last visible data point. After data is loaded in the diagram users can use view’s actions to adjust initial settings. By default the application displays each data series on a separate Y axis. 

When users start moving their mouse in the diagram area, the application selects the closest data point on each data series. Selected data points are marked with a small circle and additionally selected value is marked on Y range. **![image](img/metrics1.png)** Additionally the value of each selected endpoint with a timestamp is displayed in the  top right corner. Users can also use keyboard shortcuts to select data points.

If metrics are displayed with low resolution, for example 1 day, users can use a mouse to select an area in the diagram and zoom in. Application adjusts visible time range and resolution to selected area. For example if the new time range is shorter than 1 day, then the application will set 15 minutes resolution. If users want to display data points  with higher resolution, they need to zoom in again.

## V.1 View actions

**![image](img/metrics_actions.png)**

### V.1.1 Set Marker

Shortcut: `Ctrl + Shift + Space`   
Users can use this action to set a marker at a significant data point. Marker is always set at the selected data point therefore users must select a data point using a mouse or the keyboard shortcut.  
Marker might be also set with a mouse left click next to the data point.

### V.1.2 Clear markers

Shortcut: `Ctrl + Shift + N`   
Removes all markers from diagram

### V.1.3 Previous data point

Shortcut: `Ctrl + Shift + Left`  
Selects previous data points in all visible data series. For each selected data point the application displays its value on Y axes and in  the top right corner.

### V.1.4 Next data point

Shortcut: `Ctrl + Shift + Right`  
Selects next data points in all visible data series.

### V.1.5 Shift left

Shortcut: `Ctrl + Shift + J`  
When users trigger that operation the application calculates a new visible time range for the diagram. It subtracts the current data series resolution from start and end of the current time range. For example if the visible time range is [2024-10-11 11:00:00, 2024-10-11-13:00:00] and the current resolution is 5 minutes, then new shifted time range will be  [2024-10-11 10:55:00, 2024-10-11-12:55:00] 

### V.1.6 Shift right

Shortcut: `Ctrl + Shift + K`  
When users trigger that operation the application calculates a new visible time range for the diagram. It adds the current data series resolution to start and end of the current time range. For example if the visible time range is [2024-10-11 11:00:00, 2024-10-11-13:00:00] and the current resolution is 15 minutes, then a new shifted time range will be  [2024-10-11 11:15:00, 2024-10-11-13:15:00] 

### V.1.7 Absolute time range

Shortcut: `Ctrl + Shift + G`  
Sets a new start date and an end date of visible time range. When operation is executed an input dialog is presented and users can change the visible time range.  
![image](img/absolute_range.png)

### V.1.8 Relative time range

Shortcut: `Ctrl + Shift + T`  
When operation is executed an input dialog is presented with a predefined list of relative time ranges. When users select the time range and click the `OK` button the application calculates a new time range from the selected value.  
![image](img/relative_range.png)

### V.1.9 Hide single series

Shortcut: `Ctrl + Shift + P`  
When the operation is executed users can select one data series from the list. Selected series becomes invisible but it can be restored at any time.   
![image](img/hide_series.png)

### V.1.10 Show single series

Shortcut: `Ctrl + Shift + I`  
When the operation is executed users can select one data series from the list. All series from the diagram become invisible except the selected one.

### V.1.11 Show all series

Shortcut: `Ctrl + Shift + E`  
Restores all invisible series to the diagram.

### V.1.12 Refresh

Shortcut: `Ctrl + Shift + R`  
Reads again data points in current time range

### V.1.13 Reset zoom

Shortcut: `Ctrl + Shift + U`  
Resets zoom of diagram to resolution and relative time range that is defined in view’s configuration.

### V.1.15 Collapse range

Shortcut: `Ctrl + Shift + Down`  
Extends the length of all Y axes. By default the application each data series has a separate range (Y) axis. Application draws data series in such a way that the maximum value of the Y axis is equal to the highest value in a data series and the minimum value is equal to the lowest value of a data series. When users execute that operation the application adds some value to the highest range value and subtracts a value from the lowest range value. In consequence the area occupied by the data series line is smaller. It might improve readability of the diagram when the data series hasmany extreme values.  
![image](img/collapse.png)

### V.1.16 Expand range

Shortcut: `Ctrl + Shift + Up`  
Trims length of Y axes. It is a reverse operation to the `Collapse range` operation.

### V.1.17 Show / hide current value

Shortcut: `Ctrl + Shift + M`  
Hides or restores view of current values in top right corner of diagram.

### V.1.18 Show / hide control panel

Shortcut: `Ctrl + Shift + O`  
Shows or hides a diagram control panel. Users can use the panel to add and remove data series in the diagram. Control panel has two tabs: `Visible` - data series that are visible and their properties and `Available` - a  list of metrics that users can add to the diagram. In the first panel users can select any data series in the list. Properties of the data series are displayed in the table at the bottom of the panel: 

- metric coordinates: name, namespace and dimensions,  
- statistic used to aggregate data  
- conversion - value used to calculate data visible in the diagram. Conversion is defined in the view’s configuration.

When users select a  data series and press the button `Remove` a data series is removed from the diagram.  
![image](img/control_panel1.png)

The `Available` tab is built from three parts. In the middle is visible a list of metrics in AWS accounts, however it contains only metrics that received new values in the last 3 hours. If a list is too long users can use filters to find  metrics that they need. They can filter metrics by name, namespace, dimensions, or all values. When users press button `Add filter` they need to put an expression:

- `name [not] like <value>` to select only metrics that [don’t] have <value> in name,  
- `dim [not] like <value>` to select only metrics that [don’t] have <value> in dimensions,  
- `ns [not] like <value>` to select only metrics that [don’t] have <value> in namespace,  
- `any [not] like <value>` to select only metrics that [don’t] have <value> in name, namespace or dimensions.

For example an expression like `ns like Lambda` will select only metrics with a value `Lambda` in the  namespace. Expression `name not like guide` will select only metrics which name doesn’t contain value `guide`.  
![image](img/control_panel2.png)  
Users can add a selected metric to the chart. When they press the button `Add to chart` they need to put a label of a new data series,  a statistic that will be used to calculate data points and a conversion value. After pressing the `OK` button the  application will add a new data series to the diagram.

# Navigation

Whole user experience inside `Actions3` is organized around actions and views. Because every action contains initial settings that define what data should be read and how it should be displayed, users can get information that they need with one or two mouse clicks. Navigation in `Actions3` happens on two levels:

- on the application level where user open or close view, activates view, or display it in full screen,  
- on a view level where users execute operations specific for view types. For change metrics time range or search for specific value in logs table.

On each level most of the operations users can trigger either using computer mouse or keyboard shortcuts.

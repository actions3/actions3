/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.spi;

import java.util.Optional;

public class AppException extends RuntimeException {

  private static final long serialVersionUID = -7752786967957888686L;

  private String context;

  public AppException(String message, Throwable cause, String context) {
    super(message, cause);
    this.context = context;
  }

  public AppException(String message, Throwable cause) {
    this(message, cause, null);
  }

  public AppException(String message) {
    super(message);
  }

  public String type() {
    final var cn = getClass().getSimpleName();
    return cn.substring(cn.lastIndexOf('.') + 1);
  }

  public Optional<String> context() {
    return Optional.ofNullable(context);
  }
}

/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.spi;

import java.io.IOException;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public interface AppViewSupport {

  String getName();

  default AppView create(AppViewContext ctx) {
    return create(ctx.getConfig(), ctx.getApp());
  }

  AppView create(AppViewConfig config, Application main);

  default ValidationResult validate(String config) {
    return new ValidationResult(config, null);
  }

  default String format(String config) {
    return config;
  }

  AppViewConfig readConfig(String raw);

  String writeConfig(AppViewConfig cfg);

  String emptyConfig();

  default Icon icon() {
    try (var is = AppViewSupport.class.getResourceAsStream("default-icon.gif")) {
      return new ImageIcon(is.readAllBytes());
    } catch (final IOException e) {
      return null;
    }
  }
}

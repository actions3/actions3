/*
 * Actions3 Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.logs;

import static java.lang.Character.isDigit;
import static java.lang.Character.isLetterOrDigit;
import static java.lang.Character.isWhitespace;

import java.util.ArrayList;
import java.util.List;

public class InsightsQuery {

  private static final Generic EMPTY = new Generic(0, "", List.of());

  private int sp(String query) {
    final var len = query.length();
    var p = 0;
    final var chars = query.toCharArray();
    while (p < len && isWhitespace(chars[p])) {
      p += 1;
    }
    return p;
  }

  private QueryNode evalSeq(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    var p = sp(query);
    final var chars = query.toCharArray();
    var ch = chars[p];
    if (ld(ch)) {
      p += 1;
    } else {
      return EMPTY;
    }
    final var len = query.length();
    while (p < len) {
      ch = chars[p];
      if (ld(ch)) {
        p += 1;
      } else {
        break;
      }
    }
    return new Generic(p, query);
  }

  private QueryNode evalNum(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    var p = sp(query);
    final var chars = query.toCharArray();
    var ch = chars[p];
    if (isDigit(ch)) {
      p += 1;
    } else {
      return EMPTY;
    }
    final var len = query.length();
    while (p < len) {
      ch = chars[p];
      if (isDigit(ch)) {
        p += 1;
      } else if (ld(ch)) {
        // it might be identifier that starts with digit
        return EMPTY;
      } else {
        break;
      }
    }
    return new Generic(p, query);
  }

  private QueryNode evalIdent(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    var p = sp(query);
    final var n = evalSeq(query, '`');
    if (!n.empty()) {
      return n;
    }
    final var chars = query.toCharArray();
    var ch = chars[p];
    if (ld(ch) || ch == '@') {
      p += 1;
    } else {
      return EMPTY;
    }
    final var len = query.length();
    while (p < len) {
      ch = chars[p];
      if (ld(ch)) {
        p += 1;
      } else if (ch == '.' && p >= 2) {
        p += 1;
      } else {
        break;
      }
    }
    return new Ident(p, query);
  }

  private QueryNode evalParseAliasList(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    var p = sp(query);
    final var children = new ArrayList<QueryNode>();
    var n = evalIdent(query.substring(p));
    if (n.empty()) {
      return EMPTY;
    }
    children.add(n);
    p += n.len();
    p += sp(query.substring(p));
    if (eq(query, p, ',')) {
      p += 1;
      n = evalParseAliasList(query.substring(p));
      if (n.empty()) {
        return EMPTY;
      } else {
        children.addAll(n.children());
        p += n.len();
        return new ParseAliasList(p, query.substring(0, p).trim(), children);
      }
    }
    return new ParseAliasList(p, query.substring(0, p).trim(), children);
  }

  private QueryNode evalFieldsList(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    var p = sp(query);
    final var children = new ArrayList<QueryNode>();
    var n = evalFieldItem(query.substring(p));
    if (n.empty()) {
      return EMPTY;
    }
    children.add(n);
    p += n.len();
    p += sp(query.substring(p));
    if (eq(query, p, ',')) {
      p += 1;
      n = evalFieldsList(query.substring(p));
      if (n.empty()) {
        return EMPTY;
      } else {
        children.addAll(n.children());
        p += n.len();
        return new ListNode(p, query.substring(0, p).trim(), children);
      }
    }
    return new ListNode(p, query.substring(0, p).trim(), children);
  }

  private QueryNode evalItemsList(String query, String cmd) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    var p = sp(query);
    final var children = new ArrayList<QueryNode>();
    var n = evalListItem(query.substring(p), cmd);
    if (n.empty()) {
      return EMPTY;
    }
    children.add(n);
    p += n.len();
    p += sp(query.substring(p));
    if (eq(query, p, ',')) {
      p += 1;
      n = evalItemsList(query.substring(p), cmd);
      if (n.empty()) {
        return EMPTY;
      } else {
        children.addAll(n.children());
        p += n.len();
        return new ListNode(p, query.substring(0, p).trim(), children);
      }
    }
    return new ListNode(p, query.substring(0, p).trim(), children);
  }

  private QueryNode evalDisplayList(String query) {
    return evalItemsList(query, "display");
  }

  private QueryNode evalExprList(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    var p = sp(query);
    final var children = new ArrayList<QueryNode>();
    var n = evalBiExpr(query.substring(p));
    if (n.empty()) {
      return EMPTY;
    }
    children.add(n);
    p += n.len();
    p += sp(query.substring(p));
    if (eq(query, p, ',')) {
      p += 1;
      n = evalExprList(query.substring(p));
      if (n.empty()) {
        return EMPTY;
      } else {
        children.addAll(n.children());
        p += n.len();
        return new ListNode(p, query.substring(0, p).trim(), children);
      }
    }
    return new ListNode(p, query.substring(0, p).trim(), children);
  }

  private QueryNode evalListItem(String query, String cmd) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    var p = sp(query);
    final var children = new ArrayList<QueryNode>();
    final var n = evalBiExpr(query.substring(p));
    if (n.empty()) {
      return EMPTY;
    }
    p += n.len();
    children.add(n);
    return new FieldItem(p, query.substring(0, p).trim(), children, cmd);
  }

  private QueryNode evalFieldItem(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    var p = sp(query);
    final var children = new ArrayList<QueryNode>();
    var n = evalBiExpr(query.substring(p));
    if (n.empty()) {
      return EMPTY;
    }
    p += n.len();
    children.add(n);

    p += sp(query.substring(p));
    n = evalKw(query.substring(p), "as");
    if (n.empty()) {
      return new FieldItem(p, query.substring(0, p).trim(), children, "fields");
    }
    p += n.len();
    children.add(n);

    p += sp(query.substring(p));
    n = evalIdent(query.substring(p));
    if (n.empty()) {
      return EMPTY;
    }
    p += n.len();
    children.add(n);
    return new FieldItem(p, query.substring(0, p).trim(), children, "fields");
  }

  private QueryNode evalSubExpr(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    var p = sp(query);
    if (!eq(query, p, '(')) {
      return EMPTY;
    }
    p += 1;
    p += sp(query);
    final var n = evalBiExpr(query.substring(p));
    if (n.empty()) {
      return EMPTY;
    }

    p += n.len();
    p += sp(query.substring(p));
    if (!eq(query, p, ')')) {
      throw new BadQueryException("Missing expected ')' in subexpression");
    }

    p += 1;
    return new SubExpr(p, query.substring(0, p).trim(), List.of(n));
  }

  private QueryNode evalExprHead(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    final var p = sp(query);
    final var qs = query.substring(p);
    var n = evalNum(qs);
    if (!n.empty()) {
      return new Generic(p + n.len(), query);
    }
    n = evalStr(qs);
    if (!n.empty()) {
      return new Generic(p + n.len(), query);
    }
    n = evalFunction(qs);
    if (!n.empty()) {
      return new Generic(p + n.len(), query);
    }
    n = evalIdent(qs);
    if (!n.empty()) {
      return new Generic(p + n.len(), query);
    }
    n = evalRegex(qs);
    if (!n.empty()) {
      return new Generic(p + n.len(), query);
    }
    return EMPTY;
  }

  private QueryNode evalBiExpr(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    var p = sp(query);
    var qs = query.substring(p);
    var n = evalSubExpr(qs);
    if (n.empty()) {
      n = evalExprHead(qs);
      if (n.empty()) {
        return EMPTY;
      }
    }
    final var children = new ArrayList<QueryNode>();
    p += n.len();
    children.add(n);

    qs = query.substring(p);
    final var v = sp(qs);
    if (p + v >= query.length()) {
      return new BiExpr(p, query.substring(0, p).trim(), children);
    }
    final var ch = query.charAt(p + v);
    if (ch == '/' || ch == '*' || ch == '+' || ch == '-') {
      p += (v + 1);
    } else if (ch == '<' || ch == '>' || ch == '~' || ch == '=' || ch == '!') {
      p += (v + 1);
      if (query.charAt(p) == '=') {
        p += 1;
      }
    } else if (v > 0) {
      n = evalKw(query.substring(p), "not");
      if (!n.empty()) {
        p += n.len();
        children.add(n);
        p += sp(query.substring(p));
      }
      qs = query.substring(p);
      n = evalKw(qs, "like");
      if (n.empty()) {
        n = evalKw(qs, "and");
      }
      if (n.empty()) {
        n = evalKw(qs, "or");
      }
      if (n.empty()) {
        return new BiExpr(p, query.substring(0, p).trim(), children);
      }
      p += n.len();
      children.add(n);
    } else {
      return new BiExpr(p, query.substring(0, p).trim(), children);
    }
    p += sp(query.substring(p));
    n = evalBiExpr(query.substring(p));
    if (n.empty()) {
      return EMPTY;
    }
    p += n.len();
    children.add(n);
    return new BiExpr(p, query.substring(0, p).trim(), children);
  }

  private static boolean eq(String query, int p, char v) {
    return p < query.length() && query.charAt(p) == v;
  }

  private QueryNode evalParseRegex(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    final var children = new ArrayList<QueryNode>();
    var p = sp(query);
    var n = evalIdent(query.substring(p));
    if (n.empty()) {
      return EMPTY;
    }
    p += n.len();
    children.add(n);
    p += sp(query.substring(p));
    n = evalRegexGroups(query.substring(p));
    if (n.empty()) {
      return EMPTY;
    }
    p += n.len();
    children.add(n);

    return new ParseRegex(p, query.substring(0, p), children);
  }

  private QueryNode evalFunction(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    final var children = new ArrayList<QueryNode>();
    var p = sp(query);
    var n = evalIdent(query.substring(p));
    if (n.empty()) {
      return EMPTY;
    }
    p += n.len();
    children.add(n);
    p += sp(query.substring(p));
    if (p >= query.length() || query.charAt(p) != '(') {
      return EMPTY;
    }
    p += 1;
    p += sp(query.substring(p));
    n = evalExprList(query.substring(p));
    if (!n.empty()) {
      p += n.len();
      children.add(n);
    }
    p += sp(query.substring(p));
    if (p >= query.length() || query.charAt(p) != ')') {
      return EMPTY;
    }
    p += 1;
    return new Function(p, query.substring(0, p).trim(), children);
  }

  private QueryNode evalParsePattern(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    final var children = new ArrayList<QueryNode>();
    var p = sp(query);
    var n = evalIdent(query.substring(p));
    if (n.empty()) {
      return EMPTY;
    }
    p += n.len();
    children.add(n);
    p += sp(query.substring(p));
    n = evalStr(query.substring(p));
    if (n.empty()) {
      return EMPTY;
    }
    p += n.len();
    children.add(n);

    p += sp(query.substring(p));
    n = evalKw(query.substring(p), "as");
    if (n.empty()) {
      return EMPTY;
    }
    p += n.len();
    children.add(n);

    p += sp(query.substring(p));
    n = evalParseAliasList(query.substring(p));
    if (n.empty()) {
      return EMPTY;
    }
    p += n.len();
    children.add(n);

    return new ParsePattern(p, query.substring(0, p), children);
  }

  private QueryNode evalFields(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    final var children = new ArrayList<QueryNode>();
    var p = sp(query);
    var n = evalKw(query.substring(p), "fields");
    if (n.empty()) {
      return EMPTY;
    }
    children.add(n);
    p += n.len();
    p += sp(query.substring(p));
    n = evalFieldsList(query.substring(p));
    if (n.empty()) {
      return EMPTY;
    }
    children.add(n);
    p += n.len();
    return new FieldsCmd(p, query.substring(0, p).trim(), children, "fields");
  }

  private QueryNode evalStats(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    final var children = new ArrayList<QueryNode>();
    var p = sp(query);
    var n = evalKw(query.substring(p), "stats");
    if (n.empty()) {
      return EMPTY;
    }
    children.add(n);
    p += n.len();
    p += sp(query.substring(p));
    n = evalFieldsList(query.substring(p));
    if (n.empty()) {
      return EMPTY;
    }
    children.add(n);
    p += n.len();
    n = evalGroupExpr(query.substring(p));
    if (n.empty()) {
      return new StatsCmd(p, query.substring(0, p), children);
    }
    children.add(n);
    p += n.len();
    return new StatsCmd(p, query.substring(0, p).trim(), children);
  }

  private QueryNode evalGroupExpr(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    final var children = new ArrayList<QueryNode>();
    var p = sp(query);
    var n = evalKw(query.substring(p), "by");
    if (n.empty()) {
      return EMPTY;
    }
    p += n.len();
    n = evalFieldsList(query.substring(p));
    if (n.empty()) {
      throw new BadQueryException(
          "Wrong 'stats' command. Expected group expression after 'by' [...]'%s'"
              .formatted(query.substring(p)));
    }
    p += n.len();
    children.add(n);
    return new Generic(p, query.substring(0, p), children);
  }

  private QueryNode evalDisplay(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    final var children = new ArrayList<QueryNode>();
    var p = sp(query);
    var n = evalKw(query.substring(p), "display");
    if (n.empty()) {
      return EMPTY;
    }
    children.add(n);
    p += n.len();
    p += sp(query.substring(p));
    n = evalDisplayList(query.substring(p));
    if (n.empty()) {
      return EMPTY;
    }
    children.add(n);
    p += n.len();
    return new FieldsCmd(p, query.substring(0, p).trim(), children, "display");
  }

  private QueryNode evalLimit(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    final var children = new ArrayList<QueryNode>();
    var p = sp(query);
    var n = evalKw(query.substring(p), "limit");
    if (n.empty()) {
      return EMPTY;
    }
    children.add(n);
    p += n.len();
    p += sp(query.substring(p));
    n = evalNum(query.substring(p));
    if (n.empty()) {
      throw new BadQueryException("Expected number");
    }
    p += n.len();
    children.add(n);
    return new LimitCmd(p, query.substring(0, p).trim(), children);
  }

  private QueryNode evalDedup(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    final var children = new ArrayList<QueryNode>();
    var p = sp(query);
    var n = evalKw(query.substring(p), "dedup");
    if (n.empty()) {
      return EMPTY;
    }
    children.add(n);
    p += n.len();
    p += sp(query.substring(p));
    n = evalItemsList(query.substring(p), "dedup");
    if (n.empty()) {
      return EMPTY;
    }
    children.add(n);
    p += n.len();

    return new DedupCmd(p, query.substring(0, p).trim(), children);
  }

  private QueryNode evalSort(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    final var children = new ArrayList<QueryNode>();
    var p = sp(query);
    var n = evalKw(query.substring(p), "sort");
    if (n.empty()) {
      return EMPTY;
    }
    children.add(n);
    p += n.len();
    p += sp(query.substring(p));
    n = evalItemsList(query.substring(p), "sort");
    if (n.empty()) {
      return EMPTY;
    }
    children.add(n);
    p += n.len();

    if (p >= query.length()) {
      return new SortCmd(p, query.substring(0, p).trim(), children);
    }
    final var v = sp(query.substring(p));
    p += v;
    final var qs = query.substring(p);
    n = evalKw(qs, "asc");
    if (n.empty()) {
      n = evalKw(qs, "desc");
    }
    if (!n.empty()) {
      p += n.len();
      children.add(n);
    }
    return new SortCmd(p, query.substring(0, p).trim(), children);
  }

  private QueryNode evalParse(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    final var children = new ArrayList<QueryNode>();
    var p = sp(query);
    var n = evalKw(query.substring(p), "parse");
    if (n.empty()) {
      return EMPTY;
    }
    children.add(n);
    p += n.len();
    p += sp(query.substring(p));
    final var qs = query.substring(p);
    n = evalParsePattern(qs);
    if (n.empty()) {
      n = evalParseRegex(qs);
      if (n.empty()) {
        return EMPTY;
      }
    }
    children.add(n);
    p += n.len();

    return new ParseCmd(p, query.substring(0, p), children);
  }

  private QueryNode evalFilter(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    final var children = new ArrayList<QueryNode>();
    var p = sp(query);
    var n = evalKw(query.substring(p), "filter");
    if (n.empty()) {
      return EMPTY;
    }
    children.add(n);
    p += n.len();
    p += sp(query.substring(p));
    final var qs = query.substring(p);
    n = evalBiExpr(qs);
    if (n.empty()) {
      return EMPTY;
    }
    children.add(n);
    p += n.len();

    return new FilterCmd(p, query.substring(0, p), children);
  }

  private QueryNode evalStr(String query) {
    final var n = evalSeq(query, '\'');
    if (n.empty()) {
      return evalSeq(query, '"');
    }
    return n;
  }

  private QueryNode evalRegex(String query) {
    return evalSeq(query, '/');
  }

  private QueryNode evalRegexGroups(String query) {
    final var n = evalSeq(query, '/');
    if (n.empty()) {
      return n;
    }
    return new RegexGroups(n.len(), n.value(), n.children());
  }

  private QueryNode evalSeq(String query, char delim) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    var p = sp(query);
    final var chars = query.toCharArray();
    var ch = chars[p];
    if (ch != delim) {
      return EMPTY;
    }
    var esc = false;
    p += 1;
    final var len = chars.length;
    while (p < len) {
      ch = chars[p];
      if (ch == '\\') {
        esc = true;
      } else if (ch == delim && !esc) {
        return new Generic(p + 1, query);
      }
      // reset escape
      esc = (ch == '\\');
      p++;
    }
    return EMPTY;
  }

  private QueryNode evalKw(String query, String kw) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    final var p = sp(query);
    final var seq = evalSeq(query.substring(p));
    if (seq.equalTo(kw)) {
      return new Generic(p + seq.len(), query);
    }
    return EMPTY;
  }

  private QueryNode evalQueryHead(String query) {
    var n = evalFields(query);
    if (!n.empty()) {
      return n;
    }
    n = evalDisplay(query);
    if (!n.empty()) {
      return n;
    }
    n = evalParse(query);
    if (!n.empty()) {
      return n;
    }
    n = evalFilter(query);
    if (!n.empty()) {
      return n;
    }
    n = evalSort(query);
    if (!n.empty()) {
      return n;
    }
    n = evalDedup(query);
    if (!n.empty()) {
      return n;
    }
    n = evalLimit(query);
    if (!n.empty()) {
      return n;
    }
    n = evalStats(query);
    if (!n.empty()) {
      return n;
    }
    throw new BadQueryException("Invalid query");
  }

  public QueryNode evalQuery(String query) {
    if (query.isEmpty()) {
      return EMPTY;
    }
    var p = sp(query);
    final var children = new ArrayList<QueryNode>();
    var n = evalQueryHead(query.substring(p));
    if (n.empty()) {
      return EMPTY;
    }
    children.add(n);
    p += n.len();
    p += sp(query.substring(p));
    if (eq(query, p, '|')) {
      p += 1;
      n = evalQuery(query.substring(p));
      if (n.empty()) {
        throw new BadQueryException("Invalid query. Expected next command after '|'");
      } else {
        children.addAll(n.children());
        p += n.len();
        return new ListNode(p, query.substring(0, p).trim(), children);
      }
    } else if (p < query.length()) {
      throw new BadQueryException(
          "Expected '|' but not found. Remaining query [...] '%s'".formatted(query.substring(p)));
    }

    return new ListNode(p, query.substring(0, p).trim(), children);
  }

  private static boolean ld(char c) {
    return isLetterOrDigit(c) || c == '_';
  }

  interface QueryNode {

    default boolean empty() {
      return len() == 0;
    }

    default boolean equalTo(String s) {
      return value() != null && value().equals(s);
    }

    default void print(int level) {
      for (var i = 0; i < level; i++) {
        System.out.print(" ");
      }
      System.out.println(
          " %d (%s) %s [%s]".formatted(level, this.getClass().getSimpleName(), value(), len()));
      for (final var n : children()) {
        n.print(level + 1);
      }
    }

    int len();

    List<? extends QueryNode> children();

    String value();

    default void apply(QueryNodeVisitor visitor) {
      children().forEach(ch -> ch.apply(visitor));
      visitor.visit(this);
    }
  }

  record Function(int len, String value, List<? extends QueryNode> children) implements QueryNode {

    Function(int len, String value) {
      this(len, value.substring(0, len).trim(), List.of());
    }

    @Override
    public void apply(QueryNodeVisitor visitor) {
      children().forEach(ch -> ch.apply(visitor));
      visitor.visit(this);
    }
  }

  record BiExpr(int len, String value, List<? extends QueryNode> children) implements QueryNode {

    BiExpr(int len, String value) {
      this(len, value.substring(0, len).trim(), List.of());
    }

    @Override
    public void apply(QueryNodeVisitor visitor) {
      children().forEach(ch -> ch.apply(visitor));
      visitor.visit(this);
    }
  }

  record ListNode(int len, String value, List<? extends QueryNode> children) implements QueryNode {

    ListNode(int len, String value) {
      this(len, value.substring(0, len).trim(), List.of());
    }

    @Override
    public void apply(QueryNodeVisitor visitor) {
      children().forEach(ch -> ch.apply(visitor));
      visitor.visit(this);
    }
  }

  record Generic(int len, String value, List<? extends QueryNode> children) implements QueryNode {

    Generic(int len, String value) {
      this(len, value.substring(0, len).trim(), List.of());
    }

    @Override
    public void apply(QueryNodeVisitor visitor) {
      children().forEach(ch -> ch.apply(visitor));
      visitor.visit(this);
    }
  }

  record SubExpr(int len, String value, List<? extends QueryNode> children) implements QueryNode {

    SubExpr(int len, String value) {
      this(len, value.substring(0, len).trim(), List.of());
    }

    @Override
    public void apply(QueryNodeVisitor visitor) {
      children().forEach(ch -> ch.apply(visitor));
      visitor.visit(this);
    }
  }

  record FieldItem(int len, String value, List<? extends QueryNode> children, String cmd)
      implements QueryNode {

    @Override
    public void apply(QueryNodeVisitor visitor) {
      children().forEach(ch -> ch.apply(visitor));
      visitor.visit(this);
    }

    public DisplayField resolvedValue() {
      final var v = children.get(0).value();
      final var temporal = v.equals("@timestamp") || v.startsWith("bin(");
      if (children.size() == 3) {
        return new DisplayField(children.get(2).value(), temporal);
      }
      return new DisplayField(children.get(0).value(), temporal);
    }
  }

  record DisplayField(String value, boolean temporal) {}

  record FieldsCmd(int len, String value, List<? extends QueryNode> children, String cmd)
      implements QueryNode {

    @Override
    public void apply(QueryNodeVisitor visitor) {
      children().forEach(ch -> ch.apply(visitor));
      visitor.visit(this);
    }
  }

  record ParseCmd(int len, String value, List<? extends QueryNode> children) implements QueryNode {

    ParseCmd(int len, String value) {
      this(len, value.substring(0, len).trim(), List.of());
    }

    @Override
    public void apply(QueryNodeVisitor visitor) {
      children().forEach(ch -> ch.apply(visitor));
      visitor.visit(this);
    }
  }

  record DedupCmd(int len, String value, List<? extends QueryNode> children) implements QueryNode {

    DedupCmd(int len, String value) {
      this(len, value.substring(0, len).trim(), List.of());
    }

    @Override
    public void apply(QueryNodeVisitor visitor) {
      children().forEach(ch -> ch.apply(visitor));
      visitor.visit(this);
    }
  }

  record ParseRegex(int len, String value, List<? extends QueryNode> children)
      implements QueryNode {

    ParseRegex(int len, String value) {
      this(len, value.substring(0, len).trim(), List.of());
    }

    @Override
    public void apply(QueryNodeVisitor visitor) {
      children().forEach(ch -> ch.apply(visitor));
      visitor.visit(this);
    }
  }

  record ParsePattern(int len, String value, List<? extends QueryNode> children)
      implements QueryNode {

    ParsePattern(int len, String value) {
      this(len, value.substring(0, len).trim(), List.of());
    }

    @Override
    public void apply(QueryNodeVisitor visitor) {
      children().forEach(ch -> ch.apply(visitor));
      visitor.visit(this);
    }
  }

  record FilterCmd(int len, String value, List<? extends QueryNode> children) implements QueryNode {

    FilterCmd(int len, String value) {
      this(len, value.substring(0, len).trim(), List.of());
    }

    @Override
    public void apply(QueryNodeVisitor visitor) {
      children().forEach(ch -> ch.apply(visitor));
      visitor.visit(this);
    }
  }

  record SortCmd(int len, String value, List<? extends QueryNode> children) implements QueryNode {

    SortCmd(int len, String value) {
      this(len, value.substring(0, len).trim(), List.of());
    }

    @Override
    public void apply(QueryNodeVisitor visitor) {
      children().forEach(ch -> ch.apply(visitor));
      visitor.visit(this);
    }

    public String order() {
      if (children.size() == 3) {
        return children.get(2).value();
      }
      return "desc";
    }
  }

  record LimitCmd(int len, String value, List<? extends QueryNode> children) implements QueryNode {

    LimitCmd(int len, String value) {
      this(len, value.substring(0, len).trim(), List.of());
    }

    @Override
    public void apply(QueryNodeVisitor visitor) {
      children().forEach(ch -> ch.apply(visitor));
      visitor.visit(this);
    }
  }

  record StatsCmd(int len, String value, List<? extends QueryNode> children) implements QueryNode {

    StatsCmd(int len, String value) {
      this(len, value.substring(0, len).trim(), List.of());
    }

    @Override
    public void apply(QueryNodeVisitor visitor) {
      children().forEach(ch -> ch.apply(visitor));
      visitor.visit(this);
    }
  }

  record RegexGroups(int len, String value, List<? extends QueryNode> children)
      implements QueryNode {

    RegexGroups(int len, String value) {
      this(len, value.substring(0, len).trim(), List.of());
    }

    @Override
    public void apply(QueryNodeVisitor visitor) {
      children().forEach(ch -> ch.apply(visitor));
      visitor.visit(this);
    }
  }

  record ParseAliasList(int len, String value, List<? extends QueryNode> children)
      implements QueryNode {

    ParseAliasList(int len, String value) {
      this(len, value.substring(0, len).trim(), List.of());
    }

    @Override
    public void apply(QueryNodeVisitor visitor) {
      children().forEach(ch -> ch.apply(visitor));
      visitor.visit(this);
    }
  }

  record Ident(int len, String value) implements QueryNode {

    Ident(int len, String value) {
      this.len = len;
      this.value = value.substring(0, len).trim();
    }

    @Override
    public void apply(QueryNodeVisitor visitor) {
      visitor.visit(this);
    }

    @Override
    public List<? extends QueryNode> children() {
      return List.of();
    }
  }
}

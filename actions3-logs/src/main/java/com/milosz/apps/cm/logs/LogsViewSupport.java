/*
 * Actions3 Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.logs;

import static java.util.logging.Level.FINE;

import com.milosz.apps.cm.common.JsonException;
import com.milosz.apps.cm.common.JsonSupport;
import com.milosz.apps.cm.common.LogUtils;
import com.milosz.apps.cm.spi.AppView;
import com.milosz.apps.cm.spi.AppViewConfig;
import com.milosz.apps.cm.spi.AppViewContext;
import com.milosz.apps.cm.spi.AppViewSupport;
import com.milosz.apps.cm.spi.Application;
import com.milosz.apps.cm.spi.ValidationResult;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class LogsViewSupport implements AppViewSupport {
  private static final Logger LOG = LogUtils.getLogger(FINE, LogsViewSupport.class.getSimpleName());
  private final JsonSupport json = JsonSupport.INSTANCE;

  @Override
  public String getName() {
    return "Logs";
  }

  @Override
  public ValidationResult validate(String config) {
    String query = null;
    final var errors = new ArrayList<String>();
    LogsViewConfig cfg = null;
    try {
      cfg = json.read(config, LogsViewConfig.class);
      if (cfg.hasSearchQuery()) {
        final var iq = new InsightsQuery();
        iq.evalQuery(query = cfg.resolveSearchQuery());
      }
      if (cfg.getDateRange() == null) {
        errors.add("dateRange property is required");
      }
    } catch (final BadQueryException e) {
      if (query != null) {
        LOG.log(Level.WARNING, "Wrong search query " + query);
      }
      errors.add("Wrong search query: " + e.getMessage());
    } catch (final JsonException e) {
      LOG.log(Level.WARNING, "Invalid JSON", e);
      return new ValidationResult(null, List.of("Invalid JSON " + e.getOriginalMessage()));
    } catch (final Exception e) {
      LOG.log(Level.WARNING, "Can't read configuration", e);
      return new ValidationResult(null, List.of(e.getMessage()));
    }
    if (!errors.isEmpty()) {
      return new ValidationResult(null, errors);
    }
    return new ValidationResult(json.write(cfg, true), null);
  }

  @Override
  public Icon icon() {
    try (var is = LogsViewSupport.class.getResourceAsStream("icon.gif")) {
      return new ImageIcon(is.readAllBytes());
    } catch (final IOException e) {
      return null;
    }
  }

  @Override
  public AppView create(AppViewContext ctx) {
    return new LogsView(ctx);
  }

  @Override
  public AppViewConfig readConfig(String raw) {
    try {
      return json.read(raw, LogsViewConfig.class);
    } catch (final Exception e) {
      LOG.log(Level.WARNING, "Can't read configuration", e);
      return new LogsViewConfig();
    }
  }

  @Override
  public String format(String config) {
    return writeConfig(readConfig(config));
  }

  @Override
  public String writeConfig(AppViewConfig cfg) {
    return json.write(cfg, true);
  }

  @Override
  public String emptyConfig() {
    return writeConfig(LogsViewConfig.emptyConfig());
  }

  @Override
  public AppView create(AppViewConfig config, Application main) {
    return new LogsView((LogsViewConfig) config, main, "Logs", "Run again");
  }
}

/*
 * Actions3 Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.logs;

import com.fasterxml.jackson.databind.JsonNode;
import com.milosz.apps.cm.common.JsonSupport;
import com.milosz.apps.cm.logs.InsightsQuery.DisplayField;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import javax.swing.table.AbstractTableModel;

public class LogsTableModel extends AbstractTableModel {

  static final int SUCCESS = 0;
  static final int DYNAMIC_COLUMN = 1;
  private static final DateTimeFormatter FMT =
      DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

  private final List<DisplayField> columns = new CopyOnWriteArrayList<>();
  private final List<DynamicRef> dynamic = new CopyOnWriteArrayList<>();
  private final List<Map<String, FieldValue>> events = new CopyOnWriteArrayList<>();
  private final AtomicInteger mark = new AtomicInteger();
  private final JsonSupport mapper = JsonSupport.INSTANCE;

  public LogsTableModel(QueryVisitor query) {
    columns.addAll(query.getFields());
  }

  public void setQuery(QueryVisitor query) {
    columns.clear();
    columns.addAll(query.getFields());
    dynamic.clear();
    clear();
    fireTableStructureChanged();
  }

  @Override
  public int getRowCount() {
    if (events.isEmpty()) {
      return 1;
    }
    return events.size();
  }

  @Override
  public int getColumnCount() {
    return columns.size() + dynamic.size();
  }

  public void addEvents(List<Map<String, String>> newEvents) {
    final var rows =
        newEvents.stream()
            .map(
                e ->
                    e.entrySet().stream()
                        .collect(
                            Collectors.toMap(Map.Entry::getKey, x -> new FieldValue(x.getValue()))))
            .toList();
    if (mark.get() == events.size()) {
      events.addAll(rows);
    } else {
      merge(events.subList(mark.get(), events.size()), rows);
    }
    mark.set(0);
    fireTableStructureChanged();
  }

  @Override
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    return true;
  }

  public void clear() {
    mark.set(0);
    events.clear();
  }

  public void setMark() {
    mark.set(events.size());
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    if (events.isEmpty()) {
      return "No data";
    }
    final var event = events.get(rowIndex);
    if (columnIndex < columns.size()) {
      return baseValue(event, columnIndex);
    }
    return dynamicValue(event, columnIndex - columns.size());
  }

  private String dynamicValue(Map<String, FieldValue> event, int i) {
    final var ref = dynamic.get(i);
    final var field = column(ref.column());
    if (field.isEmpty()) {
      return null;
    }
    final var fv = event.get(field.get().value());
    return fv.get(ref.path()).orElse(null);
  }

  public int addDynamicColumn(int baseColumn, String path) {
    if (baseColumn >= columns.size()) {
      return DYNAMIC_COLUMN;
    }
    dynamic.add(new DynamicRef(columns.get(baseColumn).value(), path));
    fireTableStructureChanged();
    return SUCCESS;
  }

  private String baseValue(Map<String, FieldValue> event, int columnIndex) {
    final var field = columns.get(columnIndex);
    if (field.temporal()) {
      final var dt = LocalDateTime.parse(event.get(field.value()).value, FMT);
      final var zdt = dt.toInstant(ZoneOffset.UTC).atZone(ZoneId.systemDefault());
      return FMT.format(zdt);
    }
    final var v = event.get(field.value());
    return v != null ? v.value : null;
  }

  @Override
  public String getColumnName(int column) {
    if (column < columns.size()) {
      return columns.get(column).value();
    }
    final var def = dynamic.get(column - columns.size());
    return "%s/%s".formatted(def.column, def.path);
  }

  private Optional<DisplayField> column(String key) {
    return columns.stream().filter(f -> f.value().equals(key)).findFirst();
  }

  public Optional<String> getLastValue(String key) {
    if (events.isEmpty()) {
      return Optional.empty();
    }
    final var last = events.size() - 1;
    final var ev = events.get(last).get(key);
    if (ev == null) {
      return Optional.empty();
    }
    final var field = column(key);
    if (field.isEmpty()) {
      return Optional.empty();
    }
    final var kf = field.get();
    if (!kf.temporal()) {
      return Optional.of(ev.value);
    }
    // Temporal fields (like @timestamp) require special handling
    // as CloudWatch don't allow filtering by human readable strings
    // therefore values of temporal fields are represented as epoch milliseconds
    final var dt = LocalDateTime.parse(ev.value, FMT);
    return Optional.of(String.valueOf(dt.toInstant(ZoneOffset.UTC).toEpochMilli()));
  }

  record DynamicRef(String column, String path) {}

  class FieldValue {
    private final String value;
    private JsonNode node = null;

    public FieldValue(String value) {
      super();
      this.value = value;
    }

    Optional<String> get(String path) {
      if (value == null || value.isBlank()) {
        return Optional.empty();
      }
      if (node != null) {
        return Optional.of(node.at(path).toPrettyString());
      }
      try {
        node = LogsTableModel.this.mapper.read(value, JsonNode.class);
        return Optional.of(node.at(path).toPrettyString());
      } catch (final Exception e) {
        // Non-json files are handled gracefully
      }
      return Optional.empty();
    }
  }

  static <T> void merge(List<T> target, List<T> big) {
    var e = 0;
    var n = 0;
    while (true) {
      if (n == big.size()) {
        break;
      }
      if (e == target.size()) {
        target.add(big.get(n++));
        e++;
      } else {
        if (target.get(e).equals(big.get(n))) {
          e++;
          n++;
        } else {
          target.add(e, big.get(n));
          e++;
          n++;
        }
      }
    }
  }
}

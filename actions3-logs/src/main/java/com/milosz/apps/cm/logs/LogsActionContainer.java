/*
 * Actions3 Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.logs;

import static java.awt.event.KeyEvent.VK_B;
import static java.awt.event.KeyEvent.VK_C;
import static java.awt.event.KeyEvent.VK_D;
import static java.awt.event.KeyEvent.VK_EQUALS;
import static java.awt.event.KeyEvent.VK_F;
import static java.awt.event.KeyEvent.VK_G;
import static java.awt.event.KeyEvent.VK_J;
import static java.awt.event.KeyEvent.VK_N;
import static java.awt.event.KeyEvent.VK_Q;
import static java.awt.event.KeyEvent.VK_R;
import static java.awt.event.KeyEvent.VK_S;
import static java.awt.event.KeyEvent.VK_U;
import static java.awt.event.KeyEvent.VK_Y;

import com.milosz.apps.cm.common.ActionCommand;
import com.milosz.apps.cm.common.ActionHandler;
import com.milosz.apps.cm.common.ViewAppAction;
import com.milosz.apps.cm.spi.AppAction;
import com.milosz.apps.cm.spi.AppActionContainer;
import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.List;

public class LogsActionContainer implements AppActionContainer, ActionHandler {

  private final LogsView view;
  private final List<ViewAppAction> actions;

  public LogsActionContainer(LogsView view) {
    this.view = view;
    this.actions =
        List.of(
            new ViewAppAction("Expand/Collapse", this, VK_EQUALS, LogsViewCommand.EXPAND),
            new ViewAppAction("Load more forward", this, VK_F, LogsViewCommand.FORWARD),
            new ViewAppAction("Format cell", this, VK_J, LogsViewCommand.FORMAT),
            new ViewAppAction("Search", this, VK_S, LogsViewCommand.SEARCH),
            new ViewAppAction("Clear search", this, VK_U, LogsViewCommand.CANCEL_SEARCH),
            new ViewAppAction("Copy cell(s)", this, VK_C, LogsViewCommand.COPY),
            new ViewAppAction("Reload", this, VK_R, LogsViewCommand.RELOAD),
            new ViewAppAction("Show query", this, VK_Q, LogsViewCommand.QUERY),
            new ViewAppAction("Dynamic column", this, VK_Y, LogsViewCommand.DYNAMIC),
            new ViewAppAction("Run history", this, VK_B, LogsViewCommand.RUN_HISTORY),
            new ViewAppAction("Cancel query", this, VK_D, LogsViewCommand.CANCEL_QUERY),
            new ViewAppAction("Next window", this, VK_N, LogsViewCommand.NEXT),
            new ViewAppAction("Run again", this, VK_G, LogsViewCommand.RUN_AGAIN));
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    final var cmd = LogsViewCommand.of(e.getActionCommand());
    accept(cmd);
  }

  @Override
  public void accept(ActionCommand command) {
    if (command instanceof final LogsViewCommand cmd) {
      switch (cmd) {
        case EXPAND:
          view.expand();
          break;
        case FORWARD:
          view.loadMore();
          break;
        case FORMAT:
          view.format();
          break;
        case SEARCH:
          view.search();
          break;
        case CANCEL_SEARCH:
          view.cancelSearch();
          break;
        case COPY:
          view.copyCells();
          break;
        case RELOAD:
          view.reload();
          break;
        case QUERY:
          view.showQuery();
          break;
        case SEARCH_MORE:
          view.searchMore();
          break;
        case NEXT:
          view.nextWindow();
          break;
        case DYNAMIC:
          view.addDynamicColumn();
          break;
        case RUN_HISTORY:
          view.runHistory();
          break;
        case CANCEL_QUERY:
          view.cancelQuery();
          break;
        case RUN_AGAIN:
          view.runAgain();
          break;
      }
    }
  }

  @Override
  public Collection<AppAction> getActions() {
    return actions.stream().sorted().map(AppAction.class::cast).toList();
  }
}

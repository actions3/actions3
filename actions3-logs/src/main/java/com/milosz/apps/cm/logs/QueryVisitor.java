/*
 * Actions3 Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.logs;

import static java.util.logging.Level.FINE;

import com.milosz.apps.cm.common.LogUtils;
import com.milosz.apps.cm.logs.InsightsQuery.DisplayField;
import com.milosz.apps.cm.logs.InsightsQuery.FieldItem;
import com.milosz.apps.cm.logs.InsightsQuery.FieldsCmd;
import com.milosz.apps.cm.logs.InsightsQuery.Generic;
import com.milosz.apps.cm.logs.InsightsQuery.Ident;
import com.milosz.apps.cm.logs.InsightsQuery.ParseAliasList;
import com.milosz.apps.cm.logs.InsightsQuery.ParseCmd;
import com.milosz.apps.cm.logs.InsightsQuery.QueryNode;
import com.milosz.apps.cm.logs.InsightsQuery.RegexGroups;
import com.milosz.apps.cm.logs.InsightsQuery.SortCmd;
import com.milosz.apps.cm.logs.InsightsQuery.StatsCmd;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class QueryVisitor implements QueryNodeVisitor {
  private static final Logger LOG = LogUtils.getLogger(FINE, QueryVisitor.class.getSimpleName());
  private static final Pattern IQ_REGEX = Pattern.compile("\\(\\?<([a-zA-Z0-9]+)>.+\\?\\)");

  private final List<String> parseAliases = new ArrayList<>();

  private final List<DisplayField> fields = new ArrayList<>();

  private final List<String> sort = new ArrayList<>();

  private final Set<String> temporal = new HashSet<>();

  private String sortOrder = null;

  @Override
  public void visit(QueryNode node) {}

  @Override
  public void visit(SortCmd node) {
    sortOrder = node.order();
  }

  @Override
  public void visit(FieldItem node) {
    if (node.cmd().equals("sort")) {
      sort.add(node.children().get(0).value());
    }
  }

  @Override
  public void visit(StatsCmd node) {
    final var iv =
        new QueryNodeVisitor() {
          private final List<DisplayField> displayFields = new ArrayList<>();

          @Override
          public void visit(FieldItem node) {
            displayFields.add(node.resolvedValue());
          }
        };
    node.apply(iv);
    fields.clear();
    addFields(iv.displayFields);
  }

  private void addFields(Collection<DisplayField> fields) {
    this.fields.addAll(fields);
    temporal.addAll(
        fields.stream().filter(DisplayField::temporal).map(DisplayField::value).toList());
  }

  @Override
  public void visit(FieldsCmd node) {
    final var iv =
        new QueryNodeVisitor() {
          private final List<DisplayField> displayFields = new ArrayList<>();

          @Override
          public void visit(FieldItem node) {
            displayFields.add(node.resolvedValue());
          }
        };
    node.apply(iv);
    if (node.cmd().equals("display")) {
      // Display command instructs query engine which fields should be visible
      // therefore we need to clear fields selected in other commands
      parseAliases.clear();
      fields.clear();
    }
    addFields(iv.displayFields);
  }

  @Override
  public void visit(ParseCmd node) {
    final var iv =
        new QueryNodeVisitor() {
          private final List<String> aliases = new ArrayList<>();

          @Override
          public void visit(ParseAliasList node) {
            node.children().stream().map(Ident.class::cast).map(Ident::value).forEach(aliases::add);
          }

          @Override
          public void visit(RegexGroups node) {
            final var m = IQ_REGEX.matcher(node.value());
            var start = 0;
            while (m.find(start)) {
              aliases.add(m.group(1));
              start = m.end(1);
            }
          }
        };
    node.apply(iv);
    parseAliases.addAll(iv.aliases);
  }

  public List<String> getParseAliases() {
    return parseAliases;
  }

  public List<DisplayField> getFields() {
    return fields;
  }

  public List<String> getSort() {
    return sort;
  }

  /**
   * Checks if field with given name represents timestamp.
   *
   * <p>Field not necessary has to be visible. For example it might be hidden in `display` command
   *
   * @param f field name
   * @return true if field is present in query and is temporal
   */
  public boolean isTemporal(String f) {
    return temporal.contains(f);
  }

  public Optional<String> getSortOrder() {
    return Optional.ofNullable(sortOrder);
  }

  public static Optional<String> prettify(String raw, String cmdSep) {
    final var iq = new InsightsQuery();
    QueryNode nodes;
    try {
      nodes = iq.evalQuery(raw);
      return Optional.of(
          nodes.children().stream()
              .map(QueryNode::value)
              .collect(Collectors.joining(cmdSep + " | ")));
    } catch (final BadQueryException e) {
      LOG.warning("Can't parse query '%s'. Error %s".formatted(raw, e.getMessage()));
      return Optional.empty();
    }
  }

  /**
   * Rewrites `raw` query with new `node`.
   *
   * <p>Rewrites `raw` query in such way that new `node` is inserted <b>before</b> another node that
   * is matching condition of `matcher`
   *
   * @param raw original query
   * @param node new string to insert
   * @param matcher condition matching node of original query
   * @return new query as string
   */
  public static String rewriteQuery(String raw, String node, Predicate<QueryNode> matcher) {
    final var iq = new InsightsQuery();
    final var nodes = iq.evalQuery(raw);
    final var commands = nodes.children();
    final var newQuery = new ArrayList<QueryNode>();
    for (final var c : commands) {
      if (matcher.test(c)) {
        newQuery.add(new Generic(1, node, List.of()));
      }
      newQuery.add(c);
    }
    return newQuery.stream().map(QueryNode::value).collect(Collectors.joining(" | "));
  }
}

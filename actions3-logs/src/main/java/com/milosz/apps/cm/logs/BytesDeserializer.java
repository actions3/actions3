/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.logs;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.node.NumericNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.milosz.apps.cm.logs.LogsViewConfig.BytesLimit;
import java.io.IOException;

public class BytesDeserializer extends StdDeserializer<BytesLimit> {

  public BytesDeserializer(Class<?> vc) {
    super(vc);
  }

  public BytesDeserializer() {
    this(null);
  }

  @Override
  public BytesLimit deserialize(JsonParser p, DeserializationContext ctxt)
      throws IOException, JacksonException {
    final var node = p.getCodec().readTree(p);
    if (node instanceof final TextNode t) {
      if (t.asText().endsWith("MB")) {
        try {
          return new BytesLimit(
              Long.parseLong(t.asText().replaceAll("MB$", "")) * 1024 * 1024, "unit");
        } catch (final Exception e) {
          throw new InvalidFormatException(
              p, "Can't convert value %s".formatted(t.asText()), t.asText(), BytesLimit.class);
        }
      } else {
        throw new InvalidFormatException(
            p, "Can't convert value %s".formatted(t.asText()), t.asText(), BytesLimit.class);
      }
    } else if (node instanceof final NumericNode t) {
      return new BytesLimit(t.asLong(), "number");
    }

    throw new IllegalArgumentException("Can't deserialize query from type " + node.getClass());
  }
}

/*
 * Actions3 Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.logs;

import static java.util.Collections.unmodifiableList;
import static java.util.stream.Collectors.joining;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.milosz.apps.cm.common.DateRange;
import com.milosz.apps.cm.spi.AppViewConfig;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import software.amazon.awssdk.auth.credentials.internal.ProfileCredentialsUtils;
import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.profiles.ProfileFile;

public class LogsViewConfig implements AppViewConfig {

  private static final String DEFAULT_PROFILE = "default";

  private String awsProfile;
  private String awsRegion;
  private String name;
  private List<String> groups;
  private List<String> searchQuery;
  private List<Map<String, String>> parameters;
  private DateRangeConfig dateRange;
  private boolean askQuery = false;
  private String queryStr;
  private BytesLimit bytesLimit;

  public LogsViewConfig() {}

  public LogsViewConfig(LogsViewConfig orig) {
    this.awsProfile = orig.awsProfile;
    this.awsRegion = orig.awsRegion;
    this.name = orig.name;
    this.groups = new ArrayList<>(orig.groups);
    this.searchQuery = orig.searchQuery;
    this.parameters = orig.parameters != null ? new ArrayList<>(orig.parameters) : null;
    this.dateRange = orig.dateRange;
    this.askQuery = orig.askQuery;
    this.queryStr = orig.queryStr;
    this.bytesLimit = orig.bytesLimit;
  }

  public LogsViewConfig withSearchQuery(String query) {
    final var copy = new LogsViewConfig(this);
    copy.setSearchQuery(List.of(query));
    return copy;
  }

  public LogsViewConfig withRangeConfig(DateRangeConfig cfg) {
    final var copy = new LogsViewConfig(this);
    copy.setDateRange(cfg);
    return copy;
  }

  public LogsViewConfig withParameters(List<Map<String, String>> p) {
    final var copy = new LogsViewConfig(this);
    copy.setParameters(p);
    return copy;
  }

  public LogsViewConfig withAskQuery(boolean b) {
    final var copy = new LogsViewConfig(this);
    copy.askQuery = b;
    return copy;
  }

  public String getAwsProfile() {
    return awsProfile;
  }

  public List<Map<String, String>> getParameters() {
    return parameters;
  }

  public void setParameters(List<Map<String, String>> parameters) {
    this.parameters = parameters;
  }

  @JsonProperty(defaultValue = DEFAULT_PROFILE)
  public void setAwsProfile(String awsProfile) {
    this.awsProfile = awsProfile;
  }

  public String getAwsRegion() {
    return awsRegion;
  }

  public void setAwsRegion(String awsRegion) {
    this.awsRegion = awsRegion;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<String> getGroups() {
    return groups;
  }

  public void setGroups(List<String> groups) {
    this.groups = groups;
  }

  public List<String> getSearchQuery() {
    return searchQuery;
  }

  public boolean hasSearchQuery() {
    return searchQuery != null && !searchQuery.isEmpty();
  }

  public boolean hasParameters() {
    return parameters != null && !parameters.isEmpty();
  }

  @JsonDeserialize(using = QueryDeserializer.class)
  public void setSearchQuery(List<String> searchQuery) {
    if (searchQuery == null) {
      this.searchQuery = null;
      this.queryStr = null;
      return;
    }
    this.searchQuery = unmodifiableList(searchQuery);
    this.queryStr = searchQuery.stream().collect(joining(" "));
  }

  @JsonIgnore
  public DateRange dateRange() {
    return dateRange.resolve();
  }

  public DateRangeConfig getDateRange() {
    return dateRange;
  }

  public void setDateRange(DateRangeConfig range) {
    this.dateRange = range;
  }

  public QueryVisitor parseQuery() {
    final var iq = new InsightsQuery();
    final var query = iq.evalQuery(queryStr);
    final var visitor = new QueryVisitor();
    query.apply(visitor);
    return visitor;
  }

  public record DateRangeConfig(
      String type,
      @JsonFormat(shape = Shape.STRING) Duration relativeRange,
      @JsonIgnore ZonedDateTime start,
      @JsonIgnore ZonedDateTime end) {
    @JsonIgnore
    public boolean isAbsolute() {
      return type.equals("absolute");
    }

    @JsonIgnore
    public DateRange resolve() {
      if (start != null && end != null) {
        return new DateRange(start, end);
      } else if (relativeRange != null) {
        return DateRange.beforeNow(relativeRange);
      }
      return DateRange.beforeNow(Duration.ofHours(1));
    }
  }

  public boolean isAskQuery() {
    return askQuery;
  }

  public void setAskQuery(boolean b) {
    askQuery = b;
  }

  @JsonIgnore
  public boolean isAbsoluteDtRange() {
    return dateRange.isAbsolute();
  }

  @JsonIgnore
  public String resolveSearchQuery() {
    return queryStr != null
        ? queryStr
        : "fields @timestamp, @message\n | sort @timestamp desc\n | limit 10";
  }

  @JsonDeserialize(using = BytesDeserializer.class)
  public void setBytesLimit(BytesLimit v) {
    bytesLimit = v;
  }

  public Object getBytesLimit() {
    return bytesLimit != null ? bytesLimit.dbValue() : null;
  }

  record BytesLimit(long value, String format) {
    Object dbValue() {
      return switch (format) {
        case "number" -> value;
        case "unit" -> "%sMB".formatted(Math.floorDiv(value, 1024 * 1024));
        default -> throw new IllegalArgumentException("Unsupported format %s".formatted(format));
      };
    }
  }

  @JsonIgnore
  public long bytesLimit() {
    if (bytesLimit == null) {
      return 1024 * 1024 * 1024;
    } else if (bytesLimit.value() > 0) {
      return bytesLimit.value();
    }
    return Long.MAX_VALUE;
  }

  @Override
  public List<String> validateRuntime() {
    final var file = ProfileFile.defaultProfileFile();
    final var opt = file.profile(awsProfile);
    if (opt.isEmpty()) {
      return List.of("AWS profile '%s' doesn't exist".formatted(awsProfile));
    }
    try {
      final var utils = new ProfileCredentialsUtils(file, opt.get(), file::profile);
      utils.credentialsProvider();
    } catch (final SdkException e) {
      return List.of(e.getMessage());
    }
    return List.of();
  }

  static LogsViewConfig emptyConfig() {
    final var cfg = new LogsViewConfig();
    cfg.setAskQuery(false);
    cfg.setAwsProfile(DEFAULT_PROFILE);
    cfg.setAwsRegion("us-east-1");
    cfg.setBytesLimit(new BytesLimit(10 * 1024 * 1024, "unit"));
    cfg.setDateRange(new DateRangeConfig("absolute", Duration.ofHours(2), null, null));
    cfg.setGroups(List.of("/aws/lambda/function-*", "/aws/lambda/other-function"));
    cfg.setName("Lambda logs");
    cfg.setParameters(List.of(Map.of("name", "Transaction Id")));
    cfg.setSearchQuery(
        List.of(
            "fields @timestamp, logMessage, txId",
            "| filter txId == '&Transaction Id'",
            "| sort @timestamp asc"));
    return cfg;
  }
}

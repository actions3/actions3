/*
 * Actions3 Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.logs;

import com.milosz.apps.cm.logs.CloudWatchLogsLoader.LoadStats;
import java.awt.BorderLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

public class ProgressPanel extends JPanel implements PropertyChangeListener {

  private final JProgressBar progress;

  public ProgressPanel(LogsActionContainer actions) {
    super(new BorderLayout());
    this.progress = new JProgressBar();
    progress.setIndeterminate(true);
    progress.setStringPainted(true);
    add(progress, BorderLayout.CENTER);
    final var cancelBtn = new JButton("Cancel");
    cancelBtn.setActionCommand(LogsViewCommand.CANCEL_QUERY.command());
    cancelBtn.addActionListener(actions);
    add(cancelBtn, BorderLayout.LINE_END);
  }

  public void reset() {
    progress.setString("");
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (evt.getPropertyName().equals("statsProgress")) {
      final var stats = (LoadStats) evt.getNewValue();
      final var msg =
          "Query in progress for %s seconds. Matched %s records from %s scanned (%.4f MB)"
              .formatted(
                  stats.duration().getSeconds(),
                  stats.matched(),
                  stats.records(),
                  stats.bytes() / 1_000_000);
      progress.setString(msg);
    }
  }
}

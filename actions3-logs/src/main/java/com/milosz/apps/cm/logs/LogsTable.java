/*
 * Actions3 Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.logs;

import com.milosz.apps.cm.common.PropertyTable;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

public class LogsTable extends PropertyTable {

  private final Set<int[]> selectedCells = new TreeSet<>((a, b) -> Arrays.compare(a, b));

  public LogsTable(TableModel dm) {
    super(dm);
    setRowSelectionAllowed(false);
    setCellSelectionEnabled(true);
    getSelectionModel().addListSelectionListener(this::selectionChanged);
  }

  @Override
  public TableCellRenderer getCellRenderer(int row, int column) {
    return getDefaultRenderer(String.class);
  }

  @Override
  public TableCellEditor getCellEditor(int row, int column) {
    return getDefaultEditor(String.class);
  }

  private void selectionChanged(ListSelectionEvent e) {
    if (!e.getValueIsAdjusting()) {
      selectedCells.clear();
      for (final var c : getSelectedColumns()) {
        for (final var r : getSelectedRows()) {
          selectedCells.add(new int[] {r, c});
        }
      }
    }
  }

  public boolean hasSelectedCells() {
    return !selectedCells.isEmpty();
  }

  public List<String> selectedCellsContent() {
    return selectedCells.stream()
        .map(p -> getValueAt(p[0], p[1]))
        .filter(Objects::nonNull)
        .map(String::valueOf)
        .toList();
  }
}

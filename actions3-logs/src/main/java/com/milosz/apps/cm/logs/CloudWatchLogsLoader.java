/*
 * Actions3 Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.logs;

import static java.util.logging.Level.FINE;

import com.milosz.apps.cm.common.DateRange;
import com.milosz.apps.cm.common.LogUtils;
import com.milosz.apps.cm.spi.AppException;
import java.time.Duration;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.swing.SwingWorker;
import software.amazon.awssdk.services.cloudwatchlogs.CloudWatchLogsClient;
import software.amazon.awssdk.services.cloudwatchlogs.model.GetQueryResultsRequest;
import software.amazon.awssdk.services.cloudwatchlogs.model.GetQueryResultsResponse;
import software.amazon.awssdk.services.cloudwatchlogs.model.InvalidParameterException;
import software.amazon.awssdk.services.cloudwatchlogs.model.QueryStatus;
import software.amazon.awssdk.services.cloudwatchlogs.model.ResultField;
import software.amazon.awssdk.services.cloudwatchlogs.model.StartQueryRequest;
import software.amazon.awssdk.services.cloudwatchlogs.model.StopQueryRequest;

public class CloudWatchLogsLoader extends SwingWorker<Object, GetQueryResultsResponse> {

  private static final Logger LOG =
      LogUtils.getLogger(FINE, CloudWatchLogsLoader.class.getSimpleName());

  private final LogsViewConfig config;
  private final CloudWatchLogsClient client;
  private final LogsTableModel model;
  private LoadStats stats = LoadStats.empty();
  private int loaded;
  private final LinkedList<Exception> errors = new LinkedList<>();
  private final DateRange range;
  private final boolean reload;
  private String queryId;
  private Long queryStartTs;
  private final long bytesLimit;
  private final AtomicBoolean errorCommited = new AtomicBoolean(false);

  private final LogGroupList groups;

  public CloudWatchLogsLoader(
      LogsViewConfig config,
      LogsTableModel model,
      DateRange range,
      boolean reload,
      CloudWatchLogsClient sdkClient,
      LogGroupList groupsList) {
    this.config = config;
    client = sdkClient;

    this.model = model;
    this.range = range;
    this.reload = reload;
    this.groups = groupsList;
    bytesLimit = config.bytesLimit();
  }

  public LoadStats stats() {
    return stats;
  }

  public Optional<Exception> lastError() {
    return Optional.ofNullable(errors.peekLast());
  }

  @Override
  protected Object doInBackground() throws Exception {
    try {
      LOG.info("Search query [%s] %s".formatted(range, config.getSearchQuery()));
      if (reload) {
        model.clear();
      }
      final var logGroups = groupNames(config.getGroups());
      final var request =
          StartQueryRequest.builder()
              .queryString(config.resolveSearchQuery())
              .logGroupNames(logGroups)
              .endTime(range.end().toEpochSecond())
              .startTime(range.start().toEpochSecond())
              .build();
      queryStartTs = System.currentTimeMillis();
      final var start = client.startQuery(request);
      queryId = start.queryId();
      readRows(start.queryId());
      return null;
    } catch (final Exception e) {
      errors.add(e);
      LOG.log(Level.WARNING, "Exception", e);
      return null;
    }
  }

  String queryId() {
    return queryId;
  }

  public void commitErrors() {
    errorCommited.set(true);
  }

  public boolean errorsCommited() {
    return errorCommited.get();
  }

  @Override
  protected void process(List<GetQueryResultsResponse> chunks) {
    final var last = chunks.get(chunks.size() - 1).statistics();
    final var ls =
        new LoadStats(
            last.recordsMatched(),
            last.recordsScanned(),
            last.bytesScanned(),
            Duration.ofMillis(System.currentTimeMillis() - queryStartTs));
    if (ls.bytes() > bytesLimit) {
      final var message = "Auto cancel query after exceeding limit %s".formatted(bytesLimit);
      LOG.info(message);
      cancel(false);
      firePropertyChange("queryMessage", "", message);
      return;
    }
    firePropertyChange("statsProgress", stats, ls);
    stats = ls;
  }

  private Collection<String> groupNames(List<String> configGroups) {
    final var all = configGroups.stream().flatMap(g -> groups.get(g).stream()).distinct().toList();
    if (all.isEmpty()) {
      LOG.info("Group paterns don't match any group %s".formatted(configGroups.toString()));
      throw new AppException("Group patterns don't match any group");
    }
    LOG.info("Using %s log groups %s in search".formatted(all.size(), all));
    if (all.size() > 10) {
      throw new AppException("Group patterns match more than 10 log groups");
    }
    return all;
  }

  private void readRows(String queryId) {
    final var get = GetQueryResultsRequest.builder().queryId(queryId).build();

    var status = QueryStatus.SCHEDULED;
    GetQueryResultsResponse response = null;
    while (status == QueryStatus.RUNNING || status == QueryStatus.SCHEDULED) {
      if (isCancelled()) {
        LOG.info("Worker thread is canceled. Stop processing query %s".formatted(queryId));
        status = QueryStatus.CANCELLED;
        break;
      }
      response = client.getQueryResults(get);
      status = response.status();
      publish(response);
      try {
        Thread.sleep(1000);
      } catch (final InterruptedException e) {
        LOG.log(Level.WARNING, "Interrupted thread", e);
        throw new RuntimeException("Can't read logs from CloudWatch", e);
      }
    }
    tryCancel(false);

    if (status != QueryStatus.COMPLETE && status != QueryStatus.CANCELLED) {
      throw new IllegalStateException(
          "Query %s has failed with status %s".formatted(queryId, status));
    } else if (status == QueryStatus.CANCELLED) {
      LOG.info("Query %s is canceled. Showing only partial results".formatted(queryId));
    }

    if (response == null) {
      return;
    }
    publish(response);
    final var events =
        response.results().stream()
            .map(e -> e.stream().collect(Collectors.toMap(ResultField::field, ResultField::value)))
            .toList();
    loaded = events.size();
    model.addEvents(events);
  }

  public boolean tryCancel(boolean force) {
    if (isDone()) {
      return false;
    }
    if (!force && !isCancelled()) {
      return false;
    }
    LOG.info("Cancel query " + queryId);
    try {
      final var res = client.stopQuery(StopQueryRequest.builder().queryId(queryId).build());
      return res.success();
    } catch (final InvalidParameterException e) {
      if (e.getMessage().startsWith("Query is already ended with Complete")) {
        LOG.info("Query can't be canceled because already has completed");
      }
      return false;
    }
  }

  public int loaded() {
    return loaded;
  }

  record LoadStats(double matched, double records, double bytes, Duration duration) {
    /**
     * Calculates estimated query cost as described on https://aws.amazon.com/cloudwatch/pricing/
     *
     * @return query cost in dollars
     */
    double cost() {
      return bytes / 1_000_000_000 * 0.005;
    }

    public static LoadStats empty() {
      return new LoadStats(0, 0, 0, Duration.ZERO);
    }
  }
}

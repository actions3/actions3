/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.logs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import software.amazon.awssdk.services.cloudwatchlogs.CloudWatchLogsClient;
import software.amazon.awssdk.services.cloudwatchlogs.model.DescribeLogGroupsRequest;
import software.amazon.awssdk.services.cloudwatchlogs.model.LogGroup;

public class LogGroupList {
  private final CloudWatchLogsClient client;

  private final Map<String, List<String>> patternMap = new HashMap<>();

  public LogGroupList(CloudWatchLogsClient client) {
    this.client = client;
  }

  public synchronized List<String> get(String pattern) {
    if (!pattern.endsWith("*")) {
      return List.of(pattern);
    }
    if (patternMap.containsKey(pattern)) {
      return patternMap.get(pattern);
    }
    final var all =
        client.describeLogGroups(
            DescribeLogGroupsRequest.builder()
                .logGroupNamePrefix(pattern.substring(0, pattern.length() - 1))
                .build());
    final var groups = all.logGroups();
    final var names =
        groups.stream().map(LogGroup::logGroupName).collect(Collectors.toUnmodifiableList());
    patternMap.put(pattern, names);
    return names;
  }
}

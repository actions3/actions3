/*
 * Actions3 Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.logs;

import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class TextCellRenderer extends DefaultTableCellRenderer {

  private static final int MAX_LEN = 100;

  private final Map<Integer, Boolean> expand = new HashMap<>();
  private final Map<String, Boolean> prettyPrint = new HashMap<>();
  private final Formatter defaultFormatter = new Formatter();
  private final Formatter jsonFormatter = new JsonFormatter();
  private Pattern searchStr = null;

  public void reset() {
    expand.clear();
    prettyPrint.clear();
    searchStr = null;
  }

  public void expand(int row) {
    this.expand.put(row, !expand.getOrDefault(row, false));
  }

  public void prettyPrint(int row, int column) {
    final var rowId = "%s_%s".formatted(row, column);
    final var nv = !prettyPrint.getOrDefault(rowId, false);
    this.prettyPrint.put(rowId, nv);
    if (!this.expand.getOrDefault(row, false)) {
      this.expand.put(row, true);
    }
  }

  public void setSearchString(String v) {
    if (v == null) {
      searchStr = null;
    }
    try {
      searchStr = Pattern.compile(v);
    } catch (final Exception e) {
      e.printStackTrace();
      searchStr = null;
    }
  }

  private String search(Object v) {
    if (searchStr == null) {
      return v.toString();
    }
    final var matcher = searchStr.matcher(v.toString());
    if (!matcher.find()) {
      return v.toString();
    }
    return matcher.replaceAll(r -> "<span>%s</span>".formatted(r.group()));
  }

  @Override
  public Component getTableCellRendererComponent(
      JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

    final var display = displayValue(value, row, column);
    final var c =
        super.getTableCellRendererComponent(table, display, isSelected, hasFocus, row, column);
    final var rh = table.getRowHeight(row);
    if (c.getPreferredSize().height > rh) {
      table.setRowHeight(row, c.getPreferredSize().height);
    }
    return c;
  }

  String displayValue(Object value, int row, int column) {
    if (value == null) {
      return null;
    } else {
      final var full = expand.getOrDefault(row, Boolean.FALSE);
      var v = search(value);
      if (!prettyPrint.getOrDefault("%s_%s".formatted(row, column), false)) {
        v = defaultFormatter.format(v, full);
      } else {
        v = jsonFormatter.format(v, full);
      }

      return "<html><head><style>body { font-family: sans-serif; font-size: %spt;} span {background-color: yellow;}</style></head><body>%s</body></html>"
          .formatted(getFont().getSize(), v);
    }
  }

  @Override
  public Dimension getPreferredSize() {
    final var ps = super.getPreferredSize();
    return new Dimension(ps.width + 10, ps.height);
  }

  private static class Formatter {
    String format(Object value, boolean full) {
      final var lines = new ArrayList<String>();
      final var vs = value.toString().split("\\s");
      var line = "";
      for (final var w : vs) {
        if (w.length() > MAX_LEN) {
          if (!line.isEmpty()) {
            lines.add(line);
          }
          var wlong = w;
          while (wlong.length() > MAX_LEN) {
            lines.add(wlong.substring(0, MAX_LEN));
            wlong = wlong.substring(MAX_LEN);
          }
          line = wlong;
        } else if ((line + " " + w).length() <= MAX_LEN) {
          line += (" " + w);
        } else {
          lines.add(line);
          line = w;
        }
      }
      lines.add(line);

      final var maxLen = full ? Integer.MAX_VALUE : 5;
      var v = lines.stream().limit(maxLen).collect(Collectors.joining("<br>"));
      if (!full && lines.size() > 5) {
        v += "...";
      }
      return v;
    }
  }

  private static class JsonFormatter extends Formatter {
    private final ObjectMapper mapper;
    private final ObjectWriter writer;

    JsonFormatter() {
      this.mapper = new ObjectMapper();
      writer =
          mapper
              .writer()
              .with(
                  new DefaultPrettyPrinter()
                      .withObjectIndenter(
                          new DefaultIndenter().withLinefeed("<br>").withIndent("&nbsp;&nbsp;")));
    }

    @Override
    String format(Object value, boolean full) {
      try {
        final var node =
            mapper.readTree(value.toString().replace("<", "&lt;").replace(">", "&gt;"));
        return writer.writeValueAsString(node);
      } catch (final Exception e) {
        System.out.println(value);
      }
      return super.format(value, full);
    }
  }
}

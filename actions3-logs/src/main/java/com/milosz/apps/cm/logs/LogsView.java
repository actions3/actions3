/*
 * Actions3 Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.logs;

import static com.milosz.apps.cm.common.QueryParamsFormDialog.Param.date;
import static com.milosz.apps.cm.common.QueryParamsFormDialog.Param.multi;
import static com.milosz.apps.cm.common.QueryParamsFormDialog.showParamsDialog;
import static com.milosz.apps.cm.common.SwingSupport.showErrorDialog;
import static com.milosz.apps.cm.logs.QueryVisitor.prettify;
import static java.util.logging.Level.FINE;
import static javax.swing.JOptionPane.QUESTION_MESSAGE;
import static javax.swing.JOptionPane.showInputDialog;
import static javax.swing.JOptionPane.showMessageDialog;

import com.milosz.apps.cm.common.DateRange;
import com.milosz.apps.cm.common.DateUtils;
import com.milosz.apps.cm.common.LogUtils;
import com.milosz.apps.cm.common.QueryParamsFormDialog;
import com.milosz.apps.cm.common.QueryParamsFormDialog.InputValue;
import com.milosz.apps.cm.common.QueryParamsFormDialog.Param;
import com.milosz.apps.cm.common.QueryParamsFormDialog.TextValue;
import com.milosz.apps.cm.logs.InsightsQuery.SortCmd;
import com.milosz.apps.cm.logs.LogsViewConfig.DateRangeConfig;
import com.milosz.apps.cm.logs.SearchHistory.HistoryItem;
import com.milosz.apps.cm.spi.AppActionContainer;
import com.milosz.apps.cm.spi.AppException;
import com.milosz.apps.cm.spi.AppView;
import com.milosz.apps.cm.spi.AppViewConfig;
import com.milosz.apps.cm.spi.AppViewContext;
import com.milosz.apps.cm.spi.Application;
import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingWorker;
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.cloudwatchlogs.CloudWatchLogsClient;

public class LogsView extends JPanel implements AppView, PropertyChangeListener {

  private static final String START_DT_LABEL = "Start date";
  private static final String END_DT_LABEL = "End date";
  private static final String SEARCH_QUERY_LABEL = "Search query";
  private static final Logger LOG = LogUtils.getLogger(FINE, LogsView.class.getSimpleName());

  private LogsViewConfig config;
  private final PropertyChangeSupport changeSupport;
  private final LogsTable logs;
  private LogsTableModel tableModel;
  private final TextCellRenderer renderer = new TextCellRenderer();

  /** Time range used in latest query execution */
  private DateRange lastRange = null;

  private final AtomicBoolean loading = new AtomicBoolean(false);
  private QueryVisitor query;
  private String rawSearch;
  private final CloudWatchLogsClient sdkClient;
  private final LogGroupList groupsList;
  private final LogsActionContainer actions;
  private final TextCellEditor editor;
  private final Application main;
  private String lastQueryId;
  private final SearchHistory history;
  private final String name;
  private final String group;
  private CloudWatchLogsLoader logsLoader;
  private final ProgressPanel progress;

  public LogsView(AppViewContext ctx) {
    this((LogsViewConfig) ctx.getConfig(), ctx.getApp(), ctx.getGroup(), ctx.getName());
  }

  public LogsView(LogsViewConfig config, Application main, String group, String name) {
    this.main = main;
    this.config = config;
    this.changeSupport = new PropertyChangeSupport(this);
    setLayout(new BorderLayout());
    this.logs = new LogsTable(tableModel);
    actions = new LogsActionContainer(this);
    logs.setDefaultRenderer(String.class, renderer);
    editor = new TextCellEditor(actions);
    logs.setDefaultEditor(String.class, editor);
    add(new JScrollPane(logs), BorderLayout.CENTER);
    this.progress = new ProgressPanel(actions);
    this.sdkClient =
        CloudWatchLogsClient.builder()
            .credentialsProvider(ProfileCredentialsProvider.create(config.getAwsProfile()))
            .region(Region.of(config.getAwsRegion()))
            .build();
    groupsList = new LogGroupList(sdkClient);
    this.history = new SearchHistory();
    this.name = name;
    this.group = group;
  }

  @Override
  public JComponent component() {
    return this;
  }

  private List<Param> prepareSeachParams() {
    final var params = new ArrayList<Param>();
    if (config.getDateRange() != null && config.getDateRange().isAbsolute()) {
      final var initRange = config.dateRange();
      params.addAll(
          List.of(date(START_DT_LABEL, initRange.start()), date(END_DT_LABEL, initRange.end())));
    }
    if (config.hasParameters()) {
      config.getParameters().stream()
          .map(m -> m.get("name"))
          .map(n -> new Param(n, "txt"))
          .forEach(params::add);
    } else if (!config.hasSearchQuery() || config.isAskQuery()) {
      final var sq = config.resolveSearchQuery();
      params.add(queryParam(sq));
    }
    return params;
  }

  private DateRange startDateRange(List<? extends InputValue<?>> parameters) {
    if (config.getDateRange() == null) {
      return config.dateRange();
    } else if (!config.getDateRange().isAbsolute()) {
      return DateRange.beforeNow(config.getDateRange().relativeRange());
    } else {
      return dateRangeFromParams(parameters);
    }
  }

  private DateRange dateRangeFromParams(List<? extends InputValue<?>> parameters) {
    final var dates =
        parameters.stream()
            .filter(p -> p.name().contains("DATE"))
            .collect(Collectors.toMap(InputValue::label, iv -> iv.value().toString()));
    final var start = DateUtils.parseLocalInput(dates.get(START_DT_LABEL));
    final var end = DateUtils.parseLocalInput(dates.get(END_DT_LABEL));
    return new DateRange(start, end);
  }

  @Override
  public void start() {
    final var initParams = prepareSeachParams();
    if (!initParams.isEmpty()) {
      final List<? extends InputValue<?>> userInput = showParamsDialog(initParams);
      if (userInput.isEmpty()) {
        return;
      }
      if (config.hasParameters() && config.hasSearchQuery()) {
        var rawQuery = config.resolveSearchQuery();
        for (final var v : userInput) {
          if (v instanceof final TextValue tv) {
            rawQuery = rawQuery.replace("&" + v.label(), tv.value());
          }
        }
        config = config.withSearchQuery(rawQuery);
      } else if (!config.hasSearchQuery() || config.isAskQuery()) {
        final var sq = userInput.stream().filter(v -> v.name().equals("SEARCH_QUERY")).findFirst();
        if (sq.isEmpty()) {
          return;
        }
        config = config.withSearchQuery(sq.get().value().toString());
      }
      lastRange = startDateRange(userInput);
      config =
          config.withRangeConfig(
              new DateRangeConfig("absolute", null, lastRange.start(), lastRange.end()));
    } else {
      lastRange = config.dateRange();
    }
    this.query = config.parseQuery();
    tableModel = new LogsTableModel(query);
    logs.setModel(tableModel);

    loadData();
  }

  private void loadData() {
    loadData(false, false);
  }

  private void loadData(boolean reload) {
    loadData(reload, false);
  }

  private void loadData(boolean reload, boolean useLastRange) {
    try {
      renderer.reset();
      loading.set(true);
      var cfg = config;
      if (rawSearch != null) {
        cfg = cfg.withSearchQuery(rawSearch);
        rawSearch = null;
      }
      if (config.isAbsoluteDtRange() && reload && !useLastRange) {
        // when absolute range reload action always use date range selected by the user
        lastRange = config.dateRange();
      } else if (!config.isAbsoluteDtRange() && reload) {
        // when relative range we assume that users always want to see most recent events
        lastRange = config.dateRange();
      }
      if (reload) {
        tableModel.setQuery(cfg.parseQuery());
      }
      logsLoader =
          new CloudWatchLogsLoader(cfg, tableModel, lastRange, reload, sdkClient, groupsList);
      logsLoader.addPropertyChangeListener(this);
      progress.reset();
      add(progress, BorderLayout.PAGE_END);
      logsLoader.addPropertyChangeListener(progress);
      logsLoader.execute();
    } catch (final Exception e) {
      logsLoader.removePropertyChangeListener(progress);
      remove(progress);
      loading.set(false);
      LOG.log(Level.WARNING, "Loading failure", e);
      throw new AppException("Can't load logs from AWS");
    }
  }

  public void expand() {
    final var row = logs.getSelectedRow();
    renderer.expand(row);
    logs.setRowHeight(row, 1);
    tableModel.fireTableRowsUpdated(row, row);
  }

  public void format() {
    final var row = logs.getSelectedRow();
    final var column = logs.getSelectedColumn();
    renderer.prettyPrint(row, column);
    logs.setRowHeight(row, 1);
    tableModel.fireTableRowsUpdated(row, row);
  }

  public void search() {
    final var str = showInputDialog(this, "Search string");
    if (str != null) {
      renderer.setSearchString(str);
      tableModel.fireTableDataChanged();
    }
  }

  public void cancelSearch() {
    renderer.setSearchString(null);
    tableModel.fireTableDataChanged();
  }

  public void nextWindow() {
    if (loading.get()) {
      showMessageDialog(this, "Query in progress. Please wait until loading will finish");
      return;
    }
    final var order = query.getSortOrder().orElse("desc");
    if (query.getSort().isEmpty() || query.getSort().size() > 1) {
      changeSupport.firePropertyChange(STATUS_PROP, "", "Query don't have 'sort' command");
      return;
    }
    if ("desc".equals(order)) {
      lastRange = lastRange.backward();
    } else {
      lastRange = lastRange.forward();
    }
    tableModel.setMark();
    loadData();
  }

  public void loadMore() {
    if (loading.get()) {
      showMessageDialog(this, "Query in progress. Please wait until loading will finish");
      return;
    }
    final var order = query.getSortOrder().orElse("desc");
    if (query.getSort().isEmpty() || query.getSort().size() > 1) {
      changeSupport.firePropertyChange(STATUS_PROP, "", "Query don't have 'sort' command");
      return; // no sort clause
    }
    final var sf = query.getSort().get(0);
    if (!query.isTemporal(sf)) {
      changeSupport.firePropertyChange(
          STATUS_PROP, "", "Results are not sorted by field containing timestamp");
      return;
    }
    final var last = tableModel.getLastValue(sf);
    if (last.isEmpty()) {
      changeSupport.firePropertyChange(
          STATUS_PROP, "", "Either empty table or missing temporal column");
      return;
    }
    final var op = order.equals("desc") ? "<" : ">";
    final var raw = config.resolveSearchQuery();
    rawSearch =
        QueryVisitor.rewriteQuery(
            raw, "filter %s %s '%s'".formatted(sf, op, last.get()), c -> c instanceof SortCmd);
    tableModel.setMark();
    loadData();
  }

  public void showQuery() {
    final var query = config.resolveSearchQuery();
    final var pretty = prettify(query, "--SEP--").get();
    final var html =
        "<html><p>%s</p><p>Estimated cost: %.6f</p></html>"
            .formatted(pretty.replace("--SEP--", "<br>"), logsLoader.stats().cost());
    showMessageDialog(null, html);
    Toolkit.getDefaultToolkit()
        .getSystemClipboard()
        .setContents(new StringSelection(pretty.replace("--SEP--", "\n")), null);
  }

  @Override
  public void shutdown() {
    history.close();
    if (logsLoader != null) {
      logsLoader.tryCancel(true);
    }
  }

  @Override
  public AppActionContainer actions() {
    return actions;
  }

  @Override
  public PropertyChangeSupport changeSupport() {
    return changeSupport;
  }

  @Override
  public AppViewConfig viewConfig() {
    return config;
  }

  @Override
  public String type() {
    return "Logs";
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (evt.getSource() instanceof final CloudWatchLogsLoader cwll) {
      final var lastError = cwll.lastError();
      if (lastError.isPresent()) {
        loading.set(false);
        if (cwll.errorsCommited()) {
          return;
        }
        var lastMsg = lastError.get().getMessage();
        if (lastMsg.length() > 100) {
          lastMsg = lastMsg.substring(0, 100) + "...";
        }
        var msg =
            "Couldn't fetch data. Check logs for more details<br>Last error: %s".formatted(lastMsg);
        if (lastError.get() instanceof SdkException) {
          msg += "<ul>";
          msg += "<li>AWS profile: %s".formatted(config.getAwsProfile());
          msg += "<li><p>AWS region: %s".formatted(config.getAwsRegion());
          msg += "</ul>";
        }
        showErrorDialog(logs, msg, lastError.get());
        cwll.commitErrors();
        remove(progress);
        cwll.removePropertyChangeListener(progress);
      } else if (evt.getPropertyName().equals("state")
          && SwingWorker.StateValue.DONE.equals(evt.getNewValue())) {
        final var stats = cwll.stats();
        setStatus(
            "Loaded %s rows from %s matching records (%.4f MB) in %s seconds. Estimated cost %.6f$",
            cwll.loaded(),
            stats.matched(),
            stats.bytes() / 1_000_000,
            stats.duration().getSeconds(),
            stats.cost());
        loading.set(false);
        lastQueryId = cwll.queryId();
        history.addQuery(config.resolveSearchQuery(), lastQueryId, lastRange);
        remove(progress);
      } else if (evt.getPropertyName().equals("state")) {
        changeSupport.firePropertyChange(STATUS_PROP, "", null);
      } else if (evt.getPropertyName().equals("queryMessage")) {
        setStatus(evt.getNewValue().toString());
      }
    }
  }

  public void copyCells() {
    if (logs.hasSelectedCells()) {
      final var selection = logs.selectedCellsContent().stream().collect(Collectors.joining("\n"));
      Toolkit.getDefaultToolkit()
          .getSystemClipboard()
          .setContents(new StringSelection(selection), null);
    } else {
      final var all = new StringJoiner("\n");
      final var cjoiner = new StringJoiner(";");
      for (var c = 0; c < tableModel.getColumnCount(); c++) {

        cjoiner.add(tableModel.getColumnName(c));
      }
      all.add(cjoiner.toString());
      for (var r = 0; r < tableModel.getRowCount(); r++) {
        final var joiner = new StringJoiner(";");
        for (var c = 0; c < tableModel.getColumnCount(); c++) {
          final var v = tableModel.getValueAt(r, c);
          if (v != null) {
            joiner.add(v.toString());
          } else {
            joiner.add("");
          }
        }
        all.add(joiner.toString());
      }
      Toolkit.getDefaultToolkit()
          .getSystemClipboard()
          .setContents(new StringSelection(all.toString()), null);
    }
  }

  public void reload() {
    loadData(true);
  }

  private Param queryParam(String query) {
    return multi(
        SEARCH_QUERY_LABEL,
        prettify(query, "\n").orElse(query),
        v -> {
          if (v == null || v.toString().isBlank()) {
            return false;
          }
          return prettify(v.toString(), " ").isPresent();
        });
  }

  public void runAgain() {
    final var params = new ArrayList<Param>();
    if (config.isAbsoluteDtRange()) {
      params.addAll(
          List.of(date(START_DT_LABEL, lastRange.start()), date(END_DT_LABEL, lastRange.end())));
    }
    params.add(queryParam(config.resolveSearchQuery()));

    final var input = QueryParamsFormDialog.showParamsDialog(params);
    if (input.isEmpty()) {
      return;
    }
    if (config.isAbsoluteDtRange()) {
      lastRange = dateRangeFromParams(input);
    }
    final var sq = input.stream().filter(v -> v.name().equals("SEARCH_QUERY")).findFirst();
    if (sq.isEmpty()) {
      return;
    }
    config = config.withSearchQuery(sq.get().value().toString());
    loadData(true, true);
  }

  public void addDynamicColumn() {
    final var def = showInputDialog("New column definition");
    if (def == null) {
      return;
    }
    final var re = Pattern.compile("^([1-9]+)(/.+)$");
    final var match = re.matcher(def);
    if (match.find()) {
      final var col = match.group(1);
      final var path = match.group(2);
      final var r = tableModel.addDynamicColumn(Integer.valueOf(col) - 1, path);
      if (r == LogsTableModel.DYNAMIC_COLUMN) {
        showMessageDialog(null, "Dynamic column can't be source of another dynamic column");
      }
    } else {
      showMessageDialog(null, "Path %s is invalid".formatted(def));
    }
  }

  public void runHistory() {
    final var item =
        QueryChoiceDialog.select(
            history,
            history.all().stream()
                .sorted(Comparator.comparing(HistoryItem::ts).reversed())
                .toList());
    if (item.isEmpty()) {
      return;
    }
    final var q = item.get();
    main.openView(
        group,
        name,
        "Logs",
        config
            .withSearchQuery(prettify(q.query(), "\n").orElse(q.query()))
            .withRangeConfig(new DateRangeConfig("absolute", null, q.start(), q.end()))
            .withParameters(List.of())
            .withAskQuery(true));
  }

  private void setStatus(String fmt, Object... args) {
    changeSupport.firePropertyChange(STATUS_PROP, "", fmt.formatted(args));
  }

  public void cancelQuery() {
    if (loading.get()) {
      logsLoader.cancel(false);
    }
  }

  public void searchMore() {
    final var logActions =
        main.treeActions(LogsViewConfig.class).stream()
            .filter(
                a -> {
                  final var cfg = (LogsViewConfig) a.getConfig();
                  return cfg.getParameters() == null || cfg.getParameters().isEmpty();
                })
            .toList();
    final var options =
        logActions.stream().map(a -> "%s / %s".formatted(a.getGroup(), a.getName())).toList();
    final var opt =
        showInputDialog(
            null,
            "Select action to execute",
            "Search more ...",
            QUESTION_MESSAGE,
            null,
            options.toArray(),
            options.get(0));
    if (opt != null) {
      final var index = options.indexOf(opt);
      final var selected = logActions.get(index);
      final var cfg = (LogsViewConfig) selected.getConfig();
      final var sq =
          QueryVisitor.rewriteQuery(
              cfg.resolveSearchQuery(),
              "filter @message like '%s'".formatted(editor.selectedText()),
              c -> c instanceof SortCmd);
      main.openView(selected.getGroup(), selected.getName(), "Logs", cfg.withSearchQuery(sq));
    }
  }
}

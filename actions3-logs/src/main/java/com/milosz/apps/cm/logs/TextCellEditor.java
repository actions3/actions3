/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.logs;

import static com.milosz.apps.cm.common.MouseEventHandler.isRightClick;

import com.milosz.apps.cm.common.ActionHandler;
import com.milosz.apps.cm.common.MouseEventHandler;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.util.EventObject;
import java.util.function.Consumer;
import javax.swing.AbstractCellEditor;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.table.TableCellEditor;

public class TextCellEditor extends AbstractCellEditor
    implements TableCellEditor, Consumer<MouseEvent> {

  private final JTextPane editorComponent;
  private final JPopupMenu popup;
  private final ActionHandler actions;

  public TextCellEditor(ActionHandler ah) {
    editorComponent = new JTextPane();
    editorComponent.setContentType("text/html");
    editorComponent.setEditable(false);
    editorComponent.addMouseListener(MouseEventHandler.of(this));
    actions = ah;

    popup = new JPopupMenu();
    final var item = new JMenuItem("Search more...");
    item.setActionCommand(LogsViewCommand.SEARCH_MORE.command());
    item.addActionListener(actions);
    popup.add(item);
  }

  public String selectedText() {
    return editorComponent.getSelectedText();
  }

  @Override
  public Object getCellEditorValue() {
    return editorComponent.getText();
  }

  @Override
  public Component getTableCellEditorComponent(
      JTable table, Object value, boolean isSelected, int row, int column) {
    final var renderer = (TextCellRenderer) table.getCellRenderer(row, column);
    final var c =
        renderer.getTableCellRendererComponent(table, value, isSelected, true, row, column);
    if (c != null) {
      editorComponent.setOpaque(true);
      editorComponent.setBackground(Color.WHITE);
      if (c instanceof JComponent) {
        editorComponent.setBorder(((JComponent) c).getBorder());
      }
    } else {
      editorComponent.setOpaque(false);
    }
    editorComponent.setText(renderer.getText());
    editorComponent.setPreferredSize(c.getPreferredSize());
    return editorComponent;
  }

  @Override
  public boolean isCellEditable(EventObject anEvent) {
    if (anEvent instanceof final MouseEvent e) {
      return e.getClickCount() >= 2;
    }
    return true;
  }

  @Override
  public void accept(MouseEvent e) {
    if (isRightClick(e)) {
      popup.show(e.getComponent(), e.getX(), e.getY());
    }
  }
}

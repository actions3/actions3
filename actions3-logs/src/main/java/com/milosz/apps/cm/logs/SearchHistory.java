/*
 * Actions3 Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.logs;

import static com.milosz.apps.cm.common.store.Bytes.array;
import static java.util.logging.Level.FINE;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.milosz.apps.cm.common.Common;
import com.milosz.apps.cm.common.DateRange;
import com.milosz.apps.cm.common.JsonSupport;
import com.milosz.apps.cm.common.LogUtils;
import com.milosz.apps.cm.common.store.AnyNode;
import com.milosz.apps.cm.common.store.ExtA3Db;
import com.milosz.apps.cm.common.store.Key;
import java.io.File;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.logging.Logger;

public class SearchHistory {
  private static final Logger LOG = LogUtils.getLogger(FINE, SearchHistory.class.getSimpleName());
  private static final String HISTORY_TABLE = "history";
  private static final String HISTORY_INDEX = "sort_ts_asc";
  private static final int MAX_SIZE = 50;
  private static final Predicate<Key> HISTORY = k -> k.pkStr().equals(HISTORY_TABLE);
  private static final Predicate<Key> INDEX = k -> k.pkStr().equals(HISTORY_INDEX);

  private final ExtA3Db db;
  private final JsonSupport mapper = JsonSupport.INSTANCE;

  public SearchHistory() {
    this(Common.getConfigDir().resolve("logs-history.db").toFile());
  }

  SearchHistory(File f) {
    db = new ExtA3Db(f);
  }

  public void reindex(boolean truncate) {
    db.begin();
    db.list(
            HISTORY,
            (k, b) -> {
              final var item = mapper.read(b, HistoryItem.class);
              return new AnyNode<HistoryItem>(item, k);
            })
        .forEach(
            h -> {
              final var item = h.value();
              db.put(Key.of(HISTORY_INDEX, array(item.ts().toEpochMilli())), array(item.queryId()));
            });
    db.commit();
    if (truncate) {
      db.begin();
      final var allItems = db.findAll(INDEX);
      allItems.stream()
          .limit(allItems.size() - MAX_SIZE)
          .forEach(
              h -> {
                db.remove(queryKey(h.value()));
                db.remove(h.key());
              });
      db.commit();
    }
  }

  public void addQuery(String query, String queryId, DateRange range) {
    db.begin();
    removeOldest();
    final var item = new HistoryItem(queryId, query, Instant.now(), range.start(), range.end());
    db.put(queryKey(queryId), mapper.bytes(item));
    db.put(Key.of(HISTORY_INDEX, array(item.ts().toString())), array(queryId));
    db.commit();
  }

  private void removeOldest() {
    final var oldest = db.findAll(INDEX);
    if (oldest.size() < MAX_SIZE) {
      return;
    }
    final var oldestIdx = oldest.get(0);
    LOG.info("Remove oldest %s".formatted(oldestIdx.key().skStr()));
    db.remove(queryKey(oldestIdx.value()));
    db.remove(oldestIdx.key());
  }

  public List<HistoryItem> all() {
    return db.findAll(INDEX).stream()
        .map(
            n -> {
              final var qid = n.value();
              return db.get(queryKey(qid)).map(c -> mapper.read(c, HistoryItem.class));
            })
        .filter(Optional::isPresent)
        .map(Optional::get)
        .toList();
  }

  public void close() {
    db.close();
  }

  private Key queryKey(String queryId) {
    return Key.of(HISTORY_TABLE, queryId);
  }

  record HistoryItem(
      String queryId,
      String query,
      Instant ts,
      @JsonFormat(shape = Shape.STRING) ZonedDateTime start,
      @JsonFormat(shape = Shape.STRING) ZonedDateTime end) {}
}

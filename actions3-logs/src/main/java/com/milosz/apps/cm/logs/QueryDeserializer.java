/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.logs;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.TextNode;
import java.io.IOException;
import java.util.List;
import java.util.stream.StreamSupport;

/** Deserializer for "searchQuery" property that can be plain string or list of strings. */
public class QueryDeserializer extends StdDeserializer<List<String>> {

  public QueryDeserializer() {
    this(null);
  }

  public QueryDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public List<String> deserialize(JsonParser p, DeserializationContext ctxt)
      throws IOException, JacksonException {
    final var node = p.getCodec().readTree(p);
    if (node instanceof final TextNode t) {
      return List.of(t.asText());
    } else if (node instanceof final ArrayNode an) {
      return StreamSupport.stream(an.spliterator(), false)
          .map(TextNode.class::cast)
          .map(TextNode::asText)
          .toList();
    }
    throw new IllegalArgumentException("Can't deserialize query from type " + node.getClass());
  }
}

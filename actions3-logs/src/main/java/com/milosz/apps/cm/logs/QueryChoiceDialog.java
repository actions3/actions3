/*
 * Actions3 Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.logs;

import static com.milosz.apps.cm.common.SwingSupport.newButton;
import static com.milosz.apps.cm.logs.QueryVisitor.prettify;

import com.milosz.apps.cm.common.DateUtils;
import com.milosz.apps.cm.logs.SearchHistory.HistoryItem;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Optional;
import javax.swing.Box;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

public class QueryChoiceDialog extends JDialog implements ActionListener {

  private static final String[] COLUMNS = {"Last run", "Query"};

  private final JTable table;
  private HistoryItem selected = null;
  private final List<HistoryItem> items;

  private final SearchHistory history;

  public QueryChoiceDialog(SearchHistory history, List<HistoryItem> items) {
    setLayout(new BorderLayout());
    final var box = Box.createHorizontalBox();
    box.add(newButton("Select", this));
    box.add(newButton("Cancel", this));
    box.add(newButton("Reindex", this));
    this.items = items;
    this.table = new JTable(new QueryChoiceModel(items));
    table.setDefaultRenderer(Object.class, new CellRenderer());
    this.history = history;
    add(new JScrollPane(table));
    add(box, BorderLayout.SOUTH);
    pack();
  }

  public static Optional<HistoryItem> select(SearchHistory history, List<HistoryItem> items) {
    final var dialog = new QueryChoiceDialog(history, items);
    dialog.setModal(true);
    dialog.setVisible(true);
    return Optional.ofNullable(dialog.selected);
  }

  private static class QueryChoiceModel extends AbstractTableModel {
    private final List<HistoryItem> items;
    private final Object[] prettyQuery;

    QueryChoiceModel(List<HistoryItem> items) {
      this.items = items;
      prettyQuery =
          items.stream()
              .map(h -> prettify(h.query(), "<br>"))
              .filter(Optional::isPresent)
              .map(h -> "<html><body>" + h.get() + "</body></html>")
              .toArray();
    }

    @Override
    public String getColumnName(int column) {
      return COLUMNS[column];
    }

    @Override
    public int getRowCount() {
      return prettyQuery.length;
    }

    @Override
    public int getColumnCount() {
      return COLUMNS.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      final var item = items.get(rowIndex);
      switch (columnIndex) {
        case 0:
          return DateUtils.format(DateUtils.of(item.ts()));
        case 1:
          return prettyQuery[rowIndex];
        default:
          return null;
      }
    }
  }

  private static class CellRenderer extends DefaultTableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(
        JTable table, Object display, boolean isSelected, boolean hasFocus, int row, int column) {
      final var c =
          super.getTableCellRendererComponent(table, display, isSelected, hasFocus, row, column);
      final var rh = table.getRowHeight(row);
      if (c.getPreferredSize().height > rh) {
        table.setRowHeight(row, c.getPreferredSize().height);
      }
      return c;
    }
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    switch (e.getActionCommand()) {
      case "$CMD_SELECT":
        final var i = table.getSelectedRow();
        if (i >= 0) {
          selected = items.get(i);
          setVisible(false);
        } else {
          JOptionPane.showMessageDialog(this, "Select item in table");
        }
        break;
      case "$CMD_CANCEL":
        selected = null;
        setVisible(false);
        break;
      case "$CMD_REINDEX":
        history.reindex(false);
        selected = null;
        setVisible(false);
        break;
      default:
        throw new IllegalArgumentException("Unsupported action " + e.getActionCommand());
    }
  }
}

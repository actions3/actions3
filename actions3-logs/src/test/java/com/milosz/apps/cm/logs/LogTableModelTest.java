/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.logs;

import static com.milosz.apps.cm.logs.LogsTableModel.merge;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

public class LogTableModelTest {
  @ParameterizedTest
  @MethodSource("smallArgs")
  public void shouldMerge(List<String> small) {
    merge(small, List.of("a", "b", "c"));
    assertIterableEquals(List.of("a", "b", "c"), small);
  }

  static Stream<List<String>> smallArgs() {
    return Stream.of(list("a"), list(), list("c"), list("b"), list("a", "c"), list("a", "b"));
  }

  static List<String> list(String... v) {
    return new ArrayList<>(List.of(v));
  }
}

/*
 * Actions3 Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3. If not,
 * see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.logs;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;

import com.milosz.apps.cm.common.DateRange;
import com.milosz.apps.cm.logs.SearchHistory.HistoryItem;
import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SearchHistoryTest {
  private SearchHistory underTest;

  @BeforeEach
  public void setup() throws IOException {
    final var file = File.createTempFile(UUID.randomUUID().toString().replaceAll("-", ""), "");
    underTest = new SearchHistory(file);
  }

  @Test
  public void shouldReturnListInInsertOrder() {
    final var queries =
        IntStream.range(0, 10)
            .boxed()
            .map(String::valueOf)
            .map(
                s -> {
                  return new HistoryItem(
                      s,
                      "query-" + s,
                      Instant.now(),
                      ZonedDateTime.now().minusDays(1),
                      ZonedDateTime.now());
                })
            .collect(Collectors.toCollection(ArrayList::new));
    Collections.shuffle(queries);
    queries.forEach(
        q -> underTest.addQuery(q.query(), q.queryId(), new DateRange(q.start(), q.end())));

    final var all = underTest.all().stream().map(h -> h.queryId()).toList();
    final var expected = queries.stream().map(q -> q.queryId()).toList();

    assertIterableEquals(expected, all);
  }

  @AfterEach
  public void cleanup() {
    underTest.close();
  }
}

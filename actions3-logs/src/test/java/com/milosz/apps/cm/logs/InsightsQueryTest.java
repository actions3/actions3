package com.milosz.apps.cm.logs;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;
import static org.junit.jupiter.params.provider.Arguments.of;

import com.milosz.apps.cm.logs.InsightsQuery.DisplayField;
import com.milosz.apps.cm.logs.InsightsQuery.Generic;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class InsightsQueryTest {

  private InsightsQuery underTest;
  private QueryVisitor visitor;

  @BeforeEach
  public void setup() {
    underTest = new InsightsQuery();
    visitor = new QueryVisitor();
  }

  @Test
  public void shouldParseFullQuery() {
    final var iq = new InsightsQuery();
    final var raw =
        "fields (a+1 >= 0) as @x, B * (83 / 15) - (r not like /regex/) as @y, substr(x, open(f), 99), sub2(143), body.param, `tx_id` as TxId "
            + "| parse @message \"tmpl\" as @tmpl,@t"
            + "| filter ((a > b) and (b >= c) or (y ~= /g/ and false) * 1) "
            + "| filter txId like '&TxId'"
            + "| parse @message /user=(?<user2>.*?), method:(?<method2>.*?),\r\n"
            + "    latency := (?<latency2>.*?)/"
            + "| stats avg(body.param) by bin(1hr)"
            + "| sort @timestamp, @message desc | limit 98";
    final var r = iq.evalQuery(raw);
    System.out.println("-------------------------------------------------");
    r.print(0);
    System.out.print("----------------------------------------------------");

    r.apply(visitor);

    assertIterableEquals(
        List.of("@tmpl", "@t", "user2", "method2", "latency2"), visitor.getParseAliases());
    assertIterableEquals(List.of(f("avg(body.param)"), tf("bin(1hr)")), visitor.getFields());
    assertIterableEquals(List.of("@timestamp", "@message"), visitor.getSort());
    assertEquals("desc", visitor.getSortOrder().get());
  }

  @Test
  public void shouldParseQueryWithDedup() {
    final var iq = new InsightsQuery();
    final var raw = "fields `tx_id` as TxId | dedup @y";
    final var r = iq.evalQuery(raw);
    System.out.println("-------------------------------------------------");
    r.print(0);
    System.out.print("----------------------------------------------------");

    r.apply(visitor);

    assertIterableEquals(List.of(f("TxId")), visitor.getFields());
  }

  @MethodSource("raiseExceptionArgs")
  @ParameterizedTest
  public void shouldRaiseException(String query, String expectedMessage) {
    final var ex = assertThrowsExactly(BadQueryException.class, () -> underTest.evalQuery(query));
    assertEquals(expectedMessage, ex.getMessage());
  }

  @MethodSource("extractFieldsArgs")
  @ParameterizedTest
  public void shouldExtractFields(String query, List<DisplayField> expectedFields) {
    final var r = underTest.evalQuery(query);
    System.out.println("-------------------------------------------------");
    r.print(0);
    System.out.print("----------------------------------------------------");

    r.apply(visitor);
    assertIterableEquals(expectedFields, visitor.getFields());
  }

  @MethodSource("parseStatsArgs")
  @ParameterizedTest
  public void parseStats(String query, List<DisplayField> expected) {
    final var r = underTest.evalQuery(query);
    System.out.println("-------------------------------------------------");
    r.print(0);
    System.out.print("----------------------------------------------------");

    r.apply(visitor);
    assertIterableEquals(expected, visitor.getFields());
  }

  @MethodSource("parseFilterArgs")
  @ParameterizedTest
  public void parseFilter(String query, List<String> expected) {
    final var r = underTest.evalQuery(query);
    System.out.println("-------------------------------------------------");
    r.print(0);
    System.out.print("----------------------------------------------------");

    final var filterVisitor =
        new QueryNodeVisitor() {
          private final List<String> values = new ArrayList<>();

          @Override
          public void visit(Generic node) {
            values.add(node.value());
          }
        };

    r.apply(filterVisitor);
    assertIterableEquals(expected, filterVisitor.values);
  }

  @MethodSource("extractParseArgs")
  @ParameterizedTest
  public void shouldExtractParse(String query, List<String> expected) {
    final var r = underTest.evalQuery(query);
    System.out.println("-------------------------------------------------");
    r.print(0);
    System.out.print("----------------------------------------------------");

    r.apply(visitor);
    assertIterableEquals(expected, visitor.getParseAliases());
  }

  static Stream<Arguments> extractFieldsArgs() {
    return Stream.of(
        // display command clears fields list
        of(
            "fields @message, @timestamp | display msgDisplay, tsDisplay",
            List.of(f("msgDisplay"), f("tsDisplay"))),
        of(
            "display msgDisplay | fields @message, @timestamp",
            List.of(f("msgDisplay"), f("@message"), tf("@timestamp"))),
        of(
            "fields @message, @timestamp | display msgDisplay2 | fields @message, @timestamp",
            List.of(f("msgDisplay2"), f("@message"), tf("@timestamp"))),
        of("fields @message, @timestamp", List.of(f("@message"), tf("@timestamp"))),
        of("fields @message as m, @timestamp as t", List.of(f("m"), tf("t"))),
        of("display @message, @timestamp", List.of(f("@message"), tf("@timestamp"))),
        of("fields @message as `m-x-y`, @timestamp as t", List.of(f("`m-x-y`"), tf("t"))),
        of("fields B * (83 / 15) - (r not like /regex/) as @y", List.of(f("@y"))),
        of(
            "fields B * (83 / 15) - (r not like /regex/)",
            List.of(f("B * (83 / 15) - (r not like /regex/)"))),
        of("fields B * (83 / 15) - (r not like /regex/) as @alias", List.of(f("@alias"))),
        of("fields fn(arg1)", List.of(f("fn(arg1)"))),
        of(
            "fields fn(arg1) as f1 | stats sum(f1 + 2) as sumval by bin(1yr)",
            List.of(f("sumval"), tf("bin(1yr)"))));
  }

  static Stream<Arguments> extractParseArgs() {
    return Stream.of(
        of("parse @message \"* [*] *\" as A, B, C", List.of("A", "B", "C")),
        of(
            "parse @message /user=(?<user2>.*?), method:(?<method2>.*?),\r\n"
                + "    latency := (?<latency2>.*?)/",
            List.of("user2", "method2", "latency2")));
  }

  static Stream<Arguments> parseFilterArgs() {
    return Stream.of(
        of("filter (x == 1 or (y==2 ))", List.of("filter", "x", "1", "or", "y", "2")),
        of("filter @message like 'abc'", List.of("filter", "@message", "like", "'abc'")),
        of("filter @message not like 'abc'", List.of("filter", "@message", "not", "like", "'abc'")),
        of(
            "filter details.log_info like 'abc'",
            List.of("filter", "details.log_info", "like", "'abc'")));
  }

  static Stream<Arguments> parseStatsArgs() {
    return Stream.of(
        of("stats count()", List.of(f("count()"))),
        of("stats avg(numvalue)", List.of(f("avg(numvalue)"))),
        of("stats avg(numvalue) by other", List.of(f("avg(numvalue)"), f("other"))),
        of("stats avg(numvalue) by bin(1hr)", List.of(f("avg(numvalue)"), tf("bin(1hr)"))),
        of("stats avg(numvalue) by bin(1hr) as x", List.of(f("avg(numvalue)"), tf("x"))),
        of("stats avg(numvalue) as y by bin(1hr) as x", List.of(f("y"), tf("x"))),
        of(
            "stats avg(numvalue) as y, sum(other) by bin(1hr) as x",
            List.of(f("y"), f("sum(other)"), tf("x"))),
        of("stats avg(numvalue) by other, yyy", List.of(f("avg(numvalue)"), f("other"), f("yyy"))),
        of(
            "stats avg(numvalue) by other, bin(5m)",
            List.of(f("avg(numvalue)"), f("other"), tf("bin(5m)"))),
        of(
            "stats avg(numvalue) by other, bin(5m) as tg",
            List.of(f("avg(numvalue)"), f("other"), tf("tg"))));
  }

  static Stream<Arguments> raiseExceptionArgs() {
    return Stream.of(
        of("display @message as abc", "Expected '|' but not found. Remaining query [...] 'as abc'"),
        of("fields abc |", "Invalid query. Expected next command after '|'"),
        of("fields abc | display", "Invalid query"),
        of("fields abc | unknown_cmd", "Invalid query"));
  }

  static DisplayField f(String v) {
    return new DisplayField(v, false);
  }

  static DisplayField tf(String v) {
    return new DisplayField(v, true);
  }
}

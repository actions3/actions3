/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common.store;

import static com.milosz.apps.cm.common.store.Bytes.CHARSET;
import static com.milosz.apps.cm.common.store.Bytes.array;
import static com.milosz.apps.cm.common.store.Bytes.str;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class ExtA3DbTest {
  private File file;
  private ExtA3Db underTest;

  @BeforeEach
  public void setup() throws IOException {
    this.file = File.createTempFile(UUID.randomUUID().toString().replaceAll("-", ""), "");
    this.underTest = new ExtA3Db(file);
  }

  @AfterEach
  public void clear() {
    if (underTest != null) {
      underTest.rollback();
      this.underTest.close();
    }
  }

  @Test
  public void shouldRestoreFromBackup() {
    System.out.println(file.getName());
    underTest.begin();
    underTest.put(Key.of("a", "b"), array("c"));
    underTest.commit();
    underTest.close();
    underTest = null;

    final var bf = new File(file.getParent(), ".bak2-" + file.getName());
    file.renameTo(bf);

    var bt = new ExtA3Db(file);
    bt.begin();
    System.out.println(file.getName() + " " + bf.getAbsolutePath() + " " + bf.length());
    bt.put(Key.of("c", "d"), array("xyz"));
    bt.commit();
    bt.close();

    bf.renameTo(new File(file.getParent(), ".bak-" + file.getName()));
    bt = new ExtA3Db(file);
    final var content = bt.get(Key.of("a", "b"));
    assertEquals(content.isPresent(), true);
    assertArrayEquals(content.get(), array("c"));
    assertEquals(bt.get(Key.of("c", "d")).isEmpty(), true);
    bt.close();
  }

  @Test
  public void shouldKeepOrder() throws NoSuchAlgorithmException {
    underTest.begin();
    List<String> sequence =
        SecureRandom.getInstanceStrong().longs(15).mapToObj(v -> String.format("%25d", v)).toList();
    for (var s : sequence) {
      underTest.put(Key.of("seq", s), array(String.valueOf(s)));
    }
    var all = underTest.list().stream().map(Node::value).toList();
    var expected = sequence.stream().sorted().toList();
    for (int i = 0; i < all.size(); i++) {
      System.out.println("%s = %s".formatted(all.get(i), expected.get(i)));
    }
    assertIterableEquals(expected, all);
    underTest.rollback();
  }

  @Test
  public void shouldAddFirstPage() {
    underTest.begin();
    underTest.put(Key.of("category", "value1"), "some content".getBytes(CHARSET));
    final var content = underTest.get(Key.of("category", "value1"));
    assertEquals(content.isPresent(), true, "Content not found");
    assertEquals(new String(content.get(), CHARSET), "some content");
    underTest.rollback();
  }

  @Test
  public void shouldAddFirstBigPage() {
    underTest.begin();
    final var longContent =
        IntStream.range(0, 2000)
            .mapToObj(
                n -> OffsetDateTime.now().plusDays(n).format(DateTimeFormatter.RFC_1123_DATE_TIME))
            .collect(Collectors.joining());
    final var total = array(longContent).length;
    final var expectedPages = Math.ceil((double) total / ExtA3Db.CS);
    underTest.put(Key.of("category", "value1"), array(longContent));
    final var content = underTest.get(Key.of("category", "value1"));
    assertEquals(content.isPresent(), true, "Content not found");
    assertEquals(longContent, str(content.get()));
    assertEquals(expectedPages, underTest.treeHeader().pageCount());
    underTest.rollback();
  }

  @Test
  public void shouldUpdateBigPage() {
    underTest.begin();
    final var longContent =
        IntStream.range(0, 2000)
            .mapToObj(
                n -> OffsetDateTime.now().plusDays(n).format(DateTimeFormatter.RFC_1123_DATE_TIME))
            .collect(Collectors.joining());
    final var total = array(longContent).length;
    final var expectedPages = Math.ceil((double) total / ExtA3Db.CS);
    underTest.put(Key.of("category", "value1"), "some content".getBytes(CHARSET));
    underTest.put(Key.of("category", "value1"), array(longContent));
    final var content = underTest.get(Key.of("category", "value1"));

    assertEquals(content.isPresent(), true, "Content not found");
    assertEquals(str(content.get()), longContent);
    assertEquals(expectedPages, underTest.treeHeader().pageCount());

    final var items = underTest.findAll(k -> k.pkStr().equals("category"));
    assertEquals(1, items.size());

    underTest.rollback();
  }

  @Test
  public void shouldUpdateBigPageAndFreeUnusuedContentPages() {
    underTest.begin();
    final var longContent =
        IntStream.range(0, 2000)
            .mapToObj(
                n -> OffsetDateTime.now().plusDays(n).format(DateTimeFormatter.RFC_1123_DATE_TIME))
            .collect(Collectors.joining());
    final var updateContent = longContent.substring(longContent.length() / 2);

    underTest.put(Key.of("category", "value1"), array(longContent));
    underTest.put(Key.of("category", "value1"), array(updateContent));
    final var content = underTest.get(Key.of("category", "value1"));

    assertEquals(content.isPresent(), true, "Content not found");
    assertEquals(str(content.get()), updateContent);

    final var total = array(updateContent).length;
    final var expectedPages = Math.ceil((double) total / ExtA3Db.CS);
    final var expectedFree =
        Math.ceil((double) array(longContent).length / ExtA3Db.CS) - expectedPages;
    assertEquals(expectedPages, underTest.treeHeader().pageCount());
    assertEquals(expectedFree, underTest.treeHeader().freeCount());

    final var items = underTest.findAll(k -> k.pkStr().equals("category"));
    assertEquals(1, items.size());

    underTest.rollback();
  }

  @Test
  public void shouldUpdateBigPageAndReuseFreePages() {
    underTest.begin();
    final var longContent =
        IntStream.range(0, 2000)
            .mapToObj(
                n -> OffsetDateTime.now().plusDays(n).format(DateTimeFormatter.RFC_1123_DATE_TIME))
            .collect(Collectors.joining());
    final var updateContent = longContent.substring(longContent.length() / 2);

    underTest.put(Key.of("category", "value1"), array("LONG" + longContent));
    underTest.put(Key.of("category", "value1"), array("UPDT" + updateContent));

    final var moreContent = updateContent.substring(updateContent.length() / 2);
    underTest.put(Key.of("category", "v2"), array(moreContent));
    final var content = underTest.get(Key.of("category", "v2"));
    //
    assertEquals(content.isPresent(), true, "Content not found");
    assertEquals(str(content.get()), moreContent);

    final var total = array(updateContent).length;
    final var moreLen = Math.ceil((double) array(moreContent).length / ExtA3Db.CS);
    final var expectedPages = Math.ceil((double) total / ExtA3Db.CS) + moreLen;
    final var expectedFree =
        Math.ceil((double) array(longContent).length / ExtA3Db.CS) - expectedPages;
    assertEquals(expectedPages, underTest.treeHeader().pageCount());
    assertEquals(expectedFree, underTest.treeHeader().freeCount());

    final var items = underTest.findAll(k -> k.pkStr().equals("category"));
    assertEquals(2, items.size());

    underTest.rollback();
  }

  @Test
  public void shouldUpdateBigPageAndReuseFreePagesAndUseNew() {
    underTest.begin();
    final var longContent =
        IntStream.range(0, 2000)
            .mapToObj(
                n -> OffsetDateTime.now().plusDays(n).format(DateTimeFormatter.RFC_1123_DATE_TIME))
            .collect(Collectors.joining());
    final var updateContent = longContent.substring(longContent.length() / 2);

    underTest.put(Key.of("category", "value1"), array("LONG" + longContent));
    underTest.put(Key.of("category", "value1"), array("UPDT" + updateContent));

    final var moreContent = updateContent.substring(updateContent.length() / 2);
    underTest.put(Key.of("category", "v2"), array(moreContent));
    underTest.put(Key.of("category", "v3"), array(longContent));
    final var content = underTest.get(Key.of("category", "v3"));
    assertEquals(content.isPresent(), true, "Content not found");
    assertEquals(str(content.get()), longContent);

    final var updtLen = Math.ceil((double) array(updateContent).length / ExtA3Db.CS);
    final var moreLen = Math.ceil((double) array(moreContent).length / ExtA3Db.CS);
    final var longLen = Math.ceil((double) array(longContent).length / ExtA3Db.CS);
    final var expectedPages = updtLen + moreLen + longLen;
    assertEquals(expectedPages, underTest.treeHeader().pageCount());
    assertEquals(0, underTest.treeHeader().freeCount());

    final var items = underTest.findAll(k -> k.pkStr().equals("category"));
    assertEquals(3, items.size());

    underTest.rollback();
  }

  @Test
  public void shouldUpdatePage() {
    underTest.begin();
    underTest.put(Key.of("category", "value1"), "some content".getBytes(CHARSET));
    underTest.put(Key.of("category", "value1"), array("updated content"));
    final var content = underTest.get(Key.of("category", "value1"));

    assertEquals(content.isPresent(), true, "Content not found");
    assertEquals(str(content.get()), "updated content");

    final var items = underTest.findAll(k -> k.pkStr().equals("category"));
    assertEquals(1, items.size());

    underTest.rollback();
  }

  @Test
  public void shouldCommit() {
    underTest.begin();
    underTest.put(Key.of("category", "value1"), "some content".getBytes(CHARSET));
    final var content = underTest.get(Key.of("category", "value1"));
    assertEquals(content.isPresent(), true, "Content not found");
    assertEquals(new String(content.get(), CHARSET), "some content");
    underTest.commit();
    underTest.begin();
    final var commited = underTest.get(Key.of("category", "value1"));
    assertEquals(commited.isPresent(), true, "Content not found");
    assertEquals(new String(commited.get(), CHARSET), "some content");
    underTest.commit();
  }

  @Test
  public void shouldRollback() {
    underTest.begin();
    underTest.put(Key.of("category", "value1"), "some content".getBytes(CHARSET));
    final var content = underTest.get(Key.of("category", "value1"));
    assertEquals(content.isPresent(), true, "Content not found");
    assertEquals(new String(content.get(), CHARSET), "some content");
    underTest.commit();
    underTest.begin();
    underTest.put(Key.of("category", "value1"), "some content to rollback".getBytes(CHARSET));
    final var rbc = underTest.get(Key.of("category", "value1"));
    assertEquals(rbc.isPresent(), true, "Content not found");
    assertEquals(new String(rbc.get(), CHARSET), "some content to rollback");
    underTest.rollback();
    underTest.begin();
    final var commited = underTest.get(Key.of("category", "value1"));
    assertEquals(commited.isPresent(), true, "Content not found");
    assertEquals(new String(commited.get(), CHARSET), "some content");
    underTest.commit();
  }

  @Test
  public void shouldReadWithoutTransaction() {
    underTest.begin();
    underTest.put(Key.of("category", "value1"), "some content".getBytes(CHARSET));
    final var content = underTest.get(Key.of("category", "value1"));
    assertEquals(content.isPresent(), true, "Content not found");
    assertEquals(new String(content.get(), CHARSET), "some content");
    underTest.commit();
    final var commited = underTest.get(Key.of("category", "value1"));
    assertEquals(commited.isPresent(), true, "Content not found");
    assertEquals(new String(commited.get(), CHARSET), "some content");
  }

  @Test
  public void shouldRejectWriteWhenVersionIsOutdated() {
    underTest.begin();
    underTest.put(Key.of("a", "b"), array("c"));

    final var bt2 = new ExtA3Db(file);
    bt2.begin();
    bt2.put(Key.of("c", "d"), array("e"));
    bt2.commit();
    // other transaction for the test file commits changes when first transaction is
    // still active
    bt2.close();
    assertThrows(
        StorageException.class,
        () -> {
          underTest.put(Key.of("c", "d"), array("E"));
          underTest.commit();
        });
    underTest.rollback();
  }

  @Test
  public void shouldAllowWriteWhenVersionIsUpToDate() {
    underTest.begin();
    underTest.put(Key.of("a", "b"), array("c"));
    final var bt2 = new ExtA3Db(file);
    bt2.begin();
    bt2.put(Key.of("c", "d"), array("e"));
    bt2.commit();
    bt2.close();
    underTest.list();
    underTest.rollback();
    underTest.begin();
    underTest.put(Key.of("c", "d"), array("E"));
    underTest.commit();
    assertArrayEquals(array("E"), underTest.get(Key.of("c", "d")).get());
  }

  @Test
  public void shouldAddPageWithLesserKey() {
    underTest.begin();
    underTest.put(Key.of("K", "value1"), "some content".getBytes(CHARSET));
    underTest.put(Key.of("D", "left"), array("some", 4));

    final var content = underTest.get(Key.of("D", "left"));
    assertEquals(content.isPresent(), true, "Content not found");
    assertEquals("some", str(content.get()));
    underTest.commit();
  }

  @Test
  public void shouldAddPageWithGreaterKey() {
    underTest.begin();
    underTest.put(Key.of("K", "value1"), "some content".getBytes(CHARSET));
    underTest.put(Key.of("U", "left"), array("some", 4));

    final var content = underTest.get(Key.of("U", "left"));
    assertEquals(content.isPresent(), true, "Content not found");
    assertEquals("some", str(content.get()));
    underTest.commit();
  }

  @Test
  public void shouldAddBigPageWithGreaterKey() {
    underTest.begin();
    final var longContent =
        IntStream.range(0, 2000)
            .mapToObj(
                n -> OffsetDateTime.now().plusDays(n).format(DateTimeFormatter.RFC_1123_DATE_TIME))
            .collect(Collectors.joining());
    underTest.put(Key.of("K", "value1"), "some content".getBytes(CHARSET));
    underTest.put(Key.of("U", "left"), array(longContent));

    final var content = underTest.get(Key.of("U", "left"));
    assertEquals(content.isPresent(), true, "Content not found");
    assertEquals(longContent, str(content.get()));
    underTest.commit();
  }

  @Test
  public void shouldRemoveLeaf() {
    underTest.begin();
    underTest.put(Key.of("K", "value1"), "some content".getBytes(CHARSET));
    underTest.put(Key.of("U", "left"), array("some"));
    underTest.put(Key.of("D", "right"), array("right-node"));

    final var r = underTest.remove(Key.of("U", "left"));
    assertEquals(true, r);
    underTest.commit();

    final var cont = underTest.get(Key.of("U", "left"));
    assertEquals(true, cont.isEmpty());
  }

  @Test
  public void shouldRemoveBigInternal() {
    underTest.begin();
    final var longContent =
        IntStream.range(0, 200)
            .mapToObj(
                n -> OffsetDateTime.now().plusDays(n).format(DateTimeFormatter.RFC_1123_DATE_TIME))
            .collect(Collectors.joining());
    final var keys =
        new ArrayList<>(List.of("0020", "0011", "0078", "0013", "0015", "0054", "0006", "0079"));
    final var ks = keys.size();
    for (final var k : keys) {
      underTest.put(Key.of("n", k), array(longContent + "-" + k));
    }
    while (!keys.isEmpty()) {
      final var dbk = underTest.preorder();
      final var r = underTest.remove(dbk.get(0));
      assertTrue(r, "Item not removed");
      keys.remove(dbk.get(0).skStr());

      final var expected = keys.stream().sorted().toList();
      assertIterableEquals(expected, underTest.list().stream().map(n -> n.key().skStr()).toList());
    }
    final var total = array(longContent).length;
    final var expectedPages = Math.ceil((double) total / ExtA3Db.CS);

    assertEquals(expectedPages * ks, underTest.treeHeader().freeCount());
    assertEquals(0, underTest.treeHeader().pageCount());
    underTest.commit();
  }

  @Test
  public void shouldRemoveListRoot() {
    underTest.begin();
    for (var i = 0; i < 4; i++) {
      underTest.put(Key.of("n", "0" + i), array("value"));
    }

    final var r = underTest.remove(Key.of("n", "00"));
    assertTrue(r, "Not removed");

    assertIterableEquals(
        List.of("01", "02", "03"), underTest.list().stream().map(n -> n.key().skStr()).toList());
    underTest.commit();
  }

  @Test
  public void shouldReleaseFreePages() {
    underTest.begin();
    for (var i = 0; i < 4; i++) {
      underTest.put(Key.of("n", "0" + i), array("value"));
    }

    for (var i = 0; i < 4; i++) {
      underTest.remove(Key.of("n", "0" + i));
    }
    var tree = underTest.treeHeader();
    assertEquals(0, tree.pageCount());
    assertEquals(4, tree.freeCount());
    underTest.commit();

    // after commit all free pages from should be removed
    // because they are localized at the end of file
    underTest.begin();
    tree = underTest.treeHeader();
    assertEquals(0, tree.pageCount());
    assertEquals(0, tree.freeCount());
    assertEquals(100, file.length()); // only file header
    underTest.rollback();
  }

  @Test
  public void shouldReleaseFreePagesAtTheEndOfFile() {
    underTest.begin();
    for (var i = 0; i < 6; i++) {
      underTest.put(Key.of("n", "0" + i), array("value"));
    }

    underTest.remove(Key.of("n", "02"));
    underTest.remove(Key.of("n", "01"));
    underTest.remove(Key.of("n", "04"));
    underTest.remove(Key.of("n", "05")); // last page in file

    var tree = underTest.treeHeader();
    assertEquals(2, tree.pageCount());
    assertEquals(4, tree.freeCount());
    underTest.commit();

    underTest.begin();
    tree = underTest.treeHeader();
    assertEquals(2, tree.pageCount());
    assertEquals(2, tree.freeCount()); // page for key (n,05) and (n,06) are removed before commit
    assertEquals(100 + 2048 * 4, file.length()); // header + 2 free pages + 2 used pages
    underTest.rollback();
  }

  @Test
  public void shouldNotReleaseInternalFreePage() {
    underTest.begin();
    for (var i = 0; i < 6; i++) {
      underTest.put(Key.of("n", "0" + i), array("value"));
    }

    underTest.remove(Key.of("n", "01"));
    underTest.remove(Key.of("n", "04"));

    var tree = underTest.treeHeader();
    assertEquals(4, tree.pageCount());
    assertEquals(2, tree.freeCount());
    underTest.commit();

    underTest.begin();
    tree = underTest.treeHeader();
    assertEquals(4, tree.pageCount());
    assertEquals(2, tree.freeCount());
    underTest.rollback();
  }

  @Test
  public void shouldReuseFreePage() {
    underTest.begin();
    final var keys =
        new ArrayList<>(List.of("0020", "0011", "0078", "0013", "0015", "0054", "0006", "0079"));
    final var expected = keys.stream().sorted().toList();
    for (final var k : keys) {
      underTest.put(Key.of("n", k), array(k));
    }
    underTest.list();
    System.out.println("=============================");
    for (var i = 0; i < 2; i++) {
      System.out.println("vvvvvvvvvvvvvvvvvv iteration start vvvvvvvvvvvvvvvvvv");
      System.out.println("------------- tree before removal ---------");
      underTest.list();
      System.out.println("------------------ tree end ----------");

      final var po = underTest.preorder();
      final var k0 = po.get(0);
      final var k1 = po.get(1);
      assertTrue(underTest.remove(k0), "k0 not removed");
      assertTrue(underTest.remove(k1), "k1 not removed");

      System.out.println("------------- tree after removal ---------");
      underTest.list();
      System.out.println("------------------ tree end ----------");
      System.out.println("========PUT k1==========");
      underTest.put(k1, array(k1.skStr()));

      System.out.println("========PUT k0==========");
      underTest.put(k0, array(k0.skStr()));
      assertIterableEquals(expected, underTest.list().stream().map(n -> n.key().skStr()).toList());
      System.out.println("^^^^^^^^^^^^^^^^^^^^^ iteration end ^^^^^");
    }
    final var th = underTest.treeHeader();
    assertEquals(th.pageCount(), keys.size());
    assertEquals(th.freeCount(), 0);
    underTest.commit();
  }

  @Test
  public void shouldReuseFreePage2() {
    underTest.begin();
    final var keys =
        new ArrayList<>(
            List.of("0020", "0011", "0017", "0078", "0013", "0015", "0054", "0006", "0079"));
    final var expected = keys.stream().sorted().toList();
    for (final var k : keys) {
      underTest.put(Key.of("n", k), array(k));
    }
    underTest.list();
    System.out.println("=============================");
    for (var i = 0; i < 21; i++) {
      System.out.println("vvvvvvvvvvvvvvvvvv iteration start vvvvvvvvvvvvvvvvvv");
      System.out.println("------------- tree before removal ---------");
      underTest.preorder();
      System.out.println("------------------ tree end ----------");

      final var po = underTest.preorder();
      final var k0 = po.get(0);
      final var k1 = po.get(1);
      assertTrue(underTest.remove(k0), "k0 not removed");
      System.out.println("------------- after first removal");
      underTest.preorder();
      assertTrue(underTest.remove(k1), "k1 not removed");

      System.out.println("------------- tree after removal ---------");
      underTest.preorder();
      System.out.println("------------------ tree end ----------");
      System.out.println("========PUT k1==========");
      underTest.put(k1, array(k1.skStr()));

      System.out.println("========PUT k0==========");
      underTest.put(k0, array(k0.skStr()));
      assertIterableEquals(expected, underTest.list().stream().map(n -> n.key().skStr()).toList());
      System.out.println("^^^^^^^^^^^^^^^^^^^^^ iteration end ^^^^^");
    }
    final var th = underTest.treeHeader();
    assertEquals(th.pageCount(), keys.size());
    assertEquals(th.freeCount(), 0);
    underTest.commit();
  }

  @Test
  public void shouldReuseFreePage3() {
    underTest.begin();
    final var keys = new ArrayList<>(List.of("0020", "0011"));
    final var expected = keys.stream().sorted().toList();
    for (final var k : keys) {
      underTest.put(Key.of("n", k), array(k));
    }
    underTest.list();
    System.out.println("=============================");
    for (var i = 0; i < 1; i++) {
      System.out.println("vvvvvvvvvvvvvvvvvv iteration start vvvvvvvvvvvvvvvvvv");
      System.out.println("------------- tree before removal ---------");
      underTest.preorder();
      System.out.println("------------------ tree end ----------");

      final var po = underTest.preorder();
      final var k0 = po.get(0);
      final var k1 = po.get(1);
      assertTrue(underTest.remove(k0), "k0 not removed");
      System.out.println("------------- after first removal");
      underTest.preorder();
      assertTrue(underTest.remove(k1), "k1 not removed");

      System.out.println("------------- tree after removal ---------");
      underTest.preorder();
      System.out.println("------------------ tree end ----------");
      System.out.println("========PUT k1==========");
      underTest.put(k1, array(k1.skStr()));

      System.out.println("========PUT k0==========");
      underTest.put(k0, array(k0.skStr()));
      assertIterableEquals(expected, underTest.list().stream().map(n -> n.key().skStr()).toList());
      System.out.println("^^^^^^^^^^^^^^^^^^^^^ iteration end ^^^^^");
    }
    final var th = underTest.treeHeader();
    assertEquals(th.pageCount(), keys.size());
    assertEquals(th.freeCount(), 0);
    underTest.commit();
  }

  @Test
  public void shouldReturnSortedNodeList() {
    underTest.begin();
    final var keys =
        IntStream.range(0, 200)
            .mapToObj(i -> UUID.randomUUID().toString())
            .sorted()
            .collect(Collectors.toCollection(ArrayList::new));
    final var expectedKeys = new ArrayList<>(keys);
    final var rand = new Random(System.currentTimeMillis());
    while (!keys.isEmpty()) {
      final var i = rand.nextInt(keys.size());
      final var pk = keys.remove(i);
      underTest.put(Key.of("PK", pk), array(pk));
    }
    final var all = underTest.list().stream().map(n -> n.key().skStr()).toList();
    assertIterableEquals(expectedKeys, all, "List not match");
    underTest.commit();
  }

  @Test
  public void shouldFindAll() {
    underTest.begin();
    final var keys =
        IntStream.range(0, 200)
            .mapToObj(i -> UUID.randomUUID().toString())
            .collect(Collectors.toCollection(ArrayList::new));
    final var expectedKeys = new ArrayList<>(keys);
    expectedKeys.sort(String::compareTo);
    final var rand = new Random(System.currentTimeMillis());
    while (!keys.isEmpty()) {
      final var i = rand.nextInt(keys.size());
      final var pk = keys.remove(i);
      underTest.put(Key.of("PK", pk), array(pk));
    }
    final var all =
        underTest.findAll(k -> k.pkStr().equals("PK")).stream().map(n -> n.value()).toList();
    assertIterableEquals(expectedKeys, all, "List not match");
    underTest.commit();
  }

  @ParameterizedTest
  @ValueSource(ints = {1, 10, 100, 120, 199})
  public void shouldFindAllWithLimit(int limit) {
    underTest.begin();
    final var keys =
        IntStream.range(0, 200)
            .mapToObj(i -> UUID.randomUUID().toString())
            .collect(Collectors.toCollection(ArrayList::new));
    final var expectedKeys = new ArrayList<>(keys);
    expectedKeys.sort(String::compareTo);
    final var rand = new Random(System.currentTimeMillis());
    while (!keys.isEmpty()) {
      final var i = rand.nextInt(keys.size());
      final var pk = keys.remove(i);
      underTest.put(Key.of("PK", pk), array(pk));
    }
    final var all =
        underTest.findAll(k -> k.pkStr().equals("PK"), limit).stream().map(n -> n.value()).toList();
    assertIterableEquals(expectedKeys.subList(0, limit), all, "List not match");
    underTest.commit();
  }
}

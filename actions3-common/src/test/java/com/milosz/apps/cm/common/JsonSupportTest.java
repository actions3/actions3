/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Objects;
import org.junit.jupiter.api.Test;

public class JsonSupportTest {

  private JsonSupport underTest = JsonSupport.INSTANCE;

  @Test
  public void shouldIgnoreNullProperty() {

    var te = new TestEntity();

    var out = underTest.write(te, false);
    assertEquals("{}", out);
  }

  @Test
  public void shouldIncludeNotNullProperty() {

    var te = new TestEntity();
    te.setV(100);
    var out = underTest.write(te, false);
    assertEquals("{\"v\":100}", out);
  }

  @Test
  public void shoudlIgnoreUnkonwProperty() {
    var ts = "{\"unkonwn_property\":100}";
    var te = underTest.read(ts, TestEntity.class);
    assertEquals(new TestEntity(), te);
  }

  @Test
  public void shoudlReadProperty() {
    var ts = "{\"v\":100}";
    var expected = new TestEntity();
    expected.setV(100);
    var te = underTest.read(ts, TestEntity.class);
    assertEquals(expected, te);
  }

  public static class TestEntity {
    private Integer v;

    public Integer getV() {
      return v;
    }

    public void setV(Integer v) {
      this.v = v;
    }

    @Override
    public int hashCode() {
      return Objects.hash(v);
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) return true;
      if (obj == null) return false;
      if (getClass() != obj.getClass()) return false;
      TestEntity other = (TestEntity) obj;
      return Objects.equals(v, other.v);
    }
  }
}

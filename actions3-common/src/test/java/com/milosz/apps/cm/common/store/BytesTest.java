/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common.store;

import static com.milosz.apps.cm.common.store.Bytes.array;
import static com.milosz.apps.cm.common.store.Bytes.transfer;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import org.junit.jupiter.api.Test;

public class BytesTest {
  @Test
  public void shouldTransferData() throws Exception {
    final var src = File.createTempFile("bytes-test-src", "");
    final var target = File.createTempFile("bytes-test-target", "");

    try (var as = new RandomAccessFile(src, "rwd");
        var cs = as.getChannel();
        var at = new RandomAccessFile(target, "rwd");
        var ct = at.getChannel()) {
      write(cs, "source data to write", 10);
      write(ct, "target data", 5);
      transfer(cs, ct);
    }
    assertEquals("source data to write".getBytes().length * 10, target.length());
    assertEquals(src.length(), target.length());
  }

  @Test
  public void shouldTransferDataAndTruncateTarget() throws Exception {
    final var src = File.createTempFile("bytes-test-src", "");
    final var target = File.createTempFile("bytes-test-target", "");

    try (var as = new RandomAccessFile(src, "rwd");
        var cs = as.getChannel();
        var at = new RandomAccessFile(target, "rwd");
        var ct = at.getChannel()) {
      write(cs, "source data", 10);
      write(ct, "target data to replace", 15);
      transfer(cs, ct);
    }
    assertEquals("source data".getBytes().length * 10, target.length());
    assertEquals(src.length(), target.length());
  }

  static void write(FileChannel f, String content, int repeat) throws IOException {
    for (var r = 0; r < repeat; r++) {
      f.write(ByteBuffer.wrap(array(content)));
    }
    f.position(0);
  }
}

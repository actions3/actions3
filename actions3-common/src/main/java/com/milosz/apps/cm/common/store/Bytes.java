/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common.store;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Arrays;

public final class Bytes {
  static final Charset CHARSET = Charset.forName("UTF-8");
  static final int UNDEFINED = Integer.MIN_VALUE;

  static int get(ByteBuffer buf, int index) {
    return get(buf, index, 0);
  }

  static ByteBuffer put(ByteBuffer buf, int index, int value) {
    return put(buf, index, 0, value);
  }

  static int get(ByteBuffer buf, int index, int offset) {
    return buf.getInt(offset + index * 4);
  }

  static ByteBuffer put(ByteBuffer buf, int index, int offset, int value) {
    return buf.putInt(offset + index * 4, value);
  }

  static ByteBuffer buffer(int size) {
    return ByteBuffer.wrap(new byte[size]);
  }

  static byte[] array(ByteBuffer buf, int start, int len) {
    final var a = new byte[len];
    buf.get(start, a);
    return a;
  }

  public static byte[] array(String s, int len) {
    final var buf = buffer(len);
    buf.put(0, s.getBytes(CHARSET));
    return buf.array();
  }

  public static byte[] array(long v) {
    final var buf = buffer(8);
    buf.putLong(0, v);
    return buf.array();
  }

  public static byte[] array(String s) {
    return array(s, s.getBytes(CHARSET).length);
  }

  public static byte[] sha512(String s) {
    try {
      final var md = MessageDigest.getInstance("SHA-512");
      return md.digest(s.getBytes(CHARSET));
    } catch (final Exception e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  public static String str(byte[] a) {
    return new String(a, CHARSET);
  }

  static void print(byte[] buf) {
    System.out.println(Arrays.toString(buf));
  }

  static void print(ByteBuffer buf) {
    print(buf.array());
  }

  static void transfer(FileChannel source, FileChannel target) {
    try {
      target.transferFrom(source, 0, Long.MAX_VALUE);
      target.truncate(source.size());
    } catch (final IOException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  private Bytes() {}
}

/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common.store;

import static com.milosz.apps.cm.common.store.Bytes.UNDEFINED;
import static com.milosz.apps.cm.common.store.Bytes.array;
import static com.milosz.apps.cm.common.store.Bytes.buffer;
import static com.milosz.apps.cm.common.store.Bytes.get;
import static com.milosz.apps.cm.common.store.Bytes.put;
import static com.milosz.apps.cm.common.store.Key.LEN_PK;
import static com.milosz.apps.cm.common.store.Key.LEN_SK;

import java.nio.ByteBuffer;

record PageHeader(int number, int left, int right, int length, int next, Key key) {

  static final int LENGTH = 200;

  public PageHeader(int number, int left, int right, int length, Key key) {
    this(number, left, right, length, UNDEFINED, key);
  }

  public static PageHeader from(ByteBuffer buf) {
    final var key = array(buf, 20, LEN_PK + LEN_SK);
    return new PageHeader(
        get(buf, 0), get(buf, 1), get(buf, 2), get(buf, 3), get(buf, 4), new Key(key));
  }

  public ByteBuffer bytes() {
    final var buf = buffer(LENGTH);
    put(buf, 0, number);
    put(buf, 1, left);
    put(buf, 2, right);
    put(buf, 3, length);
    put(buf, 4, next);
    buf.put(20, key.pk());
    buf.put(20 + LEN_PK, key.sk());

    return buf;
  }

  public boolean isLeaf() {
    return left == UNDEFINED && right == UNDEFINED;
  }

  public PageHeader withLeft(int v) {
    if (number != UNDEFINED && v == number) {
      throw new IllegalArgumentException("Left Cycle found " + this);
    }
    if (v != UNDEFINED && v == right) {
      throw new IllegalArgumentException("Left == right " + this);
    }
    return new PageHeader(number, v, this.right, this.length, next, this.key);
  }

  public PageHeader withRight(int v) {
    if (number != UNDEFINED && v == number) {
      throw new IllegalArgumentException("Right Cycle found " + this);
    }
    if (v != UNDEFINED && v == left) {
      throw new IllegalArgumentException("Left == right " + this);
    }
    return new PageHeader(number, left, v, this.length, next, this.key);
  }

  public PageHeader withChild(Key ck, int v) {
    if (ck.compareTo(key) < 0) {
      return withLeft(v);
    }
    return withRight(v);
  }

  public PageHeader withKey(Key k) {
    return new PageHeader(number, left, right, length, next, k);
  }

  public PageHeader withLength(int len) {
    return new PageHeader(number, left, right, len, next, key);
  }

  public PageHeader withNext(int v) {
    assert v != number : "Detected cycle";
    return new PageHeader(number, left, right, length, v, key);
  }

  public boolean hasNext() {
    return next != UNDEFINED;
  }

  public int next(Key other) {
    if (other.compareTo(key) < 0) {
      return left;
    }
    return right;
  }
}

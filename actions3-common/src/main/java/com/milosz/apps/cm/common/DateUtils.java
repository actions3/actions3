/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public final class DateUtils {

  public static final ZoneId TZ = ZoneId.systemDefault();

  public static final ZonedDateTime MIN = of(Instant.EPOCH);

  public static final ZonedDateTime MAX = MIN.plusYears(1000);

  /** Date time format for input fields */
  public static final String DATE_TIME_FMT = "yyyy-MM-dd HH:mm:ss";

  public static final DateTimeFormatter DTF = DateTimeFormatter.ofPattern(DATE_TIME_FMT);

  private DateUtils() {}

  public static ZonedDateTime of(Instant instant) {
    return ZonedDateTime.ofInstant(instant, TZ);
  }

  public static String format(ZonedDateTime dt) {
    return DTF.format(dt);
  }

  public static ZonedDateTime of(Number num) {
    return of(Instant.ofEpochMilli(num.longValue()));
  }

  public static ZonedDateTime of(Date dt) {
    return of(dt.toInstant());
  }

  public static ZonedDateTime of(LocalDateTime dt) {
    return dt.atZone(TZ);
  }

  public static ZonedDateTime parseLocal(String txt, DateTimeFormatter fmt) {
    return of(LocalDateTime.parse(txt, fmt));
  }

  public static ZonedDateTime parseLocalInput(String txt) {
    return parseLocal(txt, DTF);
  }

  public static Date toDate(ZonedDateTime dt) {
    return Date.from(dt.toInstant());
  }

  public static long toMiliseconds(ZonedDateTime dt) {
    return dt.toInstant().toEpochMilli();
  }
}

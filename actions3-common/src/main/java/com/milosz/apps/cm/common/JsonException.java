/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common;

import com.fasterxml.jackson.core.JacksonException;
import com.milosz.apps.cm.spi.AppException;

public class JsonException extends AppException {

  private static final long serialVersionUID = -829097831171531314L;

  public JsonException(String message, Throwable cause) {
    super(message, cause);
  }

  public String getOriginalMessage() {
    if (getCause() instanceof final JacksonException pe) {
      return pe.getOriginalMessage();
    }
    return getMessage();
  }
}

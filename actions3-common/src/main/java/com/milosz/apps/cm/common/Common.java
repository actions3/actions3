/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Common {
  private static Path getConfigPath() {
    final var os = System.getProperty("os.name").toLowerCase();
    if (os.contains("win")) {
      return Paths.get("Appdata", "Local", "actions3");
    } else if (os.contains("nux")) {
      return Paths.get(".config", "actions3");
    } else {
      return Paths.get(".actions3");
    }
  }

  public static Path getConfigDir() {
    final var base = new File(System.getProperty("user.home"));
    final var dir = base.toPath().resolve(getConfigPath());
    final var fdir = dir.toFile();
    if (!fdir.exists()) {
      fdir.mkdir();
    }
    return dir;
  }

  public static Path getConfigFile() {
    return getConfigDir().resolve("settings.db");
  }

  public static Path getLogFile() {
    final var dir = getConfigDir().resolve("logs");
    final var fdir = dir.toFile();
    if (!fdir.exists()) {
      fdir.mkdir();
    }
    return dir.resolve("app.%g.log");
  }
}

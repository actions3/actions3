/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common.store;

import static com.milosz.apps.cm.common.store.Bytes.CHARSET;
import static com.milosz.apps.cm.common.store.Bytes.buffer;
import static com.milosz.apps.cm.common.store.Bytes.get;
import static com.milosz.apps.cm.common.store.Bytes.put;
import static java.util.Arrays.compare;

import java.nio.ByteBuffer;

record TreeHeader(int rootPage, int firstFree, int pageCount, int freeCount) {
  private static final byte[] VERSION = "Tree Format 1".getBytes(CHARSET);
  private static final int VL = VERSION.length;

  public static TreeHeader from(ByteBuffer buf) {
    final var version = new byte[VL];
    buf.get(0, version);
    if (compare(version, VERSION) != 0) {
      throw new IllegalStateException(
          "Unsupported format %s".formatted(new String(version, CHARSET)));
    }
    return new TreeHeader(get(buf, 0, VL), get(buf, 1, VL), get(buf, 2, VL), get(buf, 3, VL));
  }

  public int totalPageCount() {
    return freeCount + pageCount;
  }

  public TreeHeader withRootPage(int p) {
    return new TreeHeader(p, firstFree, pageCount, freeCount);
  }

  public TreeHeader withFirstFree(int p) {
    return new TreeHeader(rootPage, p, pageCount, freeCount);
  }

  public TreeHeader withFreeCount(int v) {
    return new TreeHeader(rootPage, firstFree, pageCount, v);
  }

  public TreeHeader incFree() {
    return new TreeHeader(rootPage, firstFree, pageCount, freeCount + 1);
  }

  public TreeHeader incPage() {
    return new TreeHeader(rootPage, firstFree, pageCount + 1, freeCount);
  }

  public TreeHeader incPageCount(int c) {
    assert pageCount + c >= 0 : "Negative new page count";
    return new TreeHeader(rootPage, firstFree, pageCount + c, freeCount);
  }

  public TreeHeader incFreeCount(int c) {
    assert freeCount + c >= 0 : "Negative new free count";
    return new TreeHeader(rootPage, firstFree, pageCount, freeCount + c);
  }

  public TreeHeader decFree() {
    return new TreeHeader(rootPage, firstFree, pageCount, freeCount - 1);
  }

  public TreeHeader decPage() {
    return new TreeHeader(rootPage, firstFree, pageCount - 1, freeCount);
  }

  public ByteBuffer bytes() {
    final var buf = buffer(VL + 4 * 4);
    buf.put(0, VERSION);
    put(buf, 0, VL, rootPage);
    put(buf, 1, VL, firstFree);
    put(buf, 2, VL, pageCount);
    put(buf, 3, VL, freeCount);
    return buf;
  }
}

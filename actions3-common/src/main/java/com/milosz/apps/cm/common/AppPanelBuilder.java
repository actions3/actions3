/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common;

import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import net.miginfocom.layout.AC;
import net.miginfocom.layout.LC;
import net.miginfocom.swing.MigLayout;

public class AppPanelBuilder {

  private final List<JComponent> components = new ArrayList<>();
  private final AC rowSpecs = new AC();

  public JFormattedTextField line(String label) {
    final var c = common(new JFormattedTextField(), label);
    rowSpecs.shrink(100, components.size());
    components.add(c);
    return c;
  }

  public JComboBox<String> list(String label, List<String> options) {
    final JComboBox<String> c = common(new JComboBox<>(options.toArray(new String[0])), label);
    rowSpecs.shrink(100, components.size());
    components.add(c);
    return c;
  }

  public JTextArea text(String label) {
    final var txt = new JTextArea(30, 50);
    txt.setName(label.replaceAll(" ", "_").toUpperCase());
    final var c = common(new JScrollPane(txt), label);
    rowSpecs.grow(100, components.size());
    components.add(c);
    return txt;
  }

  public JComponent component(JComponent c) {
    rowSpecs.shrink(100, components.size());
    components.add(c);
    return c;
  }

  private <T extends JComponent> T common(T c, String label) {
    c.setName(label.replaceAll(" ", "_").toUpperCase());
    c.setBorder(BorderFactory.createTitledBorder(label));
    return c;
  }

  public JPanel build() {
    final var panel = new JPanel();
    panel.setLayout(new MigLayout(new LC().wrapAfter(1), new AC().grow().size("300"), rowSpecs));
    components.forEach(c -> panel.add(c, "grow"));
    return panel;
  }
}

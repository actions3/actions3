/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common;

import static com.milosz.apps.cm.common.DateUtils.format;
import static javax.swing.JOptionPane.showMessageDialog;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.JTextComponent;
import javax.swing.text.MaskFormatter;

public class QueryParamsFormDialog extends JDialog implements ActionListener {

  private static final long serialVersionUID = -2625107906744745753L;
  private final Map<String, String> values = new HashMap<>();
  private final List<Param> params;
  private JPanel formPanel;
  private final List<JTextComponent> inputComponents = new ArrayList<>();
  private AtomicBoolean success = new AtomicBoolean();

  public QueryParamsFormDialog(Frame owner, List<Param> params) {
    super(owner);
    final var panel = contentPane2(params);
    this.params = params;
    panel.setOpaque(true);
    setContentPane(panel);
    setResizable(true);
    setLocationRelativeTo(owner);
    addWindowListener(
        new WindowAdapter() {
          @Override
          public void windowClosing(WindowEvent e) {
            if (!success.get()) {
              values.clear();
            }
          }
        });
    pack();
  }

  private JPanel contentPane2(List<Param> params) {
    final var builder = new AppPanelBuilder();
    JComponent fromEvent = null;
    for (final var p : params) {
      final var txt = p.isLineInput() ? builder.line(p.name()) : builder.text(p.name);
      if (p.defaultValue() != null) {
        txt.setText(p.defaultValue().toString());
      }
      installFormatter(txt, p);
      inputComponents.add(txt);
      FnAction.esc(txt, this::actionPerformed);
      if (fromEvent != null) {
        FnAction.enterNext(
            fromEvent,
            e -> {
              final var vt = (JComponent) e.getSource();
              getText(vt).ifPresent(v -> values.put(vt.getName(), v));
              txt.requestFocusInWindow();
              if (txt instanceof JTextComponent tp) {
                tp.selectAll();
              }
            });
      }
      fromEvent = txt;
    }
    formPanel = builder.build();
    final var panel = new JPanel();

    panel.setLayout(new BorderLayout());

    panel.add(formPanel, BorderLayout.CENTER);

    // on last event is installed handler that will
    // close window on ESC or ENTER
    FnAction.enter(fromEvent, this::actionPerformed);

    final var box = Box.createHorizontalBox();
    final var ok = new JButton("OK");
    ok.addActionListener(this);
    ok.setActionCommand(FnAction.ENTER_CMD);
    final var cancel = new JButton("Cancel");
    cancel.setActionCommand(FnAction.ESC_CMD);
    cancel.addActionListener(this);

    box.add(ok);
    box.add(cancel);

    panel.add(box, BorderLayout.SOUTH);
    return panel;
  }

  private void installFormatter(JTextComponent t, Param param) {
    if (param.type().equals("date") && t instanceof final JFormattedTextField ft) {
      getMaskFormatter().ifPresent(ft::setFormatterFactory);
      ft.setValue(param.defaultValue());
    }
  }

  private static Optional<DefaultFormatterFactory> getMaskFormatter() {
    try {
      final var df = new MaskFormatter("####-##-## ##:##:##");
      df.setAllowsInvalid(false);
      df.setOverwriteMode(true);
      return Optional.of(new DefaultFormatterFactory(df));
    } catch (final ParseException e) {
      return Optional.empty();
    }
  }

  private Optional<String> getText(JComponent src) {
    if (src instanceof final JTextComponent t) {
      var tt = t.getText();
      if (tt == null || tt.isBlank()) {
        return Optional.empty();
      }
      return Optional.of(tt);
    }
    return Optional.empty();
  }

  public List<? extends InputValue<?>> values() {
    return params.stream()
        .filter(p -> values.containsKey(p.id()))
        .map(p -> new TextValue(p.id(), values.get(p.id()), p.name()))
        .toList();
  }

  /**
   * @param params
   * @return empty list if user cancel
   */
  public static List<? extends InputValue<?>> showParamsDialog(List<Param> params) {
    final var dialog = new QueryParamsFormDialog(JOptionPane.getRootFrame(), params);
    dialog.setModal(true);
    dialog.setVisible(true);
    return dialog.values();
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals(FnAction.ESC_CMD)) {
      setVisible(false);
    } else if (e.getActionCommand().equals(FnAction.ENTER_CMD)) {
      for (final var c : inputComponents) {
        getText(c)
            .ifPresent(
                v -> {
                  values.put(c.getName(), v);
                });
      }
      if (values.size() == params.size()) {
        var errors =
            params.stream()
                .filter(p -> !p.validator().test(values.get(p.id())))
                .map(Param::name)
                .map(v -> "<li>%s</li>".formatted(v))
                .collect(Collectors.joining());
        if (!errors.isEmpty()) {
          showMessageDialog(
              this, "<html><body>%s<ul>%s</ul></body></html>".formatted("Wrong values", errors));
          return;
        }
        success.set(true);
        setVisible(false);
      } else {
        showMessageDialog(
            this,
            "<html><body>%s<ul>%s</ul></body></html>"
                .formatted("Missing required values", missingValues()));
      }
    }
  }

  private String missingValues() {
    return params.stream()
        .filter(p -> !values.containsKey(p.id()))
        .map(Param::name)
        .map(v -> "<li>%s</li>".formatted(v))
        .collect(Collectors.joining());
  }

  public static class Param {

    private final String id;
    private final String name;
    private final String type;
    private final Object defaultValue;
    private final Predicate<Object> validator;

    public Param(String name, String type, Object defaultValue, Predicate<Object> validator) {
      this.name = name;
      this.type = type;
      this.defaultValue = defaultValue;
      this.id = name.toUpperCase().replace(" ", "_");
      this.validator = validator;
    }

    public Param(String name, String type) {
      this(name, type, null, b -> true);
    }

    public static Param date(String label, ZonedDateTime dt) {
      return new Param(label, "date", format(dt), b -> true);
    }

    public static Param multi(String label, String text) {
      return new Param(label, "multi", text, b -> true);
    }

    public static Param multi(String label, String text, Predicate<Object> validator) {
      return new Param(label, "multi", text, validator);
    }

    public boolean isLineInput() {
      return !type.equals("multi");
    }

    public String id() {
      return id;
    }

    public Object defaultValue() {
      return defaultValue;
    }

    public String name() {
      return name;
    }

    public Predicate<Object> validator() {
      return validator;
    }

    public String type() {
      return type;
    }

    @Override
    public String toString() {
      return "%s %s %s".formatted(id, name, defaultValue);
    }
  }

  public static interface InputValue<T> {
    String name();

    T value();

    String label();
  }

  public record TextValue(String name, String value, String label) implements InputValue<String> {}
}

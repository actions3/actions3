/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common;

import java.time.Duration;
import java.time.ZonedDateTime;

public record DateRange(ZonedDateTime start, ZonedDateTime end) {

  public DateRange withStart(ZonedDateTime start) {
    return new DateRange(start, this.end);
  }

  public DateRange withEnd(ZonedDateTime end) {
    return new DateRange(this.start, end);
  }

  public DateRange forward() {
    return new DateRange(end, end.plus(duration()));
  }

  public String startStr() {
    return DateUtils.DTF.format(start);
  }

  public String endStr() {
    return DateUtils.DTF.format(end);
  }

  public DateRange backward() {
    return new DateRange(start.minus(duration()), start);
  }

  private Duration duration() {
    return Duration.between(start, end);
  }

  public static DateRange before(ZonedDateTime end, Duration duration) {
    final var start = end.minus(duration);
    return new DateRange(start, end);
  }

  public static DateRange beforeNow(Duration duration) {
    return before(ZonedDateTime.now(), duration);
  }
}

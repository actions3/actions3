/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common;

import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.YES_NO_OPTION;
import static javax.swing.JOptionPane.YES_OPTION;
import static javax.swing.JOptionPane.showConfirmDialog;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.swing.JButton;

public final class SwingSupport {
  private SwingSupport() {}

  public static JButton newButton(String label, String command, ActionListener listener) {
    final var btn = new JButton(label);
    btn.setActionCommand(command);
    if (listener != null) {
      btn.addActionListener(listener);
    }
    return btn;
  }

  public static JButton newButton(String label, ActionListener listener) {
    return newButton(label, "$CMD_%s".formatted(label).toUpperCase(), listener);
  }

  public static void showErrorDialog(Component parent, String message, List<String> error) {
    var errosList =
        error.stream().map(e -> "<li>%s</li>".formatted(e)).collect(Collectors.joining());
    final var dialogMsg =
        "<html><body><p>%s</p><ul>%s</ul><p>Copy message to clipboard?</p></body></html>"
            .formatted(message, errosList);
    final var opt = showConfirmDialog(parent, dialogMsg, "Error", YES_NO_OPTION, ERROR_MESSAGE);
    if (opt == YES_OPTION) {
      var copyMsg =
          Stream.concat(Stream.of(message), error.stream()).collect(Collectors.joining("\n"));
      Toolkit.getDefaultToolkit()
          .getSystemClipboard()
          .setContents(new StringSelection(copyMsg), null);
    }
  }

  public static void showErrorDialog(Component parent, String message, Throwable e) {
    final var dialogMsg =
        "<html><body><p>%s</p><p>Copy error stacktrace to clipboard?</p></body></html>"
            .formatted(message);
    final var opt = showConfirmDialog(parent, dialogMsg, "Error", YES_NO_OPTION, ERROR_MESSAGE);
    if (opt == YES_OPTION) {
      try (var sw = new StringWriter();
          var pw = new PrintWriter(sw)) {
        e.printStackTrace(pw);
        Toolkit.getDefaultToolkit()
            .getSystemClipboard()
            .setContents(new StringSelection(sw.toString()), null);
      } catch (final IOException e1) {
      }
    }
  }
}

/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common;

import static java.awt.event.MouseEvent.BUTTON3;
import static java.awt.event.MouseEvent.MOUSE_CLICKED;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.function.Consumer;

public class MouseEventHandler implements MouseListener {

  private final Consumer<MouseEvent> handler;

  public MouseEventHandler(Consumer<MouseEvent> handler) {
    super();
    this.handler = handler;
  }

  @Override
  public void mouseClicked(MouseEvent e) {
    handler.accept(e);
  }

  @Override
  public void mousePressed(MouseEvent e) {
    handler.accept(e);
  }

  @Override
  public void mouseReleased(MouseEvent e) {
    handler.accept(e);
  }

  @Override
  public void mouseEntered(MouseEvent e) {
    handler.accept(e);
  }

  @Override
  public void mouseExited(MouseEvent e) {
    handler.accept(e);
  }

  public static MouseEventHandler of(Consumer<MouseEvent> handler) {
    return new MouseEventHandler(handler);
  }

  public static boolean isRightClick(MouseEvent e) {
    return e.getID() == MOUSE_CLICKED && e.getButton() == BUTTON3 && e.getClickCount() == 1;
  }
}

/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common;

import static com.fasterxml.jackson.databind.DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.milosz.apps.cm.common.store.Bytes;
import java.io.IOException;

public enum JsonSupport {
  INSTANCE;

  private final ObjectMapper json;
  private final ObjectWriter prettyWriter;

  private JsonSupport() {
    this.json = new ObjectMapper();

    // don't output null values
    json.setDefaultPropertyInclusion(Include.NON_NULL);

    // ignore unknown properties in given type
    json.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    json.registerModule(new JavaTimeModule());

    // ensure that timezone is not reset during deserialization
    json.configure(ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false);

    prettyWriter =
        json.writer(
            new DefaultPrettyPrinter().withArrayIndenter(DefaultIndenter.SYSTEM_LINEFEED_INSTANCE));
  }

  public <T> T read(byte[] data, Class<T> type) {
    try {
      return json.readValue(data, type);
    } catch (final IOException e) {
      throw new JsonException("Read JSON error", e);
    }
  }

  public <T> T read(String s, Class<T> type) {
    return read(Bytes.array(s), type);
  }

  public String write(Object v, boolean pretty) {
    final var writter = pretty ? prettyWriter : json.writer();
    try {
      return writter.writeValueAsString(v);
    } catch (final JacksonException e) {
      throw new JsonException("Write JSON error", e);
    }
  }

  public byte[] bytes(Object v) {
    try {
      return json.writeValueAsBytes(v);
    } catch (final JacksonException e) {
      throw new JsonException("Write JSON error", e);
    }
  }
}

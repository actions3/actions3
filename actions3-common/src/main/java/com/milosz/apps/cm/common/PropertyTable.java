/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

/** Table dedicated for displaying properties. */
public class PropertyTable extends JTable {

  public PropertyTable(TableModel dm) {
    super(dm);
    setAutoResizeMode(AUTO_RESIZE_OFF);
  }

  /** Prepares renderer and adjust column width to text width. */
  @Override
  public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
    final var component = super.prepareRenderer(renderer, row, column);
    final var rendererWidth = component.getPreferredSize().width;
    final var tableColumn = getColumnModel().getColumn(column);
    tableColumn.setPreferredWidth(
        Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
    return component;
  }
}

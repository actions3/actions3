/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common.store;

import static com.milosz.apps.cm.common.store.Bytes.UNDEFINED;
import static com.milosz.apps.cm.common.store.Bytes.buffer;
import static com.milosz.apps.cm.common.store.Bytes.transfer;
import static com.milosz.apps.cm.common.store.PageHeader.LENGTH;
import static java.util.Arrays.copyOfRange;
import static java.util.logging.Level.FINE;

import com.milosz.apps.cm.common.LogUtils;
import com.milosz.apps.cm.spi.AppException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileLock;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ExtA3Db {
  private static final Logger LOG = LogUtils.getLogger(FINE, ExtA3Db.class.getSimpleName());

  /** Size of content page */
  static final int PS = 2 * 1024;

  /** Length of data file header */
  static final int FHS = 100;

  /** Space reserved for writing key's value */
  static final int CS = PS - PageHeader.LENGTH;

  private final RandomAccessFile data;

  /** Current transaction */
  private RandomAccessFile tx;

  /** Database file */
  private final File fileData;

  /** Rollback journal. Contains all changes done in active transaction */
  private final File fileTx;

  private final AtomicLong version = new AtomicLong(System.currentTimeMillis());

  /**
   * Unique session id specific for this instance of class. It is used as base for name of backup
   * file and rollback journal
   */
  private final String session;

  private final AtomicInteger changes = new AtomicInteger();
  private final File fileBackup;

  public ExtA3Db(File f) {
    this.fileData = f;
    this.session = UUID.randomUUID().toString().substring(0, 10);
    this.fileTx =
        new File(fileData.getParent(), ".tx-%s-%s".formatted(session, fileData.getName()));
    this.fileBackup = new File(fileData.getParent(), ".bak-%s".formatted(fileData.getName()));
    fileTx.deleteOnExit();
    try {
      this.data = new RandomAccessFile(fileData, "rwd");
      init();
    } catch (final FileNotFoundException e) {
      throw new AppException("File %s doesn't exist " + f.getAbsolutePath(), e);
    }
  }

  public void begin() {
    assert tx == null;
    assert !fileTx.exists();
    FileLock dataLock = null;
    try {
      pullVersion();
      tx = new RandomAccessFile(fileTx, "rwd");
      final var dataChan = data.getChannel();
      dataLock = dataChan.lock();
      tx.getChannel().transferFrom(dataChan, 0, Long.MAX_VALUE);
      dataLock.close();
      assert tx.length() == data.length();
    } catch (final Exception e) {
      LOG.log(Level.WARNING, "Begin transaction", e);
      throw new StorageException("Transaction failure in database", e, "0001");
    }
  }

  /**
   * Compares version in database file with version of current transaction
   *
   * <p>If version of database file is higher than version of current transaction it means that the
   * former was modified after current transaction has started. In such case method throws
   * {@linkplain IllegalStateException}.
   *
   * @throws IllegalStateException if transaction data is out of date
   */
  private void checkVersion() {
    final var last = readVersion();
    if (last > version.get()) {
      throw new IllegalStateException("Old version");
    }
  }

  public void close() {
    assert tx == null : "Transaction in progress";
    try {
      data.close();
    } catch (final Exception e) {
      LOG.log(Level.WARNING, "Closing database", e);
      throw new StorageException("Transaction failure in database", e, "0002");
    }
  }

  /**
   * Commits active transaction
   *
   * <p>Method commits active transaction in following steps:
   *
   * <ol>
   *   <li>Checks latest saved version in data file. If saved version is higher than transaction's
   *       version then commit is canceled.
   *   <li>Creates backup of data file.
   *   <li>Cleanup free pages.
   *   <li>Replaces content of data file with content of rollback journal.
   *   <li>Removes backup file and rollback journal.
   * </ol>
   *
   * @throws StorageException if transaction data is out of date. In such case client should
   *     rollback changes, start new transaction and make changes again
   */
  public void commit() {
    assert tx != null;
    assert fileTx.exists();
    try {
      checkVersion();
      final var dataChan = data.getChannel();
      try (var txChan = tx.getChannel();
          var dataLock = dataChan.lock();
          var backup = new RandomAccessFile(fileBackup, "rwd");
          var backupChannel = backup.getChannel();
          var backupLock = backupChannel.lock()) {
        // create backup of data file in case transfer of transaction data
        // fail in the middle of operation
        transfer(dataChan, backupChannel);

        sortFreePage();

        var tree = treeHeader();
        txChan.truncate(tree.totalPageCount() * PS + FHS);

        // transfer transaction data to data file
        transfer(txChan, dataChan);
        pushVersion();

        dataLock.isValid();
        backupLock.isValid();
      }
      tx.close();
      tx = null;

      // operation was successful so transaction data and
      // backup file are no longer needed
      Files.delete(fileTx.toPath());
      Files.delete(fileBackup.toPath());
    } catch (final Exception e) {
      LOG.log(Level.WARNING, "Commit transaction", e);
      throw new StorageException("Transaction failure in database", e, "0003");
    }
  }

  private int contentPos(int index) {
    return pagePos(index) + LENGTH;
  }

  private List<Node<String>> doFindAll(Predicate<Key> test, int limit) {

    final var th = treeHeader();
    if (th.pageCount() > 0) {
      final var ph = pageHeader(th.rootPage());
      return subtree(
          ph,
          test,
          header -> {
            return StrNode.of(read(header), header.key());
          },
          limit);
    }
    return List.of();
  }

  private Optional<byte[]> doGet(Key key) {
    return getHeader(key).map(ph -> read(ph));
  }

  private List<Node<String>> doList() {

    final var th = treeHeader();
    if (th.pageCount() > 0) {
      final var ph = pageHeader(th.rootPage());
      return subtree(
          ph,
          k -> true,
          header -> {
            return StrNode.of(read(header), header.key());
          },
          Integer.MAX_VALUE);
    }
    return List.of();
  }

  private List<Key> doPreorder() {

    final var th = treeHeader();
    if (th.pageCount() > 0) {
      final var ph = pageHeader(th.rootPage());
      return preorder(ph);
    }
    return List.of();
  }

  private void doPut(Key key, byte[] content) {
    final var existing = getHeader(key);
    if (existing.isPresent()) {
      final var header = existing.get();
      writeContent(content, header, treeHeader());
      return;
    }
    var th = treeHeader();
    if (th.pageCount() == 0 && th.freeCount() == 0) {
      // tree is empty and no free nodes
      // new node is tree root in first page
      final var ph = new PageHeader(0, UNDEFINED, UNDEFINED, content.length, key);
      writeContent(content, ph, th.withRootPage(0).incPage());
    } else if (th.freeCount() == 0) {
      final var ph = new PageHeader(th.pageCount(), UNDEFINED, UNDEFINED, content.length, key);
      final var parent = getLeaf(key).withChild(key, th.pageCount());
      write(parent);
      writeContent(content, ph, th.incPage());
    } else if (th.pageCount() > 0 && th.freeCount() > 0) {

      final var parent = getLeaf(key);
      var pageToUse = pageHeader(th.firstFree());
      final var newFf = pageToUse.next();
      pageToUse = pageToUse.withNext(UNDEFINED).withLength(content.length).withKey(key);
      write(parent.withChild(key, pageToUse.number()));
      writeContent(content, pageToUse.withKey(key), th.incPage().decFree().withFirstFree(newFf));
    } else {
      // tree is empty but unused nodes exist
      // new node is a tree root
      var pageToUse = pageHeader(th.firstFree());
      final var newFf = pageToUse.next();
      pageToUse = pageToUse.withLength(content.length).withNext(UNDEFINED).withKey(key);
      if (th.rootPage() == UNDEFINED) {
        th = th.withRootPage(pageToUse.number());
      }
      // last free page was used then newFf is UNDEFINED
      writeContent(content, pageToUse, th.incPage().decFree().withFirstFree(newFf));
    }
  }

  private boolean doRemove(Key rmKey) {

    pushVersion();
    final var phOpt = getHeader(rmKey);
    if (phOpt.isEmpty()) {
      return false;
    }
    final var rmNode = phOpt.get();
    var th = treeHeader();
    if (rmNode.isLeaf()) {
      final var rmParent = getNode(rmKey, rmNode.number());
      if (rmParent.isPresent()) {
        write(rmParent.get().withChild(rmKey, UNDEFINED));
      } else {
        // selected node is the only one item in database
        th = th.withRootPage(UNDEFINED);
      }
    } else {
      // internal node, possibly root
      final var rmParent = getNode(rmKey, rmNode.number());
      if (rmNode.left() != UNDEFINED) {
        // if removed node has smaller children (left subtree exists)
        // then we search for replacement there
        final var rmReplace = max(pageHeader(rmNode.left()));
        if (rmReplace.number() == rmNode.left()) {
          // node that will replace removed one is also its direct child
          if (rmParent.isPresent()) {
            write(rmParent.get().withChild(rmReplace.key(), rmReplace.number()));
          } else {
            th = th.withRootPage(rmReplace.number());
          }
          write(rmReplace.withRight(rmNode.right()));
        } else {
          final var oldLeft = rmReplace.left();
          final var rmReplacementParent = getNode(rmReplace.key(), rmReplace.number()).get();
          write(rmReplace.withLeft(rmNode.left()).withRight(rmNode.right()));
          write(rmReplacementParent.withRight(oldLeft));
          if (rmParent.isPresent()) {
            write(rmParent.get().withChild(rmReplace.key(), rmReplace.number()));
          } else {
            th = th.withRootPage(rmReplace.number());
          }
        }
      } else if (rmNode.right() != UNDEFINED) {
        // removed node has only greater children (right subtree exists)
        // we search for replacement there
        final var rmReplace = min(pageHeader(rmNode.right()));
        if (rmReplace.number() == rmNode.right()) {
          if (rmParent.isPresent()) {
            write(rmParent.get().withChild(rmReplace.key(), rmReplace.number()));
          } else {
            th = th.withRootPage(rmReplace.number());
          }
          // node that will replace removed node is also its child
          // therefore left subtree of removed node becomes subtree of
          // replacement node
          write(rmReplace.withLeft(rmNode.left()));
        } else {
          // node that is replacing removed node is not its direct child
          // therefore parent of replacing node must be updated with subtree

          final var oldRight = rmReplace.right();
          final var rmReplaceParent = getNode(rmReplace.key(), rmReplace.number()).get();
          write(rmReplace.withLeft(rmNode.left()).withRight(rmNode.right()));
          write(rmReplaceParent.withLeft(oldRight));
          if (rmParent.isPresent()) {
            write(rmParent.get().withChild(rmReplace.key(), rmReplace.number()));
          } else {
            th = th.withRootPage(rmReplace.number());
          }
        }
      }
      write(rmNode.withLeft(UNDEFINED).withRight(UNDEFINED));
    }
    removeContent(rmNode, th);
    return true;
  }

  /**
   * Return all items matching condition in ascending order up to defined limit
   *
   * @param test condition
   * @param limit number of items to read
   * @return list of items as string
   */
  public List<Node<String>> findAll(Predicate<Key> test, int limit) {
    assert limit > 0;
    synchronized (data) {
      return doFindAll(test, limit);
    }
  }

  public List<Node<String>> findAll(Predicate<Key> test) {
    synchronized (data) {
      return doFindAll(test, Integer.MAX_VALUE);
    }
  }

  public Optional<byte[]> get(Key key) {
    synchronized (data) {
      return doGet(key);
    }
  }

  private Optional<PageHeader> getHeader(Key key) {

    final var th = treeHeader();
    var pn = th.rootPage();
    while (pn != UNDEFINED) {
      final var ph = pageHeader(pn);
      final var cmp = key.compareTo(ph.key());
      if (cmp == 0) {
        return Optional.of(ph);
      } else if (cmp < 0) {
        pn = ph.left();
      } else {
        pn = ph.right();
      }
    }
    return Optional.empty();
  }

  private PageHeader getLeaf(Key key) {

    final var th = treeHeader();
    var pn = th.rootPage();
    while (pn != UNDEFINED) {
      final var ph = pageHeader(pn);
      pn = ph.next(key);
      if (pn == UNDEFINED) {
        return ph;
      }
    }
    throw new IllegalStateException("Not found");
  }

  private Optional<PageHeader> getNode(Key key, int child) {

    final var th = treeHeader();
    var pn = th.rootPage();
    while (pn != UNDEFINED) {
      final var ph = pageHeader(pn);
      final var cmp = key.compareTo(ph.key());
      if (cmp < 0) {
        if (ph.left() == child) {
          return Optional.of(ph);
        } else {
          pn = ph.left();
        }
      } else if (cmp >= 0) {
        if (ph.right() == child) {
          return Optional.of(ph);
        } else {
          pn = ph.right();
        }
      }
    }
    return Optional.empty();
    // throw new IllegalStateException("Not found");

  }

  private void init() {
    try {
      if (data.length() > 0) {
        restore();
        pullVersion();
        var tree = treeHeader();
        LOG.info(
            "Database %s stats: free=%s, used=%s"
                .formatted(fileData.getName(), tree.freeCount(), tree.pageCount()));
      } else {
        final var th = new TreeHeader(UNDEFINED, UNDEFINED, 0, 0);
        pushVersion();
        writeDirect(th.bytes(), 8);
      }
    } catch (final IOException e) {
      LOG.log(Level.WARNING, "Initialize connection", e);
      throw new StorageException("Database connection failure", e);
    }
  }

  public List<Node<String>> list() {
    synchronized (data) {
      return doList();
    }
  }

  /**
   * List all nodes which key matches give condition
   *
   * @param <T>
   * @param condition if true then node is added to list
   * @param factory factory that deserialize node
   * @return list of matching nodes
   */
  public <T> List<Node<T>> list(
      Predicate<Key> condition, BiFunction<Key, byte[], Node<T>> factory) {
    final var th = treeHeader();
    if (th.pageCount() > 0) {
      final var ph = pageHeader(th.rootPage());
      return subtree(
          ph,
          condition,
          header -> {
            return factory.apply(header.key(), read(header));
          },
          Integer.MAX_VALUE);
    }
    return List.of();
  }

  private PageHeader max(PageHeader node) {

    if (node.right() == UNDEFINED) {
      return node;
    }
    return max(pageHeader(node.right()));
  }

  private PageHeader min(PageHeader node) {

    if (node.left() == UNDEFINED) {
      return node;
    }
    return min(pageHeader(node.left()));
  }

  private PageHeader pageHeader(int number) {
    final var buf = read(LENGTH, pagePos(number));
    return PageHeader.from(buf);
  }

  private LinkedList<PageHeader> pageList(int start, int limit) {
    var page = pageHeader(start);
    final var all = new LinkedList<PageHeader>();
    all.add(page);
    while (page.hasNext() && all.size() < limit) {
      page = pageHeader(page.next());
      all.add(page);
    }
    return all;
  }

  private int pagePos(int number) {
    assert number >= 0 : "Negative page number";
    return FHS + number * PS;
  }

  public List<Key> preorder() {
    synchronized (data) {
      return doPreorder();
    }
  }

  private List<Key> preorder(PageHeader ph) {

    final List<Key> st = new ArrayList<>();
    st.add(ph.key());
    if (ph.left() != UNDEFINED) {
      final var leaf = preorder(pageHeader(ph.left()));
      st.addAll(leaf);
    }
    if (ph.right() != UNDEFINED) {
      st.addAll(preorder(pageHeader(ph.right())));
    }
    return st;
  }

  private void pullVersion() {
    final var last = readVersion();
    version.set(last);
  }

  private void pushVersion() {
    checkVersion();
    version.set(System.currentTimeMillis());
    writeVersion(version.get());
  }

  public void put(Key key, byte[] content) {
    synchronized (data) {
      doPut(key, content);
    }
  }

  /**
   * Reads data from channel
   *
   * @param cap expected buffer's length
   * @param pos position to start reading from
   * @return read data
   */
  private ByteBuffer read(int cap, int pos) {
    final var chan = tx != null ? tx.getChannel() : data.getChannel();
    final var buf = buffer(cap);
    try {
      chan.read(buf, pos);
      return buf;
    } catch (final IOException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  private byte[] read(PageHeader ph) {
    final var pages = pageList(ph.number(), Integer.MAX_VALUE);
    try (var bos = new ByteArrayOutputStream()) {
      for (final var p : pages) {
        final var data = read(p.length(), contentPos(p.number())).array();
        bos.write(data);
      }
      return bos.toByteArray();
    } catch (final IOException e) {
      LOG.log(Level.WARNING, "Read header", e);
      throw new StorageException("Can't read from database", e, "0001");
    }
  }

  private long readVersion() {
    final var buf = buffer(8);
    try {
      final var chan = data.getChannel();
      chan.read(buf, 0);
      buf.rewind();
      chan.position(0);
      return buf.getLong();
    } catch (final IOException e) {
      LOG.log(Level.WARNING, "Read version", e);
      throw new StorageException("Can't read from database", e, "0002");
    }
  }

  public boolean remove(Key rmKey) {
    synchronized (data) {
      return doRemove(rmKey);
    }
  }

  private void removeContent(PageHeader main, TreeHeader tree) {
    final var pages = new LinkedList<PageHeader>();
    pages.add(main);
    if (main.hasNext()) {
      pages.addAll(pageList(main.next(), Integer.MAX_VALUE));
    }
    final var th = tree.incFreeCount(pages.size()).incPageCount(-pages.size());
    final var ff = th.firstFree();
    write(th.withFirstFree(main.number()));
    write(main.withLeft(UNDEFINED).withRight(UNDEFINED));
    if (ff != UNDEFINED) {
      write(pages.getLast().withLeft(UNDEFINED).withRight(UNDEFINED).withNext(ff));
    }
  }

  private void sortFreePage() {
    var tree = treeHeader();
    if (tree.freeCount() < 2) {
      return;
    }
    var freeHeaders = new TreeSet<PageHeader>(Comparator.comparing(PageHeader::number).reversed());
    freeHeaders.addAll(pageList(tree.firstFree(), Integer.MAX_VALUE));
    int total = tree.totalPageCount();
    PageHeader prev = null;
    for (var fh : freeHeaders) {
      if (fh.number() < total - 1) {
        if (prev != null) {
          write(prev.withNext(fh.number()));
        }
        prev = fh;
      } else {
        LOG.info("Skip free page " + fh.number());
        total--;
      }
    }
    write(
        tree.withFirstFree(freeHeaders.getFirst().number())
            .withFreeCount(total - tree.pageCount()));
  }

  private void restore() {
    try {
      if (fileBackup.exists() && fileBackup.length() > 0) {
        try (var abak = new RandomAccessFile(fileBackup, "rwd");
            var cb = abak.getChannel(); ) {
          final var cl = cb.tryLock();
          if (cl != null) {
            final var target = data.getChannel();
            target.transferFrom(cb, 0, Long.MAX_VALUE);
            target.truncate(cb.size());
          }
        }
        Files.delete(fileBackup.toPath());
      }
    } catch (final Exception e) {
      LOG.log(Level.WARNING, "Restore db", e);
      throw new StorageException("Can't write database", e, "0001");
    }
  }

  public void rollback() {
    if (tx == null) {
      return;
    }
    assert fileTx.exists();
    try {
      tx.close();
      fileTx.delete();
      tx = null;
    } catch (final IOException e) {
      LOG.log(Level.WARNING, "Rollback", e);
      throw new StorageException("DB transaction error", e, "0004");
    }
  }

  public int size() {
    return treeHeader().pageCount();
  }

  private <T> List<T> subtree(
      PageHeader ph, Predicate<Key> cmp, Function<PageHeader, T> map, int limit) {
    final List<T> st = new ArrayList<>();
    if (ph.left() != UNDEFINED) {
      final var leaf = subtree(pageHeader(ph.left()), cmp, map, limit);
      st.addAll(leaf);
    }
    if (cmp.test(ph.key()) && st.size() < limit) {
      st.add(map.apply(ph));
    }
    var rightLimit = limit - st.size();
    if (ph.right() != UNDEFINED && rightLimit > 0) {
      final var leaf = subtree(pageHeader(ph.right()), cmp, map, rightLimit);
      st.addAll(leaf);
    }
    return st;
  }

  TreeHeader treeHeader() {
    final var buf = read(FHS - 8, 8);
    return TreeHeader.from(buf);
  }

  private void write(ByteBuffer buf, int pos) {
    assert tx != null : "No transaction"; // transaction in progress

    try {
      tx.getChannel().write(buf, pos);
      buf.rewind();
      changes.incrementAndGet();
    } catch (final IOException e) {
      LOG.log(Level.WARNING, "Write data", e);
      throw new StorageException("Can't write in database", e, "0002");
    }
  }

  private void write(PageHeader ph) {
    write(ph.bytes(), pagePos(ph.number()));
  }

  private void write(TreeHeader th) {
    write(th.bytes(), 8);
  }

  private void writeContent(byte[] content, PageHeader main, TreeHeader tree) {
    final var required = Math.ceil((double) content.length / CS);
    final var pages = new LinkedList<PageHeader>();
    pages.add(main);
    if (main.hasNext()) {
      pages.addAll(pageList(main.next(), Integer.MAX_VALUE));
    }
    var th = tree;
    if (required > pages.size()) {
      if (th.freeCount() > 0) {
        final var free = pageList(th.firstFree(), (int) required - pages.size());
        pages.addAll(free);
        th =
            th.withFirstFree(free.getLast().next())
                .incFreeCount(-free.size())
                .incPageCount(free.size());
      }
      final var size = required - pages.size();
      for (var i = 0; i < size; i++) {
        pages.add(new PageHeader(th.pageCount(), UNDEFINED, UNDEFINED, UNDEFINED, main.key()));
        th = th.incPage();
      }
    }
    var total = content.length;
    PageHeader header = null;
    var pc = 0;
    while (total > 0) {
      final var len = Math.min(CS, total);
      final var nextHeader = pages.pop().withLength(len);
      if (header != null) {
        write(header.withNext(nextHeader.number()).withKey(main.key()));
      }
      final var start = pc++ * CS;
      final var end = start + Math.min(CS, total);
      write(ByteBuffer.wrap(copyOfRange(content, start, end)), contentPos(nextHeader.number()));

      header = nextHeader;
      total -= CS;
    }
    write(header.withNext(UNDEFINED).withKey(main.key()));
    if (!pages.isEmpty()) {
      final var ff = th.firstFree();
      th =
          th.incFreeCount(pages.size())
              .incPageCount(-pages.size())
              .withFirstFree(pages.getFirst().number());
      if (ff != UNDEFINED) {
        write(pages.getLast().withNext(ff));
      }
    }
    write(th);
  }

  private void writeDirect(ByteBuffer buf, int pos) {

    try {
      data.getChannel().write(buf, pos);
      buf.rewind();
    } catch (final IOException e) {
      LOG.log(Level.WARNING, "Write direct", e);
      throw new StorageException("Can't write to database", e, "0003");
    }
  }

  private void writeVersion(long v) {
    final var buf = buffer(8);
    buf.putLong(v);
    buf.rewind();
    try {
      final var chan = data.getChannel();
      chan.write(buf, 0);
      chan.position(0);
    } catch (final IOException e) {
      LOG.log(Level.WARNING, "Write version", e);
      throw new StorageException("Can't write to database", e, "0004");
    }
  }
}

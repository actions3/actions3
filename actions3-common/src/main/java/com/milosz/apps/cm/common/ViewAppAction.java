/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common;

import com.milosz.apps.cm.spi.AppAction;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Objects;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

public record ViewAppAction(String name, ActionHandler handler, int keyId, ActionCommand command)
    implements AppAction {

  public static final int KEY_MASK = InputEvent.CTRL_DOWN_MASK | InputEvent.SHIFT_DOWN_MASK;

  @Override
  public boolean match(KeyEvent e) {
    return isViewAction(e) && e.getKeyCode() == keyId;
  }

  @Override
  public void invoke(KeyEvent e) {
    handler.accept(command());
  }

  private boolean isViewAction(KeyEvent e) {
    return e.getModifiersEx() == KEY_MASK;
  }

  @Override
  public void installMenu(JMenu menu) {
    final var item = new JMenuItem(name);
    item.addActionListener(handler);
    item.setActionCommand(command.command());
    if (keyId > 0) {
      item.setAccelerator(KeyStroke.getKeyStroke(keyId, KEY_MASK));
    }
    menu.add(item);
  }

  @Override
  public int compareTo(AppAction o) {
    if (o instanceof final ViewAppAction vva) {
      return Integer.compare(keyId, vva.keyId());
    }
    return name().compareTo(o.name());
  }

  @Override
  public int hashCode() {
    return Objects.hash(keyId, name);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final var other = (ViewAppAction) obj;
    return keyId == other.keyId && Objects.equals(name, other.name);
  }
}

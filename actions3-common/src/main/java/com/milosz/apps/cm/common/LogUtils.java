/*
 * Actions3
 * Copyright (C) 2023 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LogUtils {

  private static final AtomicBoolean INIT = new AtomicBoolean(false);

  private static void init(Level level) {
    synchronized (INIT) {
      if (!INIT.getAndSet(true)) {
        final var file = Common.getLogFile().toString();
        final var log = Logger.getLogger("");
        try {
          final var handler = new FileHandler(file, 1024 * 1024 * 5, 10, true);
          handler.setFormatter(new SimpleFormatter());
          log.addHandler(handler);
        } catch (final Exception e) {
          e.printStackTrace();
        }
      }
    }
  }

  public static Logger getLogger(Level level, String name) {
    init(level);
    return Logger.getLogger(name);
  }
}

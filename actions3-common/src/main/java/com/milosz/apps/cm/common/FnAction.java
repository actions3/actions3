/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.function.Consumer;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;

/**
 * Implementation of Swing Action that allows handling ActionEvents in input dialogs in functional
 * style
 */
public class FnAction extends AbstractAction {

  private static final long serialVersionUID = -8180651361090674302L;

  private final Consumer<ActionEvent> handler;

  private static final KeyStroke ENTER = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);

  private static final KeyStroke ESC = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);

  public static final String ESC_CMD = "$ESC";

  public static final String ENTER_CMD = "$ENTER";

  public FnAction(String name, Consumer<ActionEvent> handler) {
    super();
    this.handler = handler;
    putValue(Action.ACTION_COMMAND_KEY, name);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    handler.accept(e);
  }

  /**
   * Installs new action on given Swing component that is triggered on specified key stroke.
   *
   * @param ks keys sequence that triggers action
   * @param name name of action that matches
   * @param c
   * @param eventHandler
   */
  public static void install(
      KeyStroke ks, String name, JComponent c, Consumer<ActionEvent> eventHandler) {
    final var act = new FnAction(name, eventHandler);
    c.getInputMap(JComponent.WHEN_FOCUSED).put(ks, name);
    c.getActionMap().put(name, act);
  }

  public static void enter(JComponent c, Consumer<ActionEvent> eventHandler) {
    enter(ENTER_CMD, c, eventHandler);
  }

  public static void enter(String name, JComponent c, Consumer<ActionEvent> eventHandler) {
    var ks = ENTER;
    if (c instanceof JTextArea) {
      ks = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, KeyEvent.CTRL_DOWN_MASK);
    }
    install(ks, name, c, eventHandler);
  }

  public static void enterNext(JComponent c, Consumer<ActionEvent> eventHandler) {
    enter("$NEXT_" + c.getName(), c, eventHandler);
  }

  public static void esc(String name, JComponent c, Consumer<ActionEvent> eventHandler) {
    install(ESC, name, c, eventHandler);
  }

  public static void esc(JComponent c, Consumer<ActionEvent> eventHandler) {
    esc(ESC_CMD, c, eventHandler);
  }
}

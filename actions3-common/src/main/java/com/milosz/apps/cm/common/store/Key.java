/*
 * Actions3
 * Copyright (C) 2023-2024 Miłosz Pigłas
 *
 *
 * Actions3 is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * Actions3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Actions3.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package com.milosz.apps.cm.common.store;

import static com.milosz.apps.cm.common.store.Bytes.CHARSET;
import static com.milosz.apps.cm.common.store.Bytes.array;
import static com.milosz.apps.cm.common.store.Bytes.buffer;
import static java.util.Arrays.compare;
import static java.util.Arrays.copyOf;
import static java.util.Arrays.copyOfRange;

import java.util.Arrays;
import java.util.Objects;

/**
 * Unique key of item in database
 *
 * @param pk partition key. It might be used to mimic table
 * @param sk sort key. Value used to order items inside partition
 */
public record Key(byte[] pk, byte[] sk) implements Comparable<Key> {

  static final int LEN_PK = 20;
  static final int LEN_SK = 80;

  public Key(byte[] array) {
    this(copyOfRange(array, 0, LEN_PK), copyOfRange(array, LEN_PK, LEN_PK + LEN_SK));
  }

  public void validate() {}

  @Override
  public int compareTo(Key o) {
    final var cp = compare(pk, o.pk);
    return cp != 0 ? cp : compare(sk, o.sk);
  }

  public String pkStr() {
    return new String(pk, CHARSET).trim();
  }

  public String skStr() {
    return new String(sk, CHARSET).trim();
  }

  @Override
  public String toString() {
    return pkStr() + ":" + skStr();
  }

  public byte[] bytes() {
    final var buf = buffer(LEN_PK + LEN_SK);
    buf.put(pk).put(sk);
    return buf.array();
  }

  public static Key of(String pk, String sk) {
    return new Key(array(pk, LEN_PK), array(sk, LEN_SK));
  }

  public static Key of(String pk, byte[] sk) {
    return new Key(array(pk, LEN_PK), copyOf(sk, LEN_SK));
  }

  @Override
  public int hashCode() {
    return Objects.hash(Arrays.hashCode(pk), Arrays.hashCode(sk));
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final var other = (Key) obj;
    return Arrays.equals(pk, other.pk) && Arrays.equals(sk, other.sk);
  }
}

#!/usr/bin/python

from urllib.request import *
from os import environ
import json

PACKAGE_ID=35654817

def new_request(path, method='GET'):
    api_url = environ['CI_API_V4_URL']
    project_id = environ['CI_PROJECT_ID']

    url= f'{api_url}/projects/{project_id}' + path
    
    req = Request(url, method=method)
    if 'PRIVATE_TOKEN' in environ:
        req.add_header('PRIVATE-TOKEN', environ['PRIVATE_TOKEN'])
    elif 'CI_JOB_TOKEN' in environ:
        req.add_header('JOB-TOKEN', environ['CI_JOB_TOKEN'])
    else:
        raise Exception('Missing access token')
    return req

def delete_file(pkg_file):
    delete_path = f'/packages/{PACKAGE_ID}/package_files/{pkg_file["id"]}'
    with urlopen(new_request(delete_path, method='DELETE')) as resp:
        print('Remove Status', resp.status)
        if resp.status >= 400:
            print(resp.read().decode('UTF-8'))
            raise Exception("Failed to remove package file", pkg_file)
        

def cleanup():
    list_path = f'/packages/{PACKAGE_ID}/package_files?per_page=100'
    with urlopen(new_request(list_path)) as resp:
        print('Status', resp.status)
        all_files = json.load(resp)
        all_files.sort(key=lambda e: e['created_at'], reverse=True)
        if len(all_files) > 1:
            for af in all_files[1:]:
                delete_file(af)
        else:
            print("Nothing to remove")    
        
if __name__ == '__main__':
    cleanup()

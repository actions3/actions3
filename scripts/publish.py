#!/usr/bin/python

from urllib.request import *
from os import environ

def publish():
	api_url = environ['CI_API_V4_URL']
	project_id = environ['CI_PROJECT_ID']
	with open('actions3-core/target/exec/actions3.jar', 'rb') as ab:
		upload_url = f'{api_url}/projects/{project_id}/packages/generic/actions3/latest/actions3.jar'
		print('Uploading to ', upload_url)
		req = Request(
			upload_url,
			data=ab.read(),
			method='PUT'
		)
		if 'PRIVATE_TOKEN' in environ:
			req.add_header('PRIVATE-TOKEN', environ['PRIVATE_TOKEN'])
		elif 'CI_JOB_TOKEN' in environ:
			req.add_header('JOB-TOKEN', environ['CI_JOB_TOKEN'])
		else:
			raise Exception('Missing access token')
		with urlopen(req) as resp:
			print('Status', resp.status)
			print(resp.read().decode('UTF-8'))

if __name__ == '__main__':
	publish()

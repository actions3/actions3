# Actions3 - AWS Logs and Metric on desktop

Actions3 is a portable desktop application for browsing AWS CloudWatch logs and metrics.
Inside application users can configure and save `actions` and display metrics or logs
with one click.

[Latest version](https://gitlab.com/actions3/actions3/-/packages/35654817)

![AWS CloudWatch Metrics](docs/img/metrics_actions.png)

![AWS CloudWatch Logs](docs/img/logs_actions.png).

**[User guide](docs/guide.md)**

## Main features

- displays the AWS CloudWatch Metrics on a line chart,
- runs the AWS CloudWatch Log Insights query and displays results in a table,
- one-click access to predefined queries and metric charts,
- saves history of log search queries,
- support for keyboard navigation in metrics chart and logs table,
- support for logs in JSON format,
- save views configuration in project files and share with other team members,
- and other ...

## Building

**Requirements**:
 - Java 21 SDK
 - Apache Maven 3.9.2
 - GraalVM (optional)
 
Maven command `mvn clean install` build executable "uber jar" archive file `actions3.jar` 
in `actions3-core/target/exec`directory. File contains all required dependencies and 
can be run on any platform supported by Java JRE with command `java -jar actions3.jar`.

## Usage

1. Download latest version from project's home page.
2. Install Java 21 JRE.
3. Run `java -jar actions3.jar` or double click on file.

## License

Actions3 is distributed under GNU General Public License.

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

## Q&A

Q: I can create metrics dashboard in AWS account. Why do I need another 
   application?
   
A: CloudWatch dashboards have many functions that can be useful for monitoring 
   resources. Many of them are implemented in Actions3. You may find the `Actions3` 
   useful if you want quick access to metrics, easy definition of views or good 
   keyboard support.
   
Q: Can I use `Actions3` on computers with MacOS?

A: You can use application on any operating system that is supported by Java JRE,
   however only Windows and Linux are officially supported.
   
Q: Can I use `Actions3` for free?

A: GNU General Public License allows you to use application without any additional
   cost. However remember that you might be charged for fetching metrics from AWS. 
